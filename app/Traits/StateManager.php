<?php
namespace App\Traits;

trait StateManager
{

    public function getStateAttribute()
    {
        return $this->isCheckStateProperty() && $this->isStateProperty() ? $this->setStates() : null;
    }

    public function setStates()
    {
        $states = [];
        foreach ($this->getCheckStateAttributes() as $attribute) {
            $st = $this->setAttributeState($attribute);
            if (is_array($st)) {
                foreach ($st as $value) {
                    $states[] = $value;
                }
            } else {
                $states[] = $st;
            }
        }
        return $states;
    }

    public function setAttributeState($attrname)
    {
        $attribute = $this->getAttribute($attrname);
        $result = [];
        if (is_array($attribute)) {
            foreach ($attribute as $key => $attr) {
                if ($this->setStateData($attr, $attrname, $key)) {
                    $result[$key] = $this->setStateData($attr, $attrname, $key);
                }

            }
            return $result;
        } else {
            return $this->setStateData($attribute, $attrname);
        }
    }

    public function setStateData($attribute, $attrname, $child = '')
    {
        if (!empty($attribute) && $attribute != null) {
            return false;
        }
        $child = $child ? "($child)" : '';
        return 'The page does not have <span style="color:red;">"' . ucfirst($attrname) . $child .'</span>"';
    }

    public function getCheckStateAttributes()
    {
        return is_array($this->checkState)
        ? $this->checkState
        : [];
    }

    public function isCheckStateProperty()
    {
        return property_exists($this, 'checkState');
    }

    public function isStateProperty()
    {
        return property_exists($this, 'appends') && in_array('state', $this->appends);
    }
}
