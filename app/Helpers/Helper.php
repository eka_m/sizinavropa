<?php

function addAttributeIf($attr, $obj, $key)
{
    return isset($obj[$key]) ? $attr . "=$obj[$key]" : "";
}

function setImage($obj, $key)
{
    if (is_array($obj)) {
        return isset($obj[$key]) ? $obj[$key] : "";
    }
    return "/uploads" . $obj;
}

function setImageAdmin($attr, $obj, $key)
{
    if (isJSON($obj)) {
        $obj = json_decode($obj,true);
        return addAttributeIf($attr, $obj, $key);
    }
    $res = [
        "path" => "/uploads" . $obj,
        "params" => [],
    ];
    $res = json_encode($res);
    return $attr . "=$res";
}

function isJSON($string){
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
 }
