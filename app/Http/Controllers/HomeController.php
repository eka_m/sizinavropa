<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Setting;
use App\Models\Slide;
use App\Models\Video;
use App\Models\Locale;

use Illuminate\Http\Request;

class HomeController extends GlobalController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //try {
            $slides = $this->slides();
            $news = $this->news();
            // dd($news->first()->image);
            // $news = $this->setNewsLang($news);
            $videos = $this->videos();
            $popular = $this->popular();
            return view('pages.home.home', [
                'news' => $news,
                'slides' => $slides,
                'videos' => $videos,
                'popular' => $popular,
            ]);
        //} catch (\Exception $e) {

        //}
    }

    public function err () {
        echo "Whoops, looks like something went wrong.";
        exit();
    }

    private function slides()
    {
        return Slide::orderBy('created_at', 'DESC')->get();
    }

    private function news()
    {
        $data = Locale::where('slug', $this->locale)
        ->with(['articles' => function($query){
            $query->select('id', 'page_id', 'short', 'views', 'fakeviews', 'name', 'url', 'image', 'person', 'interesting', 'status', 'created_at', 'pos', 'locale_id')
            ->where('status', 1)
            ->with('page')
            ->orderBy('pos', 'DESC')
            ->limit(16);
        }])->firstOrfail();
        return $data->articles;
    }
    

    private function videos()
    {
        return Video::orderBy('created_at', 'DESC')->limit(4)->get();
    }

    private function popular()
    {
        return Article::orderBy('fakeviews', 'DESC')->where('status', 1)->where('parent_id', 0)->limit(15)->get();
    }

    public function search(Request $request)
    {
        try {
            $field = $request->get('query');
            $articles = Article::where('name', 'like', '%' . $field . '%')
            // ->orWhere('content', 'like', '%' . $field . '%')
            ->where('status', 1)
            ->orderBy('created_at', 'DESC')
            ->with('page')
            ->paginate(6);
            return view('partials.search-result', ['articles' => $articles]);
        } catch (\Exception $e) {

        }
    }
}
