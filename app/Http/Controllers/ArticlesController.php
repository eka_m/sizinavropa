<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticlesController extends GlobalController
{

    public function show($url)
    {

        $article = Article::where('url', $url)
        ->with('page')
        ->with('locale')
        ->with(['parent' => function ($query) {
            $query->where('status', 1)->with('locale');
         }])
        ->with(['children' => function ($query) {
            $query->where('status', 1)->with('locale');
        }])->firstOrFail();

       

        if($this->locale != $article->locale->slug) {
            $url = '/'.$this->locale;
            if($article->parent) {
                if($article->parent->locale->slug == $this->locale) {
                    $url = $url.'/article/'.$article->parent->url;
                }
            } elseif(!$article->children->isEmpty()) {
                foreach($article->children as $children) { 
                    if($children->locale->slug == $this->locale) { 
                        $url = $url.'/article/'.$children->url;
                    }
                }
            }
            return redirect($url);
        }
        $article->views = $article->views + 1;
        $article->save();
        $parsedContent = $this->getGallery($article->content);
        $article->content = $parsedContent['content'];
        return view('pages.articles.article', ['article' => $article, 'gallery'=> $parsedContent['gallery']]);
    }


}
