<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Event;
use App\Models\Admin\Imagine;
use App\Models\Admin\Masterclass;
use App\Models\Admin\Program;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImagineController extends Controller
{

    public function index()
    {
        return view('admin.pages.imagine.imagine');
    }

    public function films()
    {
        $films = Imagine::orderBy('date', 'ASC')->get();
        return view('admin.pages.imagine.films.films', ['films' => $films]);
    }

    public function createfilm()
    {
        return view('admin.pages.imagine.films.create');
    }

    public function storefilm(Request $request)
    {
        $inputs = $request->all();
        $inputs['slug'] = str_slug($inputs['title']);
        Imagine::create($inputs);
        return redirect()->route('admin.imagine.films');
    }

    public function editfilm($id)
    {
        $film = Imagine::findOrFail($id);
        return view('admin.pages.imagine.films.edit', ['film' => $film]);
    }

    public function updatefilm(Request $request, $id)
    {
        $film = Imagine::findOrFail($id);
        $inputs = $request->all();
        $inputs['slug'] = str_slug($inputs['title']);
        $film->update($inputs);

        return redirect()->route('admin.imagine.films');
    }

    public function deletefilm($id)
    {
        Imagine::destroy($id);
        return redirect()->route('admin.imagine.films');
    }


    public function program()
{
    $program = Program::orderBy('date', 'ASC')->get()->groupBy(function ($val) {
        return Carbon::parse($val->date)->format('d');
    });
    $result = [];
    foreach ($program as $group) {
        $result[$group[0]->date] = $group->groupBy('venue');
    }
    return view('admin.pages.imagine.program.program', ['program' => $result]);
}

    public function createprogramevent()
    {
        return view('admin.pages.imagine.program.create');
    }

    public function storeprogramevent(Request $request)
    {
        Program::create($request->all());
        return redirect()->route('admin.imagine.program');
    }

    public function editprogramevent($id)
    {
        $program = Program::findOrFail($id);
        return view('admin.pages.imagine.program.edit', ['program' => $program]);
    }

    public function updateprogramevent(Request $request,$id)
    {
        $program = Program::findOrFail($id);
        $program->update($request->all());
        return redirect()->route('admin.imagine.program');

    }

    public function deleteprogramevent($id)
    {
        Program::destroy($id);
        return redirect()->route('admin.imagine.program');
    }

    public function events()
    {
        $events= Event::orderBy('date', 'ASC')->get();
        return view('admin.pages.imagine.events.events', ['events' => $events]);
    }

    public function createevent()
    {
        return view('admin.pages.imagine.events.create');
    }

    public function storeevent(Request $request)
    {
        $inputs = $request->all();
        $inputs['slug'] = str_slug($inputs['name']);
        Event::create($inputs);
        return redirect()->route('admin.imagine.events');
    }

    public function editevent($id)
    {
        $event = Event::findOrFail($id);
        return view('admin.pages.imagine.events.edit', ['event' => $event]);
    }

    public function updateevent(Request $request,$id)
    {
        $event = Event::findOrFail($id);
        $inputs = $request->all();
        $inputs['slug'] = str_slug($inputs['name']);
        $event->update($inputs);
        return redirect()->route('admin.imagine.events');

    }

    public function deleteevent($id)
    {
        Event::destroy($id);
        return redirect()->route('admin.imagine.events');
    }

    public function masterclasses()
    {
        $masterclasses= Masterclass::orderBy('date', 'ASC')->get();
        return view('admin.pages.imagine.masterclasses.masterclasses', ['masterclasses' => $masterclasses]);
    }

    public function createmasterclass()
    {
        return view('admin.pages.imagine.masterclasses.create');
    }

    public function storemasterclass(Request $request)
    {
        $inputs = $request->all();
        $inputs['slug'] = str_slug($inputs['name']);
        Masterclass::create($inputs);
        return redirect()->route('admin.imagine.masterclasses');
    }

    public function editmasterclass($id)
    {
        $masterclass = Masterclass::findOrFail($id);
        return view('admin.pages.imagine.masterclasses.edit', ['masterclass' => $masterclass]);
    }

    public function updatemasterclass(Request $request,$id)
    {
        $masterclass = Masterclass::findOrFail($id);
        $inputs = $request->all();
        $inputs['slug'] = str_slug($inputs['name']);
        $masterclass->update($inputs);
        return redirect()->route('admin.imagine.masterclasses');

    }

    public function deletemasterclass($id)
    {
        Masterclass::destroy($id);
        return redirect()->route('admin.imagine.masterclasses');
    }


}
