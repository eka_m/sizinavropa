<?php

namespace App\Http\Controllers\Admin;


use App\Models\Admin\Locale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use View;

class GlobalController extends Controller
{
    public function __construct(Request $request)
    {
        $locales = Locale::orderBy('pos','ASC')->get();
        View::share('locales', $locales);
    }

    function checkRole($roles) {
        $userRole = Auth::user()->role->name;
        if (!in_array($userRole, $roles)) {
            return abort(404);
        }
    }
}