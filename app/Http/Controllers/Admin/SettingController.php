<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Article;
use App\Models\Admin\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $settings = Setting::all();
        $settings = $settings->toArray();
        foreach ($settings as $key => $value) {
            $settings[$value['name']] = $value;
            unset($settings[$key]);
        }
        foreach ($settings as $key => $value) {
            $articles = json_decode($value['params']);
            foreach ($articles as $v) {
                $settings[$key]['items'][] = Article::select(['name','id'])->where('id',$v)->first();
            }
        }
        return view('admin.pages.settings.settings', ['settings' => $settings]);
    }

    public function setStatus($settingname)
    {
        try {
            $setting = Setting::where('name', $settingname)->first();
            $setting->status = !$setting->status;
            $setting->save();
            return response()->json(['message' => 'Status updated']);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $settingname)
    {
        try {
            $setting = Setting::where('name', $settingname)->first();
            $articles = [];
            foreach ($request->all() as $key => $value) {
                $articles[$key] = $value['id'];
            }
            $setting->params = json_encode($articles);
            $setting->save();
            return response()->json(['message' => 'Popup updated'], 200);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
