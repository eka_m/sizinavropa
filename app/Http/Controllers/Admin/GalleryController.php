<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $galleries = Gallery::orderBy('created_at', 'DESC')->get();
            return view('admin.pages.gallery.galleries', ['galleries' => $galleries]);
        } catch (\Exception $e) {

        }
    }

    public function getAllGalleriesInJson()
    {
        try {
            $galleries = Gallery::select(['name','id'])->orderBy('created_at', 'DESC')->get();
            return response()->json($galleries);
        } catch (\Exception $e) {

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Gallery::create($request->all());
            return redirect()->route('galleries.index');
        } catch (\Exception $e) {

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {
        try {
            return view('admin.pages.gallery.edit', ['gallery' => $gallery]);
        } catch (\Exception $e) {

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Admin\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $gallery)
    {
        try {
            $gallery->update($request->all());
            return redirect()->route('galleries.index');
        } catch (\Exception $e) {

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
        try {
            $gallery->delete();
            return response()->json(['message' => 'Gallery deleted']);
        } catch (\Exception $e) {
        }
    }
}
