<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Slide;
use Illuminate\Http\Request;

class SlidesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $slides = Slide::orderBy('created_at', 'DESC')->paginate(10);
            return view('admin.pages.slider.slider', ['slides' => $slides]);
        } catch (\Exception $e) {

        }
    }

    public function async()
    {
        try {
            $slides = Slide::orderBy('created_at', 'DESC')->paginate(10);
            return response()->json(['slides' => $slides, 'message' => 'Slides loaded'], 200);
        } catch (\Exception $e) {

        }
    }

    public function search(Request $request)
    {
        try {
            $field = $request->get('searchfield');

            $slides = Slide::where('name', 'like', '%' . $field . '%')->orderBy('created_at', 'DESC')->paginate(10);
            if (empty($field) || !$slides->count()) {
                $notification = [
                    'message' => 'Nothing found',
                    'type' => 'danger'
                ];
                return redirect()->route('slides.index')->with('notification', $notification);
            }
            return view('admin.pages.slider.slider', ['slides' => $slides]);
        } catch (\Exception $e) {

        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if(!$request->input('image')) {
                return response()->json(['message' => 'Image required', 'type' => 'warning'], 200);
            }
            Slide::create($request->all());
            return response()->json(['message' => 'Slide added'], 200);
        } catch (\Exception $e) {
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slide $slide
     * @return \Illuminate\Http\Response
     */
    public function show(Slide $slide)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slide $slide
     * @return \Illuminate\Http\Response
     */
    public function edit(Slide $slide)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Slide $slide
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slide $slide)
    {
        try {
            if(!$request->input('image')) {
                return response()->json(['message' => 'Image required', 'type' => 'warning'], 200);
            }
            $slide->update($request->all());
            return response()->json(['message' => 'Slide updated'], 200);
        } catch (\Exception $e) {
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slide $slide
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slide $slide)
    {
        try {
            $slide->delete();
            return response()->json(['message' => 'Slide deleted'],200);
        } catch (\Exception $e) {

        }
    }
}
