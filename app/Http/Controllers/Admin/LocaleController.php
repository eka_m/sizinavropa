<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Locale;
use Illuminate\Cache\RetrievesMultipleKeys;
use Illuminate\Http\Request;

class LocaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $locales = Locale::orderBy('pos', 'ASC')->get();
            return view('admin.pages.locale.locale', ['locales' => $locales]);
        } catch (\Exception $e) {

        }
    }

    public function sort(Request $request)
    {
        try {
            $data = $request->all();
            foreach ($data as $key => $val) {
                $locale = Locale::find($key);
                $locale->update(['pos' => $val]);
            }
            return response()->json(['message' => 'Sorted'], 200);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $pos = Locale::max('pos');
            $data['pos'] = $pos + 1;
            Locale::create($data);
            $locales = Locale::orderBy('pos', 'ASC')->get();
            return response()->json(['message' => 'Locale added', 'locales' => $locales]);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    public function setStatus($id)
    {
        try {
            $locales = Locale::find($id);
            $locales->status = !$locales->status;
            $locales->save();
            return response()->json(['message' => 'Status updated']);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Locale $locale
     * @return \Illuminate\Http\Response
     */
    public function show(Locale $locale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Locale $locale
     * @return \Illuminate\Http\Response
     */
    public function edit(Locale $locale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Locale $locale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            $locale = Locale::find($id);
            $locale->update($data);
            $locales = Locale::orderBy('pos', 'ASC')->get();
            return response()->json(['message' => 'Updated', 'locales' => $locales]);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Locale $locale
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Locale::destroy($id);
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }
}
