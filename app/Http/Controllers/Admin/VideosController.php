<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Video;
use Illuminate\Http\Request;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $videos = Video::orderBy('created_at', 'DESC')->paginate(10);
            return view('admin.pages.videos.videos', ['videos' => $videos]);
        } catch (\Exception $e) {

        }
    }

    public function async()
    {
        try {
            $videos = Video::orderBy('created_at', 'DESC')->paginate(10);
            return response()->json(['message' => 'Videos loaded', 'videos' => $videos]);
        } catch (\Exception $e) {

        }
    }


    public function search(Request $request)
    {
        try {
            $field = $request->get('searchfield');

            $videos = Video::where('name', 'like', '%' . $field . '%')->orderBy('created_at', 'DESC')->paginate(10);
            if (empty($field) || !$videos->count()) {
                $notification = [
                    'message' => 'Nothing found',
                    'type' => 'danger'
                ];
                return redirect()->route('videos.index')->with('notification', $notification);
            }
            return view('admin.pages.videos.videos', ['videos' => $videos]);
        } catch (\Exception $e) {

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Video::create($request->all());
            return response()->json(['message' => 'Video saved'], 200);
        } catch (\Exception $e) {

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Video $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        try {
            $video->update($request->all());
            return response()->json(['message' => 'Video saved'], 200);
        } catch (\Exception $e) {

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        try {
            $video->delete();
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e) {

        }
    }
}
