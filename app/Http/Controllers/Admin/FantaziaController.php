<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Fantazia\Event;
use App\Models\Admin\Fantazia\Fantazia;
use App\Models\Admin\Fantazia\Masterclass;
use App\Models\Admin\Fantazia\Program;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FantaziaController extends Controller
{

    public function index()
    {
        return view('admin.pages.fantazia.index');
    }

    public function films()
    {
        $films = Fantazia::orderBy('date', 'ASC')->get();
        return view('admin.pages.fantazia.films.films', ['films' => $films]);
    }

    public function createfilm()
    {
        return view('admin.pages.fantazia.films.create');
    }

    public function storefilm(Request $request)
    {
        $inputs = $request->all();
        $inputs['slug'] = str_slug($inputs['title']);
        Fantazia::create($inputs);
        return redirect()->route('admin.fantazia.films');
    }

    public function editfilm($id)
    {
        $film = Fantazia::findOrFail($id);
        return view('admin.pages.fantazia.films.edit', ['film' => $film]);
    }

    public function updatefilm(Request $request, $id)
    {
        $film = Fantazia::findOrFail($id);
        $inputs = $request->all();
        $inputs['slug'] = str_slug($inputs['title']);
        $film->update($inputs);

        return redirect()->route('admin.fantazia.films');
    }

    public function deletefilm($id)
    {
        Fantazia::destroy($id);
        return redirect()->route('admin.fantazia.films');
    }


    public function program()
{
    $program = Program::orderBy('date', 'ASC')->get()->groupBy(function ($val) {
        return Carbon::parse($val->date)->format('d');
    });
    $result = [];
    foreach ($program as $group) {
        $result[$group[0]->date] = $group->groupBy('venue');
    }
    return view('admin.pages.fantazia.program.program', ['program' => $result]);
}

    public function createprogramevent()
    {
        return view('admin.pages.fantazia.program.create');
    }

    public function storeprogramevent(Request $request)
    {
        Program::create($request->all());
        return redirect()->route('admin.fantazia.program');
    }

    public function editprogramevent($id)
    {
        $program = Program::findOrFail($id);
        return view('admin.pages.fantazia.program.edit', ['program' => $program]);
    }

    public function updateprogramevent(Request $request,$id)
    {
        $program = Program::findOrFail($id);
        $program->update($request->all());
        return redirect()->route('admin.fantazia.program');

    }

    public function deleteprogramevent($id)
    {
        Program::destroy($id);
        return redirect()->route('admin.fantazia.program');
    }

    public function events()
    {
        $events= Event::orderBy('date', 'ASC')->get();
        return view('admin.pages.fantazia.events.events', ['events' => $events]);
    }

    public function createevent()
    {
        return view('admin.pages.fantazia.events.create');
    }

    public function storeevent(Request $request)
    {
        $inputs = $request->all();
        $inputs['slug'] = str_slug($inputs['name']);
        Event::create($inputs);
        return redirect()->route('admin.fantazia.events');
    }

    public function editevent($id)
    {
        $event = Event::findOrFail($id);
        return view('admin.pages.fantazia.events.edit', ['event' => $event]);
    }

    public function updateevent(Request $request,$id)
    {
        $event = Event::findOrFail($id);
        $inputs = $request->all();
        $inputs['slug'] = str_slug($inputs['name']);
        $event->update($inputs);
        return redirect()->route('admin.fantazia.events');

    }

    public function deleteevent($id)
    {
        Event::destroy($id);
        return redirect()->route('admin.fantazia.events');
    }

    public function masterclasses()
    {
        $masterclasses = Masterclass::orderBy('date', 'ASC')->get();
        return view('admin.pages.fantazia.masterclasses.masterclasses', ['masterclasses' => $masterclasses]);
    }

    public function createmasterclass()
    {
        return view('admin.pages.fantazia.masterclasses.create');
    }

    public function storemasterclass(Request $request)
    {
        $inputs = $request->all();
        $inputs['slug'] = str_slug($inputs['name']);
        Masterclass::create($inputs);
        return redirect()->route('admin.fantazia.masterclasses');
    }

    public function editmasterclass($id)
    {
        $masterclass = Masterclass::findOrFail($id);
        return view('admin.pages.fantazia.masterclasses.edit', ['masterclass' => $masterclass]);
    }

    public function updatemasterclass(Request $request,$id)
    {
        $masterclass = Masterclass::findOrFail($id);
        $inputs = $request->all();
        $inputs['slug'] = str_slug($inputs['name']);
        $masterclass->update($inputs);
        return redirect()->route('admin.fantazia.masterclasses');

    }

    public function deletemasterclass($id)
    {
        Masterclass::destroy($id);
        return redirect()->route('admin.fantazia.masterclasses');
    }


}
