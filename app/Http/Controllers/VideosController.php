<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VideosController extends GlobalController
{
    public function show()
    {
        $videos = Video::orderBy('created_at', 'DESC')->paginate(9);
        if (!$videos->count()) {
            abort(404);
        }
        return view('pages.videos.videos', ['videos' => $videos]);

    }
}
