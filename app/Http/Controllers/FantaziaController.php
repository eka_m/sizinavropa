<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Fantazia\Event;
use App\Models\Fantazia\Fantazia;
use App\Models\Fantazia\Program;
use App\Models\Fantazia\Masterclass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;

class FantaziaController extends Controller
{
    public function show()
    {
        return view('pages.fantazia.home');
    }

    public function page($page)
    {
        if (View::exists('pages.fantazia.' . $page)) {
            return view('pages.fantazia.' . $page);
        } else {
            abort(404);
        }

    }

    public function films()
    {
        $films = Fantazia::orderBy('date', 'ASC')->get();
        return view('pages.fantazia.films.films', ['films' => $films]);
    }

    public function film($slug)
    {
        $film = Fantazia::where('slug', $slug)->firstOrFail();
        return view('pages.fantazia.films.film', ['film' => $film]);
    }

    public function program(Request $request)
    {
        $page = $request->get('page') ? $request->get('page') : 2;
        
        if($page > 2) {
            abort(404);
        }

        $weeks = [
            '1' => ['2018-09-18', '2018-10-31'],
            '2' => ['2018-11-01', '2018-11-10'],
            // '2' => ['2018-05-09', '2018-05-17'],
            // '3' => ['2018-05-14', '2018-05-17'],
            
        ];
        $program = Program::orderBy('date', 'ASC')
        ->whereBetween('date', $weeks[$page])->get()->groupBy(function ($val) {
            return Carbon::parse($val->date)->format('d');
        });
        $result = [];
        foreach ($program as $group) {
            $result[$group[0]->date] = $group->groupBy('venue');
        }
        return view('pages.fantazia.program.program',['program' => $result]);
    }

    public function events()
    {
        $events = Event::orderBy('date', 'ASC')->get();
        return view('pages.fantazia.events.events', ['events' => $events]);
    }

    public function event($slug)
    {
        $event = Event::where('slug', $slug)->firstOrFail();
        return view('pages.fantazia.events.event', ['event' => $event]);
    }

    public function masterclasses()
    {
        $masterclasses = Masterclass::orderBy('date', 'ASC')->get();
        return view('pages.fantazia.masterclasses.masterclasses', ['masterclasses' => $masterclasses]);
    }

    public function masterclass($slug)
    {
        $masterclass = Masterclass::where('slug', $slug)->firstOrFail();
        return view('pages.fantazia.masterclasses.masterclass', ['masterclass' => $masterclass]);
    }

    public function bakuHeritage()
    {
        $images = Storage::disk('uploads')->files('images/fantazia-baku-heritage');
        return view('pages.fantazia.heritage.baku',compact('images'));
    }
    public function europeanHeritage()
    {
        return view('pages.fantazia.heritage.europe');
    }
}
