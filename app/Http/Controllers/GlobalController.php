<?php

namespace App\Http\Controllers;


use App\Models\Article;
use App\Models\Locale;
use App\Models\Page;
use App\Models\Gallery;

use Illuminate\Http\Request;
use View;
use DOMDocument;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


class GlobalController extends Controller
{
    protected $locale = 'az';

    public function __construct(Request $request)
    {
        $this->locale = LaravelLocalization::getCurrentLocale();
        $clearpages = Page::orderBy('pos', 'ASC')->where('place', 'mainmenu')->get();
        $toppages = Page::orderBy('pos', 'ASC')->where('place', 'topmenu')->where('id', '!=', 6)->get();
        $allLocales = Locale::orderBy('pos', 'ASC')->where('slug', '!=', $this->locale)->get();
        $pages = self::mapTree($clearpages);
        $pages = self::prepareMenu($pages, 0, $request->url());
        $interesting = $this->interesting();
        // $interesting = $this->setNewsLang($interesting);
        View::share('pages', $pages);
        View::share('toppages', $toppages);
        View::share('clearpages', $clearpages);
        View::share('interesting', $interesting);
        View::share('currentlocale', $this->locale);
        View::share('locales', $allLocales);
    }

    private static function mapTree($dataset, $parent = 0)
    {
        $tree = array();
        foreach ($dataset as $id => $node) {
            if ($node->parent_id != $parent) continue;
            $node->children = self::mapTree($dataset, $node->id);
            $tree[$id] = $node;
        }
        return $tree;
    }

    private static function prepareMenu($array, $i = 0, $path)
    {


        $tree = '<ul id="main-menu" class="sm sm-simple">';
        if ($i > 0) {
            $tree = '<ul>';
        }


        foreach ($array as $item) {
            $class = ends_with($path, $item->url) ? 'class="active"' : '';
            $tree .= '<li><a href="/page/' . $item->url . '" ' . $class . '>' . $item->name . '</a>';
            if ($item->children && !empty($item->children)) {
                $tree .= self::prepareMenu($item->children, $i + 1, $path);
            }
            $tree .= '</li>';
            $i++;
        }
        $tree .= '</ul>';
        return $tree;
    }

    private function interesting()
    {
        $data = Locale::where('slug', $this->locale)
            ->with(['articles' => function($query){
                $query->select('id', 'page_id', 'short', 'views', 'fakeviews', 'name', 'url', 'image', 'person', 'interesting', 'status', 'created_at', 'pos', 'locale_id')
                ->where('interesting', 1)
                ->where('status', 1)
                ->with('page')
                ->orderBy('pos', 'DESC')
                ->limit(3);
            }])->firstOrfail();
        return $data->articles;
    }

    protected function setNewsLang($news)
    {
        foreach ($news as $key => $value) {
            if ($value['children']) {
                foreach ($value['children'] as $child) {
                    if ($child['locale']['slug'] == $this->locale) {
                        $news[$key] = $child;
                    }
                }
            }
        }
        return $news;
    }

    protected function getGallery($html)
    {
        if(!$html && $html == '') return $html;
        $libxmlPreviousState = libxml_use_internal_errors(true);
        $dom = new DOMDocument;
        $dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
        libxml_clear_errors();
        libxml_use_internal_errors($libxmlPreviousState);
        $elements = $dom->getElementsByTagName('gallery');
        $gallery = [];
        for ($i = $elements->length - 1; $i >= 0; $i--) {
            $element = $elements->item($i);
            $id_element = $element->getAttribute("id");
            $dbgallery =  Gallery::find($id_element);
            if(!$dbgallery) return $html;
            $gallery[] =$dbgallery;
            $params = json_decode($dbgallery->params, true);
            $view = View::make('partials.unite-gallery', ["gallery" => $dbgallery, "params" => $params]);
            $template = $view->render();
            $template = (string)$view;
            if ($template) {
                $newdoc = new DOMDocument;
                $newdoc->loadHTML(mb_convert_encoding($template, 'HTML-ENTITIES', 'UTF-8'));
                $element->parentNode->replaceChild($dom->importNode($newdoc->documentElement, TRUE), $element);
            }
        }
        return ["content" => $dom->saveHTML(), 'gallery' => empty($gallery) ? [] : $gallery];
    }

}