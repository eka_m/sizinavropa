<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Locale;

class PagesController extends GlobalController
{
    public function show($url)
    {
        $page = Page::where('url', $url)->firstOrFail();
        return call_user_func_array([$this, $page->type.'Page'], [$page]);
    }

    public function routePage($page) {
        return redirect('/'.$this->locale.'/'.$page->url);
    }

    public function regularPage($page) {
        $articles = $page->articles()
        ->orderBy('created_at', 'DESC')
        ->where('status',1)
        ->where('locale_id', Locale::where('slug', $this->locale)->first()->id)
        ->with('locale')
        ->paginate(6);
        $parsedContent = $this->getGallery($page->content);
        $page->content = $parsedContent['content'];
        return view('pages.pages.page', ['page' => $page, 'articles' => $articles, 'gallery'=> $parsedContent['gallery']]);
    }

}