<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Locale;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;

class EventsController extends GlobalController
{
	public function show ()
	{
		$events = $this->program();
		return view( 'pages.events.events', [ 'events' => $events ] );
	}

	public function calendar ()
	{
		return view( 'pages.events.calendar', [ 'venuesandevents' => $this->oneDayEvents( Carbon::now()->format( 'Y-m-d' ) ) ] );
	}

	public function allEvents ()
	{
		$data = Locale::where( 'slug', $this->locale )->with( [ 'articles' => function ( $query ) {
			$query->select( 'id', 'locale_id', 'name', 'url', 'date', 'event_name', 'date', 'start', 'end', 'end_date', 'venue' )
				->where( 'status', 1 )
				->where( 'venue', '!=', null )
				->orderBy( 'date', 'ASC' );
		} ] )->firstOrfail();

		$articles = $this->dateRange( $data->articles );

		$allevents = $articles->groupBy( function ( $val ) {
			return Carbon::parse( $val->date )->format( 'm-d-Y' );
		} );

		foreach ( $allevents as $date => $events ) {
			$allevents[ $date ] = $events->groupBy( 'venue' );
		}

		return response()->json( $allevents, 200 );
	}

	public function asyncOneDayEvents ( $day )
	{
		$day = Carbon::parse( $day )->format( 'Y-m-d' );
		return response()->json( $this->oneDayEvents( $day ), 200 );
	}

	private function oneDayEvents ( $day )
	{
		$data = Locale::where( 'slug', $this->locale )->with( [ 'articles' => function ( $query ) use ( $day ) {
			$query->select( 'id', 'locale_id', 'name', 'url', 'date', 'event_name', 'date', 'start', 'end', 'venue' )
				->where( 'status', 1 )
				->where( 'venue', '!=', null )
				->where( 'date', $day )
				->orderBy( 'date', 'ASC' );
		} ] )->firstOrfail();

		return collect( $data->articles )->groupBy( 'venue' );
	}

	private function program ()
	{

		$data = Locale::where( 'slug', $this->locale )->with( [ 'articles' => function ( $query ) {
			$query->select( 'locale_id', 'name', 'url', 'date', 'event_name', 'date', 'start', 'end', 'end_date', 'venue' )
				->orderBy( 'date', 'ASC' )
				->where( 'status', 1 )
				->where( 'venue', '!=', null )
				->where( function ( $q ) {
//                    $q->whereBetween('date', [Carbon::now()->startOfWeek()->format('Y-m-d'), Carbon::now()->endOfWeek()->format('Y-m-d')]);
//                        ->orWhereBetween('end_date', [Carbon::now()->startOfWeek()->format('Y-m-d'), Carbon::now()->endOfWeek()->format('Y-m-d')]);
					$q->past( 'date', Carbon::now()->endOfWeek()->format( 'Y-m-d' ) )->future( 'end_date', Carbon::now()->startOfWeek()->format( 'Y-m-d' ) );
				} );
		} ] )->firstOrfail();

		$articles = $this->dateRange( $data->articles, true );

		$currentWeekArticles = $articles->groupBy( function ( $val ) {
			return Carbon::parse( $val->date )->format( 'N' );
		} );

		foreach ( $currentWeekArticles as $key => $group ) {
			$currentWeekArticles[ $key ] = $group->groupBy( 'venue' );
		}

		return $currentWeekArticles;
	}

	private function dateRange ( $articles, $thisweek = false )
	{
		$result = collect( [] );
		foreach ( $articles as $key => $item ) {
			if ( $item->end_date != null ) {
				$period = new DatePeriod(
					new DateTime( $item->date ),
					new DateInterval( 'P1D' ),
					new DateTime( $item->end_date )
				);
				foreach ( $period as $value ) {
					$date = $value->format( 'Y-m-d' );
					if ( $thisweek ) {
						if ( $date >= Carbon::now()->startOfWeek()->format( 'Y-m-d' ) && $date <= Carbon::now()->endOfWeek()->format( 'Y-m-d' ) ) {
							$result->push( $this->cloneEvent( $item, $date ) );
						}
					} else {
						$result->push( $this->cloneEvent( $item, $date ) );
					}
				}
			} else {
				$result->push( $this->cloneEvent( $item, $item->date ) );
			}
		}
		return $result;
	}

	private function cloneEvent ( $item, $date )
	{
		$event = new Article;
		$event->location_id = $item->location_id;
		$event->event_name = $item->event_name;
		$event->url = $item->url;
		$event->name = $item->name;
		$event->venue = $item->venue;
		$event->start = $item->start;
		$event->end = $item->end;
		$event->date = $date;
		return $event;
	}
}
