<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['name', 'content', 'parent_id', 'url', 'keywords', 'description', 'img', 'cover','social', 'coveropacity', 'place', 'type'];

    protected $table = 'pages';

    public function parent()
    {
        return $this->belongsTo('App\Models\Admin\Page', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Admin\Page', 'parent_id', 'id');
    }

    public static function tree()
    {
        return static::with(implode('.', array_fill(0, 4, 'children')))->where('parent_id', '=', NULL)->get();
    }
}
