<?php

namespace App\Models\Admin\Fantazia;

use Illuminate\Database\Eloquent\Model;

class Masterclass extends Model
{
    protected $table = 'fantazia_masterclass';

    protected $fillable = ['name', 'content', 'date', 'slug', 'image', 'venue'];
}
