<?php

namespace App\Models\Admin\Fantazia;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 'fantazia_program';
    protected $fillable = ['title', 'date','start','end', 'venue'];
}
