<?php

namespace App\Models\Admin\Fantazia;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'fantazia_events';
    protected $fillable = ['name', 'content', 'date', 'slug', 'image', 'venue'];
}
