<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'name',
        'short',
        'content',
        'status',
        'person',
        'interesting',
        'page_id',
        'url',
        'keywords',
        'description',
        'image',
        'cover',
        'views',
        'fakeviews',
        'popup',
        'coveropacity',
        'locale_id',
        'parent_id',
        'pos',
        'date',
        'end_date',
        'venue',
        'start',
        'end',
        'event_name'
    ];

    protected $table = 'articles';

    // protected $casts = ['image' => 'array'];

    public function setImageAttribute($value) {
        $this->attributes['image'] = json_encode($value);
    }
    
    public function setCoverAttribute($value) {
        $this->attributes['cover'] = json_encode($value);
    }

    public function page()
    {
        return $this->belongsTo('App\Models\Admin\Page', 'page_id')->select(['name', 'id']);
    }

    public function locale () {
        return $this->belongsTo('App\Models\Admin\Locale');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Admin\Article', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Admin\Article', 'parent_id', 'id');
    }

}
