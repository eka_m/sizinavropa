<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Masterclass extends Model
{
    protected $fillable = ['name', 'content', 'date', 'slug', 'image', 'venue'];
}
