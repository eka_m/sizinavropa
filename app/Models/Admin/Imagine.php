<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Imagine extends Model
{
    protected $fillable = ['title', 'org_title', 'year', 'country', 'length', 'genre', 'director', 'writers', 'cast', 'synopsis', 'date', 'venue', 'lang', 'subtitle', 'poster', 'slug', 'trailer'];
    protected $table = 'imagine';
}
