<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 'program';
    protected $fillable = ['title', 'date','start','end', 'venue'];
}
