<?php

namespace App\Models\Fantazia;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 'fantazia_program';
}
