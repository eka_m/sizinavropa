<?php

namespace App\Models\Fantazia;

use Illuminate\Database\Eloquent\Model;

class Fantazia extends Model
{
    protected $table = 'fantazia';
}
