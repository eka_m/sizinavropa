<?php

namespace App\Models\Fantazia;

use Illuminate\Database\Eloquent\Model;

class Masterclass extends Model
{
    protected $table = 'fantazia_masterclasses';
}
