<?php

namespace App\Models;

use Carbon\Carbon;
use App\Traits\ImageManager;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use ImageManager;

    public $imageManager = ['image', 'cover'];

    public function page() {
        return $this->belongsTo('App\Models\Page');
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('Y/m/d');
    }

    public function locale () {
        return $this->belongsTo('App\Models\Locale');
    }

    public function parent() {
        return $this->belongsTo('App\Models\Article', 'parent_id');
    }

    public function children() {
        return $this->hasMany('App\Models\Article', 'parent_id', 'id');
    }
}
