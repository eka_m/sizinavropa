<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Query\Builder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	    $this->initPastFutureMacro();
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

	private function initPastFutureMacro()
	{
		Builder::macro('past', function ($column, $when = 'now', $strict = false) {
			$when = Carbon::parse($when);
			$operator = $strict ? '<' : '<=';
			return $this->where($column, $operator, $when);
		});
		Builder::macro('future', function ($column, $when = 'now', $strict = false) {
			$when = Carbon::parse($when);
			$operator = $strict ? '>' : '>=';
			return $this->where($column, $operator, $when);
		});
	}
}
