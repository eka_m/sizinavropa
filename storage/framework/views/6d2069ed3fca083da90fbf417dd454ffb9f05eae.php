<?php $__env->startSection('title', 'IMAGINE 2018'); ?>
<?php $__env->startSection('css'); ?>
##parent-placeholder-2f84417a9e73cead4d5c99e05daff2a534b30132##
<style>
.about-text .list p {
    font-size:16px;
    margin: 2px;
    font-family: 'Exo 2',sans-serif;
}
.about-text .list {
    margin-bottom: 20px;
}
.about-text .list p  .tire{
    font-weight: bold;
    display: inline-block;
    margin-right: 10px;
}
.about-text h5 {
margin: 5px;
font-family: 'Exo 2',sans-serif;
}
</style>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
        <?php echo $__env->make('pages.imagine.nav',['active' => 'about'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
                <a href="<?php echo e(route('imagine')); ?>"><img src="<?php echo e(asset('img/imagine/imagine-new.png')); ?>" alt=""></a>
            </div>
        </div>
        <div class="uk-container-small bg-white uk-margin-auto uk-padding-small" >
            <div class="page-title uk-text-center">
                <h1 class="uk-heading-line uk-h1"><span>IMAGINE 2018 - HIGHLIGHTS</span></h1>
            </div>
            <div class="page-content about-text">
                <h5><strong>A.     Documentary Film Festival</strong><h5>
                    <div class="list">
                            <p><span class="tire">-</span>40 films from 30 countries</p>
                            <p><span class="tire">-</span>Venues: Park Cinema, the Landmark and Yarat Contemporary Art Space;</p>
                            <p>Film Directors to be invited: Ieva Ozolina (Latvia), Peter Lataster (Netherlands), Piotr Stasik (Poland), Kenneth Michiels (Belgium), Pieter-Jan de Pue (Belgium), Giedre Žickyte (Lithuania), Xavier Solano (Belgium);</p>
                            <p><span class="tire">-</span>Cooperation with Adam Mickiewicz Institute; Piotr Stasik (Poland) will present his movie “21 x New York”;</p>
                            <p><span class="tire">-</span>Cooperation with CINEX which brought to Baku a series of excellent films, including the world acclaimed "Land of the Enlightened";</p>
                            <p><span class="tire">-</span>Many awarded films such as: “Sign” from Hungary (6 international awards, including “Olympia International Film Festival for Children and Young People – Greece”, “Short Shorts Film Festival & Asia in Japan”, “TIFF Kids - Toronto International Film Festival”, “Chicago International Children's Film Festival and 89th Academy Awards for best live action short film in 2017;</p>
                            <p><span class="tire">-</span>Almost all EU Member States participate at IMAGINE 2018; we are however happy to have films from non-EU countries, namely from Argentina, Brazil, Costa Rica, Ecuador, Peru, Mexico, Israel, Switzerland, Norway, the US, Moldova and Serbia;</p>

                    </div>
                    <h5><strong> B.    Conferences and master classes with special guests</strong><h5>
                    <div class="list">
                        <p><span class="tire">-</span>All invited film directors will deliver master classes; more information about the venues in the programme;</p>
                        <p><span class="tire">-</span>Master classes will be held in the local universities such as Azerbaijani National University of Culture and Arts, ADA University and Azerbaijan University of Languages;</p>
                        <p><span class="tire">-</span>This year, we have a special partnership with Ca-Foscari University of Venice, Italy with a special guest, PhD Carlo Frappi; Dr. Frappi with long research experience regarding multiculturalism and intercultural dialogue, including in Azerbaijan, will deliver a speech on the topic: "Identity and Foreign Policy. The case for Azerbaijani Multiculturalism"; </p>
                        <p><span class="tire">-</span>Belgian director Xavier Solano is returning to Baku after IMAGINE 2017; he will present his experience with the project MEMORY LAB, promoting the integration of different communities through movie making, a project supported by Belgium's Flemish community; Solano will also present "Quijotes negros", a film by Ecuador's director Sandino Burbano, who studied and worked in Baku in the early 90's; Burbano has given an interview on his memories from Baku.</p>
                    </div>

                    <h5><strong>C.    Concerts</strong><h5>
                        <div class="list">
                            <p><span class="tire">-</span>Music, together with film making, is a universal language representing one of the most powerful tools able to foster mutual understanding and cooperation among cultures and people. Ethnic music in particular has always been the core of cultural identity, a "story telling" through sounds and words, in order to connect people’s life to their roots. Since ancient times, we have had several samples of cultural cooperation between west and east, especially in the field of music, and this is the kind of dialogue that we would like to resurface.</p>
                            <p><span class="tire">-</span>IMAGINE 2018 is proud to host two ethnic concerts reflecting the values of tolerance and intercultural dialogue;</p>
                            <p><span class="tire">-</span>What could be the common ground between musicians from Peru, Greece and Azerbaijani Mugam? This is a question we want to answer in the concerts of May 4th and May 6th at 8 pm in Mugham Centre;</p>
                            <p><span class="tire">-</span>Peruvian musicians Karl Struyf and Alejandro Rivas know Baku, they have already performed and are passionate about Mugham;</p>
                            <p><span class="tire">-</span>Greek rembetiko star Makis Stamoulis and his friend Kostas Spyropoulos have performed twice in Baku and have managed to create bridges with the Azerbaijani public;</p>
                            <p><span class="tire">-</span>Tickets for both concerts are available at iticket.az for a fee; a limited number of free tickets will be available for media representatives.</p>
                        </div>

                    <h5><strong>D.    Europe Day</strong><h5>
                        <div class="list">
                            <p><span class="tire">-</span>Europe Day will be celebrated with music and images this year at the Opera Studio, in Baku on May 7th;</p>
                            <p><span class="tire">-</span>The concert featuring many surprises will include pieces from leading European composers performed by talented Azerbaijani musicians; Visual aids will guide us in this journey;</p>
                            <p><span class="tire">-</span>The concert will be recorded and a number of CDs will be produced to commemorate a very special day indeed.</p>
                        </div>

                        <h5><strong>E.    Theatre Performance</strong><h5>
                            <div class="list">
                                <p><span class="tire">-</span>Local artists and musicians will create a special “European Square”, providing an open space theatre performance;<p>
                                <p><span class="tire">-</span>The performance will be open and all interested will sit around a table and would also be able to enjoy snacks and wine.<p>
                            </div>

                            <h5>For more details:<h5>
                            <div class="list">
                                    <p><a hef="https://sizinavropa.az/imagine">Sizinavropa.az</a><span class="tire">&nbsp;&nbsp;</span>
                                    <a href="https://www.facebook.com/ImagineEuroToleranceFestival" target="_blank">Facebook</a></p>
                            </div>
                           
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.imagine', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>