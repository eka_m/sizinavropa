<?php $__env->startSection('title','Eventa Calendar'); ?>
<?php $__env->startSection('css'); ?>
    ##parent-placeholder-2f84417a9e73cead4d5c99e05daff2a534b30132##
    <link rel="stylesheet" href="<?php echo e(asset('plugins/calendario/calendario.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/calendario/custom_calendario_theme_2.min.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    ##parent-placeholder-93f8bb0eb2c659b85694486c41717eaf0fe23cd4##
    <script src="<?php echo e(asset('plugins/calendario/jquery.calendario.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/calendario/calendario-setup.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/sticky/sticky.min.js')); ?>"></script>
    <script>
        $(".custom-calendar-full").stick_in_parent({
            offset_top:30
        });
    </script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section class="row row-content gradient-bg mt-3 no-gutters">
        <div class="col-12 col-md-6 px-3 py-3 px-lg-5 py-lg-5 custom-calendar-full-container">
            <div class="custom-calendar-full">
                <div class="custom-month-year">
                        <span id="custom-prev" class="custom-prev"><i class="sa-angle-left"></i></span>
                        <span id="custom-month" class="custom-month"></span>
                        <span id="custom-year" class="custom-year"></span>
                        <span id="custom-next" class="custom-next"><i class="sa-angle-right"></i></span>
                        <nav>
                            <span id="custom-current" class="custom-current" title="Go to current date">---</span>
                        </nav>
                </div>
                <div id="fullcalendar" class="fc-calendar-container"></div>
            </div>
        </div>
        <div class="col-12 col-md-6 px-3 py-3 px-lg-5 py-lg-5 custom-calendar-day">
            <div class="custom-calendar-day-current_date">
                <?php 
                    $day = date('N') != 0 ? date('N') : 7;
                 ?>
                <span class="fullcalendar-current-weekday"><?php echo e(__('words.week.'.$day)); ?></span>,
                <span class="fullcalendar-current-day"><?php echo e(date('j')); ?><span>
                <span class="fullcalendar-current-month"><?php echo e(__('words.months.'.date('n'))); ?></span>
            </div>
            <div class="custom-calendar-day-events">
                <?php $__empty_1 = true; $__currentLoopData = $venuesandevents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $venue => $events): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div class="fullcalendar-venue"><?php echo e($venue); ?></div>
                    <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="fullcalendar-event">
                            <span>
                                <?php echo e($event->start ? $event->start.' / ' : '-- '); ?>

                                <?php echo e($event->end ? $event->end : '--'); ?>

                            </span>
                        <a href="/article/<?php echo e($event->url); ?>"><?php echo $event->event_name ? $event->event_name : $event->name; ?></a>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <span class="fullcalendar-noevents"><?php echo e(__('words.noevents')); ?></span>
                <?php endif; ?>
            </div>
            
           
            <div id="fullcalendar-spinner">
                <div class="sk-cube-grid">
                    <div class="sk-cube sk-cube1"></div>
                    <div class="sk-cube sk-cube2"></div>
                    <div class="sk-cube sk-cube3"></div>
                    <div class="sk-cube sk-cube4"></div>
                    <div class="sk-cube sk-cube5"></div>
                    <div class="sk-cube sk-cube6"></div>
                    <div class="sk-cube sk-cube7"></div>
                    <div class="sk-cube sk-cube8"></div>
                    <div class="sk-cube sk-cube9"></div>
                  </div>
            </div>
           
        </div>
    </section>
    <div class="clearfix"></div>    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>