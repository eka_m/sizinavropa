<footer class="row footer gradient-bg mt-5 p-3">
    <div class="col footer__col row-content">
        <div class="row footer__row">
            <div class="col-xs-12 col-md-4 about order-2 order-md-1">
                <div class="row footer__row__header justify-content-center">
                    <div class="col">
                        <h3><?php echo e(__('words.about')); ?></h3>
                    </div>
                </div>
                <div class="row about__content pr-2 pb-3">
                    <div class="col" style="font-size: 1.2em;">
                        <?php echo __('words.short-about'); ?>

                    </div>
                </div>
                <div class="row about__social">
                    <div class="col">
                        <a href="" class="about__social__link about__social__link--fb"><i class="sa-facebook"></i></a>
                        <a href="" class="about__social__link about__social__link--yt"><i class="sa-youtube"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 mostRead order-1 order-md-2">
                <div class="row footer__row__header">
                    <div class="col">
                        <h3><?php echo e(__('words.mostread')); ?></h3>
                    </div>
                </div>
                <div class="row mostRead__items">
                    <div class="col">
                        <?php $__currentLoopData = $interesting->slice(0, 3); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <article class="row mostRead__items__item">
                                <div class="col-4">
                                    <img src="<?php echo e(setImage($article->image, 'interesting')); ?>" class="fluid image" alt="">
                                </div>
                                <header class="col-8 mostRead__items__item__info">
                                    <a class="link_reset" href="/article/<?php echo e($article->url); ?>"><h3><?php echo e($article->name); ?></h3></a>
                                </header>
                            </article>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-3 offset-md-1 footerLinks order-3 order-md-3">
                <div class="row footer__row__header">
                    <div class="col">
                        <h3><?php echo e(__('words.links')); ?></h3>
                    </div>
                </div>
                <div class="row footerLinks__row">
                    <div class="col footerLinks__row__links">
                        <?php $__currentLoopData = $clearpages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <a class="footerLinks__row__links__link" href="/<?php echo e($currentlocale); ?>/page/<?php echo e($page->url); ?>">
                                    <?php if($currentlocale == env('DEFAULTLOCALE')): ?><?php echo e($page->name); ?><?php else: ?> <?php echo e($page->name_en); ?><?php endif; ?>
                                </a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php $__currentLoopData = $toppages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $toppage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <a class="footerLinks__row__links__link" href="/<?php echo e($currentlocale); ?>/page/<?php echo e($toppage->url); ?>">
                                    <?php if($currentlocale == env('DEFAULTLOCALE')): ?><?php echo e($toppage->name); ?><?php else: ?> <?php echo e($toppage->name_en); ?><?php endif; ?>
                                </a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row footer__row">
            <div class="col tx-center">
                <hr class="cl--white snowCollect">
                <span class="d-block"><?php echo e(__('words.copyright')); ?></span>
                <span class="d-block"> 2017 © SizinAvropa.az</span>
            </div>
        </div>
    </div>
</footer>
