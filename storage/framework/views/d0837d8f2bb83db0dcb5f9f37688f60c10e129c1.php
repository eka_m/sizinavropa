<!-- Twitter Card data -->

  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="Sizin Avropa">
  <meta name="twitter:title" content="<?php echo e($title); ?>">
  <meta name="twitter:description" content="<?php echo e($description); ?>">
  <meta name="twitter:creator" content="Sizin Svropa">
  <meta name="twitter:image:src" content="https://sizinavropa.az<?php echo e($image); ?>">
  <meta name="twitter:domain" content="sizinavropa.az"/>

  <!-- Open Graph data -->
  <meta property="og:title" content="<?php echo e($title); ?>" />
  <meta property="og:type" content="article" />
  <meta property="og:url" content="<?php echo e($url); ?>" />
  <meta property="og:image" content="https://sizinavropa.az<?php echo e($image); ?>" />
  <meta property="og:description" content="<?php echo e($description); ?>" />
  <meta property="og:site_name" content="Sizin Avropa" />