


<div id="gallery<?php echo e($gallery->id); ?>" class="gallery<?php echo e($gallery->id); ?> unite-gallery" style="display:none; margin: 0 auto">
    <?php $__currentLoopData = json_decode($gallery->images); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <img src="/uploads<?php echo e($item->path); ?>"
             alt="<?php echo e($item->title); ?>"
             data-image="/uploads<?php echo e($item->path); ?>">
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

