<?php $__env->startSection('title', '::New Page'); ?>
<?php $__env->startSection('js'); ?>
    ##parent-placeholder-93f8bb0eb2c659b85694486c41717eaf0fe23cd4##
    <script type="text/javascript" src="<?php echo e(asset('plugins/tinymce/tinymce.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('plugins/tinymce/init.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="uk-section">
        <div class="uk-container head-font">
            <form action="<?php echo e(route('pages.update', $page->id)); ?>" method="POST">
                <?php echo e(method_field('PUT')); ?>

                <?php echo e(csrf_field()); ?>

                <ul class="uk-subnav uk-subnav-pill" uk-switcher="animation: uk-animation-slide-bottom-medium">
                    <li><a href="#">Title & Cover</a></li>
                    <li><a href="#">Content</a></li>
                    <li><a href="#">SEO</a></li>
                </ul>
                <ul class="uk-switcher uk-margin">
                    <li>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Name</div>
                            <input type="text" class="uk-input uk-form-large" id="translit-it" name="name" placeholder="Page name"
                                   value="<?php echo e($page->name); ?>">
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Slug</div>
                            <slug inputname="url" oldval="<?php echo e($page->url); ?>"></slug>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Placement</div>
                            <select class="uk-select uk-form-large" name="place">
                                <option value="mainmenu"   <?php if($page->place == 'mainmenu'): ?> selected <?php endif; ?>>Main menu</option>
                                <option value="topmenu" <?php if($page->place == 'topmenu'): ?> selected <?php endif; ?>>Top menu</option>
                            </select>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Type</div>
                            <select class="uk-select uk-form-large" name="type">
                                <option value="regular" <?php if($page->type == 'regular'): ?> selected <?php endif; ?>>Regular</option>
                                <option value="route" <?php if($page->type == 'route'): ?> selected <?php endif; ?>>Route</option>
                                <option value="custom" <?php if($page->type == 'custom'): ?> selected <?php endif; ?>>Custom</option>
                            </select>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Parent</div>
                            <select class="uk-select uk-form-large" name="parent_id">
                                <option value="0">No parent</option>
                                <?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($page->id === $item->id) continue; ?>
                                    <option value="<?php echo e($item->id); ?>"
                                            <?php if($page->parent && $page->parent->id === $item->id): ?> selected <?php endif; ?>><?php echo e($item->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="uk-margin">
                            <imageinput inputname="cover" img="<?php echo e($page->cover); ?>" btn="Choose cover"></imageinput>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Cover overlay opacity</div>
                            <range class="uk-form-width-small" name="coveropacity" min="0" max="1" step="0.1" default="<?php echo e($page->coveropacity); ?>"></range>
                        </div>
                    </li>
                    <li>
                        <textarea name="content" id="editor" cols="30" rows="10" class="uk-textarea"><?php echo $page->content; ?></textarea>
                    </li>
                    <li>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Keywords</div>
                            <input type="text" class="uk-input uk-form-large" name="keywords" placeholder="Keywords"
                                   value="<?php echo e($page->keywords); ?>">
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Description</div>
                            <textarea name="description" class="uk-textarea uk-form-large" rows="5"
                                      placeholder="Description"><?php echo e($page->description); ?></textarea>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Social buttons</div>
                            <select name="social" class="uk-select uk-form-width-xsmall">
                                <option value="0" <?php if(!$page->social): ?> selected <?php endif; ?> >OFF</option>
                                <option value="1" <?php if($page->social): ?> selected <?php endif; ?>>ON</option>
                            </select>
                        </div>
                    </li>
                </ul>
                <div class="uk-margin">
                    <a href="<?php echo e(route('pages.index')); ?>" class="uk-button uk-button-danger uk-float-left">Exit without
                        saving</a>
                    <button class="uk-button uk-button-secondary uk-float-right">Save</button>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>