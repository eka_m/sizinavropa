<?php $__env->startSection('title','::Pages'); ?>
<?php $__env->startSection('content'); ?>
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <a href="<?php echo e(route('pages.create')); ?>" class="uk-icon-button uk-button-secondary uk-float-right" uk-icon="icon: plus"></a>
        </div>
        <div class="uk-container uk-container-small">
            <tree :pages="<?php echo e(json_encode($pages)); ?>"></tree>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>