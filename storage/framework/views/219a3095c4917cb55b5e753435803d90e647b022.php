<?php $__env->startSection('title', '::Videos'); ?>
<?php $__env->startSection('content'); ?>
    <videos data="<?php echo e(json_encode($videos)); ?>"></videos>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>