
<div class="social-share-container gradient-bg">
  <!-- Sharingbutton Facebook -->
  <a class="social-share-link social-share-popup" href="https://facebook.com/sharer/sharer.php?u=<?php echo e($url); ?>"
     target="_blank" aria-label="Share on Facebook">
    <img src="<?php echo e(asset('img/social_buttons/facebook.svg')); ?>" alt="">
  </a>

  <!-- Sharingbutton Twitter -->
  <a class="social-share-link social-share-popup"
     href="https://twitter.com/intent/tweet/?text=<?php echo e($title); ?>&amp;url=<?php echo e($url); ?>" target="_blank"
     aria-label="Share on Twitter">
    <img src="<?php echo e(asset('img/social_buttons/twitter.svg')); ?>" alt="">
  </a>

  <!-- Sharingbutton Google+ -->
  <a class="social-share-link social-share-popup" href="https://plus.google.com/share?url=<?php echo e($url); ?>"
     target="_blank" aria-label="Share on Google+">
    <img src="<?php echo e(asset('img/social_buttons/google-plus.svg')); ?>" alt="">
  </a>

  <!-- Sharingbutton WhatsApp -->
  <a class="social-share-link" href="whatsapp://send?text=<?php echo e($title); ?>%20<?php echo e($url); ?>" target="_blank"
     aria-label="Share on WhatsApp">
    <img src="<?php echo e(asset('img/social_buttons/whatsapp.svg')); ?>" alt="">
  </a>
</div>

