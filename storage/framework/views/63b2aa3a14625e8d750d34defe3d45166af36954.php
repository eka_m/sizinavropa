<?php $__env->startSection('title','Weekly Events'); ?>

<?php $__env->startSection('content'); ?>

    <section class="row row-content events mt-3 no-gutters">
        <div class="col">
            <div class="row">
                <div class="col">
                <a href="/<?php echo e($currentlocale); ?>/full-calendar" style="font-size:20px; float:right;" class="tx--white d-inline-block p-3 m-2 bg--blue"><?php echo e(__('words.calendar')); ?> > </a>
                </div>
            </div>
            <div class="row">
                <?php $__currentLoopData = trans('words.week'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $daynumber => $dayname): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col  events__item p-1">
                    <div class="events__item__day bg--orange tx--white">
                        <h3><?php echo e($dayname); ?></h3>
                        <?php echo e(Carbon\Carbon::createFromTimestamp(strtotime($daynumber.' day this week') - (24*60*60))->day); ?>

                        <?php echo e(__('words.months.'.Carbon\Carbon::createFromTimestamp(strtotime($daynumber.' day this week') - (24*60*60))->month)); ?>

                    </div>
                    <?php if(isset($events[$daynumber])): ?>
                        <?php $__currentLoopData = $events[$daynumber]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="events__item__venue">
                                <p class="events__item__venue__name"><?php echo e($k); ?></p>
                                <ul class="events__item__venue__event">
                                    <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <?php if($item->start && $item->end): ?>
                                                <span class="events__item__venue__event__time">
                                                <span>
                                                    <?php echo e(date("H:i", strtotime($item->start)).' - '); ?><?php echo e(date("H:i", strtotime($item->end))); ?>

                                                </span>
                                            </span>
                                            <?php endif; ?>
                                            <div class="events__item__venue__event__name">
                                                <?php if($item->event_name): ?>
                                                    <?php echo $item->event_name; ?>

                                                <?php else: ?>
                                                    <a href="<?php echo e(route('article',$item->url)); ?>"><?php echo e($item->name); ?></a>
                                                <?php endif; ?>
                                            </div>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <?php if($events->isEmpty()): ?>
            <div class="row">
                <div class="col tx-center p-5">
                    <p><?php echo e(__('words.noevents')); ?></p>
                </div>
            </div>
            <?php endif; ?>
        </div>

    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>