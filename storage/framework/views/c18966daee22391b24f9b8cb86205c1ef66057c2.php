<div class="uk-container-large uk-padding-remove">
    <div class="main-nav">
        <div class="topnav" id="myTopnav">
            <a href="<?php echo e(route('imagine')); ?>">Home</a>
            <a href="<?php echo e(route('imagine.page','about')); ?>" <?php if($active == 'about'): ?> class="active" <?php endif; ?>>About</a>
            <a href="<?php echo e(route('imagine.program')); ?>" <?php if($active == 'programs'): ?> class="active" <?php endif; ?>>Programs</a>
            <a href="<?php echo e(route('imagine.films')); ?>" <?php if($active == 'movies'): ?> class="active" <?php endif; ?>>Movies</a>
            <a href="<?php echo e(route('imagine.events')); ?>" <?php if($active == 'events'): ?> class="active" <?php endif; ?>>Events</a>
            
            <a href="<?php echo e(route('imagine.page','partners')); ?>" <?php if($active == 'partners'): ?> class="active" <?php endif; ?>>Partners</a>
            <a href="javascript:void(0);" style="font-size:32px;" class="icon" onclick="myFunction()">&#9776;</a>
        </div>
    </div>
</div>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
</script>
