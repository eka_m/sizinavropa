<section class="row subscribe gradient-bg p-3 mt-3 align-items-center">
    <div class="col row-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 subscribe__text pl-lg-3">
                <p><?php echo e(__('words.subscribe')); ?></p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 subscribe__input">
                <form action="">
                    <input type="text" placeholder="<?php echo e(__('words.enteremail')); ?>">
                </form>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 subscribe__button pr-lg-5">
                <a href="#"><?php echo e(__('words.send')); ?> <span class="rarr">&rarr;</span></a>
            </div>
        </div>
    </div>
</section>
