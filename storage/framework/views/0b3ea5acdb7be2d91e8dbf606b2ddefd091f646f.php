<?php $__env->startSection('css'); ?>
    ##parent-placeholder-2f84417a9e73cead4d5c99e05daff2a534b30132##
    <link rel="stylesheet" href="<?php echo e(asset('plugins/slick/slick.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/plyr/plyr.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    ##parent-placeholder-93f8bb0eb2c659b85694486c41717eaf0fe23cd4##
    <script type="text/javascript" src="<?php echo e(asset('plugins/slick/slick.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('plugins/plyr/plyr.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('pages.new_home.slick', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('pages.new_home.subscribes', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('pages.new_home.mainnews', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('pages.new_home.tv', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('pages.new_home.interestingfeed', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.new_base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>