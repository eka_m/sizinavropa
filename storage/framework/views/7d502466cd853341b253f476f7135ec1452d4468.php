<?php $__env->startSection('title', 'IMAGINE 2018'); ?>

    
    


    
    
    
    
        
            
                
                    
                    
                
            

        
    

<?php $__env->startSection('content'); ?>
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
        <?php echo $__env->make('pages.imagine.nav',['active' => 'movies'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
                <a href="<?php echo e(route('imagine')); ?>"><img src="<?php echo e(asset('img/imagine/imagine-new.png')); ?>" alt=""></a>
            </div>
        </div>
        <div class="uk-container bg-white uk-margin-auto uk-padding-small">
            <div class="page-title uk-text-center">
                <h1 class="uk-heading-line uk-h2"><span>Movies</span></h1>
            </div>
            <div class="page-content">
                <div id="films" class="films uk-flex uk-child-width-1-4@l uk-child-width-1-3@m  uk-child-width-1-2@s uk-child-width-1-1@xs grid"
                     uk-grid-parallax uk-scrollspy="target: > div > .uk-card; cls:uk-animation-fade; delay: 500">
                    <?php $__currentLoopData = $films; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $film): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="grid-item">
                            <div class="film-item uk-card-hover uk-card uk-card-default"
                                 onclick="window.open('<?php echo e(route('imagine.film',$film->slug)); ?>','_self')">
                                <a href="<?php echo e(route('imagine.film',$film->slug)); ?>"><?php echo e($film->title); ?></a>
                                <span class="uk-display-block">
                                    <?php echo e($film->date ? date("d F g:i a", strtotime($film->date)) : ''); ?>

                                </span>
                                <?php if($film->poster !== null): ?>
                                    <img src="<?php echo e(asset('uploads'.$film->poster)); ?>" alt="">
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.imagine', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>