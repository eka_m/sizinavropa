<?php $__env->startSection('title','::Create Gallery'); ?>
<?php $__env->startSection('js'); ?>
    ##parent-placeholder-93f8bb0eb2c659b85694486c41717eaf0fe23cd4##
    <script type="text/javascript" src="<?php echo e(asset('plugins/moxiemanager/js/moxman.loader.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
          <h3>New Gallery</h3>
        </div>
        <div class="uk-container uk-container-small">
            <form action="<?php echo e(route('galleries.store')); ?>" method="post">
                <?php echo e(csrf_field()); ?>

                <div class="uk-magin">
                    <div class="uk-padding-small">
                        <label class="uk-form-label uk-animation-slide-bottom">Gallery name</label>
                        <input type="text" class="uk-input uk-form-large" name="name" value="" placeholder="Gallery name">
                    </div>
                </div>
                <div class="uk-magin">
                    <div class="uk-padding-small">
                        <label class="uk-form-label uk-animation-slide-bottom">Gallery description</label>
                        <textarea name="description" class="uk-textarea uk-form-large" id="" cols="30" rows="5" placeholder="Gallery description"></textarea>
                    </div>
                </div>
                <div class="uk-magin">
                    <div class="uk-padding-small">
                        <label class="uk-form-label uk-animation-slide-bottom">Gallery type</label>
                        <select class="uk-select uk-form-large" name="type">
                            <option value="default">Default</option>
                            <option value="tiles">Tiles</option>
                            <option value="tilesgrid">Tiles grid</option>
                            <option value="carousel">Carousel</option>
                            <option value="compact">Compact</option>
                            <option value="grid">Grid</option>
                            <option value="slider">Slider</option>
                        </select>
                    </div>
                </div>
                <div class="uk-margin">
                    <div class="uk-padding-small">
                        <gallery-images></gallery-images>
                    </div>
                </div>
                <div class="uk-margin">
                    <div class="uk-padding-small">
                        <button class="uk-button uk-button-primary uk-float-right">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>