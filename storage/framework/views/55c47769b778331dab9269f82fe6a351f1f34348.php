<?php $__env->startSection('title', '::Articles'); ?>
<?php $__env->startSection('content'); ?>
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <div uk-grid>
                <div class="uk-width-5-6">
                    <form method="GET" action="<?php echo e(route('articles.search')); ?>">
                        <button class="uk-icon-button uk-button-primary" uk-icon="icon: search"></button>
                        <div uk-form-custom>
                            <input class="uk-input uk-form-width-medium uk-border-rounded" name="searchfield"
                                   type="text" placeholder="Search">
                        </div>
                    </form>
                </div>
                <div class="uk-width-1-6">
                    <a href="<?php echo e(route('articles.create')); ?>" class="uk-icon-button uk-button-secondary uk-float-right"
                       uk-icon="icon: plus"></a>
                </div>
            </div>
        </div>
        <div class="uk-container uk-container-small">
            <div class="uk-grid-medium uk-child-width-1-1@s uk-child-width-1-2@m uk-child-width-1-3@l uk-text-center"
                 uk-grid-parallax uk-scrollspy="target: > div; cls:uk-animation-scale-up; delay: 500">
                <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <article-item item="<?php echo e($article); ?>" userrole="<?php echo e(Auth::user()->role->name); ?>"></article-item>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <div class="uk-margin uk-flex uk-padding-small">
                <div class="uk-margin-auto">
                    <?php echo e($articles->links('vendor.pagination.uikit')); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>