<section class="row videoChannel gradient-bg mt-5 p-3">
    <div class="col row-content">
        <div class="row">
            <div class="col blockheader blockheader--gradientblock">
                    <span class="blockheader__name"><span>SİZİN AVROPA</span><div
                                class="tv"><span>TV</span></div></span>
            </div>
        </div>
        <div class="row videoChannel__row no-gutters">
            <div class="col-sm-12 snowCollect col-lg-5 order-2 order-lg-1">
                <div class="row videoChannel__row__items videoChannel__row__items--small no-gutters">
                    <?php $__currentLoopData = $videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-6">
                            <article class="videoChannel__row__items__item videoChannel__row__items__item--small smallTvVideo" data-video="<?php echo e(json_encode($video)); ?>">
                                <div class="videoChannel__row__items__item--small__image imgLiquid">
                                    <img src="https://img.youtube.com/vi/<?php echo e($video->link); ?>/mqdefault.jpg" class="fluid image" alt="">
                                </div>
                                <header class="videoChannel__row__items__item__info videoChannel__row__items__item--small__info cl--<?php echo e($video->color); ?>" data-description="<?php echo e($video->description); ?>" data-color="<?php echo e($video->color); ?>">
                                    <h3><?php echo e($video->name); ?></h3>
                                </header>
                                <button class="videoChannel__row__items__item--small__playbutton"><i class="sa-play"></i>
                                </button>
                            </article>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="col-sm-12 col-lg-7 order-1 order-lg-2">
                <div class="row no-gutters">
                    <div class="col">
                        <article class="videoChannel__row__items__item videoChannel__row__items__item--big snowCollect" style="background-color:<?php echo e($videos->first()->color); ?>;">
                            <div class="row no-gutters">
                                <div class="col">
                                    <div class="videoChannel__row__items__item--big__video">
                                        <div data-type="<?php echo e($videos->first()->type); ?>" data-video-id="<?php echo e($videos->first()->link); ?>" class="tvvideo"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row no-gutters">
                                <div class="col">
                                    <header class="videoChannel__row__items__item videoChannel__row__items__item__info videoChannel__row__items__item--big__info tvvideoinfo">
                                        <h3 class="mb-1"><?php echo e($videos->first()->name); ?></h3>
                                        <p><?php echo e($videos->first()->description); ?></p>
                                    </header>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        <div class="row blockgo blockgo--gradient">
            <div class="col pt-0 mt-0">
                <a href="<?php echo e(route('videos')); ?>" class="blockgo__arrow blockgo__arrow--right">&rarr;</a>
            </div>
        </div>
    </div>
</section>
