<?php $__env->startSection('title','::Create Gallery'); ?>
<?php $__env->startSection('js'); ?>
    ##parent-placeholder-93f8bb0eb2c659b85694486c41717eaf0fe23cd4##
    <script type="text/javascript" src="<?php echo e(asset('plugins/moxiemanager/js/moxman.loader.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <h3>Edit Gallery</h3>
        </div>
        <div class="uk-container uk-container-small">
            <form action="<?php echo e(route('galleries.update',$gallery->id)); ?>" method="post">
                <?php echo e(csrf_field()); ?>

                <?php echo e(method_field('PUT')); ?>

                <div class="uk-magin">
                    <div class="uk-padding-small">
                        <label class="uk-form-label uk-animation-slide-bottom">Gallery name</label>
                        <input type="text" class="uk-input uk-form-large" name="name" value="<?php echo e($gallery->name); ?>"
                               placeholder="Gallery name">
                    </div>
                </div>
                <div class="uk-magin">
                    <div class="uk-padding-small">
                        <label class="uk-form-label uk-animation-slide-bottom">Gallery description</label>
                        <textarea name="description" class="uk-textarea uk-form-large" id="" cols="30" rows="5"
                                  placeholder="Gallery description"><?php echo e($gallery->description); ?></textarea>
                    </div>
                </div>
                <div class="uk-magin">
                    <div class="uk-padding-small">
                        <label class="uk-form-label uk-animation-slide-bottom">Gallery type</label>
                        <select class="uk-select uk-form-large" name="type">
                            <option value="default" <?php if($gallery->type == 'default'): ?> selected <?php endif; ?>>Default</option>
                            <option value="tiles" <?php if($gallery->type == 'tiles'): ?> selected <?php endif; ?>>Tiles</option>
                            <option value="tilesgrid" <?php if($gallery->type == 'tilesgrid'): ?> selected <?php endif; ?>>Tiles grid
                            </option>
                            <option value="carousel" <?php if($gallery->type == 'carousel'): ?> selected <?php endif; ?>>Carousel</option>
                            <option value="compact" <?php if($gallery->type == 'compact'): ?> selected <?php endif; ?>>Compact</option>
                            <option value="grid" <?php if($gallery->type == 'grid'): ?> selected <?php endif; ?>>Grid</option>
                            <option value="slider" <?php if($gallery->type == 'slider'): ?> selected <?php endif; ?>>Slider</option>
                        </select>
                    </div>
                </div>
                <div class="uk-margin">
                    <div class="uk-padding-small">
                        <gallery-images :current="<?php echo e(json_encode($gallery->images)); ?>"></gallery-images>
                    </div>
                </div>
                <div class="uk-margin">
                    <div class="uk-padding-small">
                        <button class="uk-button uk-button-primary uk-float-right">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>