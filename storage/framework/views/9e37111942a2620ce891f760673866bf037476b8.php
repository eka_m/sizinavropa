<?php $__env->startSection('title', 'IMAGINE 2018 Programs'); ?>
<?php $__env->startSection('css'); ?>
##parent-placeholder-2f84417a9e73cead4d5c99e05daff2a534b30132##
<style>
.pr-prev, .pr-next {
    text-decoration: none;
    background: #18c6ff;
    transition: all .5s;
    
}
.pr-prev:hover, .pr-next:hover {
    background: #BE30A0;
} 
.pr-prev:hover{
    border-radius: 0 50px 50px 50px;
}
.pr-next {
    float:right;
}
.pr-next:hover{
    border-radius: 50px 0 50px 50px;
}
</style>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
        <?php echo $__env->make('pages.imagine.nav',['active' => 'programs'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
                <a href="<?php echo e(route('imagine')); ?>"><img src="<?php echo e(asset('img/imagine/imagine-new.png')); ?>" alt=""></a>
            </div>
        </div>
        <div class="uk-container-large bg-white uk-margin-auto uk-padding-small">
            <div class="page-title uk-text-center">
                <h1 class="uk-heading-line uk-h1"><span>Program</span></h1>
                <a href="<?php echo e(asset('img/imagine/Imagine2018Flyer.png')); ?>" class="pr-download" download>Download</a>
            </div>
            <div class="uk-container bg-white uk-margin-auto uk-padding-small uk-clearfix">
                <?php if(\Request::get('page') < 2): ?>
                <a href="/imagine/program?page=<?php echo e(\Request::get('page') ? \Request::get('page')  + 1 : 2); ?>" class="pr-next uk-button uk-button-primary uk-button-small">Next page <span>&rarr;</span></a>
                <?php endif; ?>
                <?php if(\Request::get('page') > 1): ?>
                <a href="/imagine/program?page=<?php echo e(\Request::get('page') ? \Request::get('page') - 1 : 1); ?>" class="pr-prev uk-button uk-button-primary uk-button-small"><span>&larr;</span> Previous page</a>
                <?php endif; ?>
            </div>
            <div class="page-content-program">
                <div class="program-block uk-child-width-expand@l uk-child-width-1-6@m uk-child-width-1-5@s grid uk-grid-small"
                     uk-grid uk-scrollspy="target: > div > .uk-card; cls:uk-animation-fade; delay: 500">
                    <?php $__currentLoopData = $program; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="grid-item">
                            <div class="uk-card ">
                                <div class="programItem">
                                    <p class="pr-head"><?php echo e(date("l", strtotime($key))); ?> <?php echo e(date("d.m.", strtotime($key))); ?></p>
                                    <?php $__currentLoopData = $group; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p class="programPlace"><?php echo e($k); ?></p>
                                        <ul class="program uk-list ">
                                            <?php $__currentLoopData = $value->sortBy('start'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li>
                                                    <?php if($item->start && $item->end): ?>
                                                        <span class="time"><span><?php echo e(date("H:i", strtotime($item->start)).'-'); ?><?php echo e(date("H:i", strtotime($item->end))); ?></span></span>
                                                    <?php endif; ?>
                                                    <div class="pr-title"><?php echo $item->title; ?></div>
                                                </li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.imagine', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>