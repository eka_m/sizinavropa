<?php $__env->startSection('content'); ?>
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-flex uk-flex-center">
            <div class="uk-card uk-card-body uk-card-default uk-width-1-3@m uk-animation-scale-down ">
                <div class="uk-card-title">
                    <h2>Ecms</h2>
                </div>
                <form role="form" method="POST" action="<?php echo e(route('login')); ?>">
                    <?php echo e(csrf_field()); ?>

                    <div class="uk-margin  uk-width-1-1">
                        <label class="uk-form-label uk-animation-slide-bottom-small">Email</label>
                        <div class="uk-form-controls">
                            <div class="uk-inline">
                                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: user"></span>
                                <input id="email" type="email"
                                       class="uk-input uk-form-width-large <?php echo e($errors->has('email') ? 'uk-form-danger' : ''); ?>"
                                       name="email" value="<?php echo e(old('email')); ?>" required
                                       autofocus>
                            </div>
                            <?php if($errors->has('email')): ?>
                                <span class="uk-text-danger">
                                        <?php echo e($errors->first('email')); ?>

                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label class="uk-form-label uk-animation-slide-bottom-small">Password</label>
                        <div class="uk-form-controls">
                            <div class="uk-inline">
                                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                                <input id="password" type="password"
                                       class="uk-input uk-form-width-large <?php echo e($errors->has('password') ? 'uk-form-danger' : ''); ?>"
                                       name="password"
                                       required>
                            </div>
                            <?php if($errors->has('password')): ?>
                                <span class="uk-text-danger">
                                    <?php echo e($errors->first('password')); ?>

                                </span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <input type="checkbox" class="uk-checkbox"
                               name="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>
                        <label class="uk-form-label">Remember Me</label>
                    </div>
                    <div class="uk-margin">
                        <button type="submit" class="uk-button uk-button-primary uk-float-right uk-width-1-1">
                            Login
                        </button>
                    </div>
                    
                    
                    
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>