<section class="row mainmenu gradient-bg">
    <div class="col pl-md-5 pr-md-5 row-content">
        <nav id="navigation" class="navigation">
            <div class="nav-header">
                
                    <a class="nav-logo" href="<?php echo e(route('home')); ?>">
                    <img src="<?php echo e(asset('img/logo3.png')); ?>" alt="logo" style="height: 80px; margin: 5px 0; float: left">
                </a>
                <div class="nav-toggle"></div>
            </div>
            <div class="nav-search">
                <div class="nav-search-button">
                    <i class="nav-search-icon sa-search"></i>
                </div>
                <form>
                    <div class="nav-search-inner">
                        <input type="search" name="search" placeholder="<?php echo e(__('words.search')); ?>"/>
                    </div>
                </form>
            </div>
            <div class="nav-menus-wrapper">
                <ul class="nav-menu nav-menu-social">
                    <li><a href="https://www.facebook.com/SizinAvropa" target="_blank"><i class="sa-facebook"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCRRbADrlDJfBgJGPLvStCfA"  target="_blank"><i class="sa-youtube"></i></a></li>
                </ul>
                <ul class="nav-menu align-to-right">
                    <?php $__currentLoopData = $toppages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li class="<?php echo e(request()->is('*/'.$page->url) ? 'active' : ''); ?>">
                            <a href="/<?php echo e($currentlocale); ?>/page/<?php echo e($page->url); ?>">
                                <?php if($currentlocale == env('DEFAULTLOCALE')): ?><?php echo e($page->name); ?><?php else: ?> <?php echo e($page->name_en); ?><?php endif; ?></a>
                        </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <li>
                        <a href="<?php echo e(LaravelLocalization::getLocalizedURL($locales->first()->slug, null, [], true)); ?>" class="up-text ">
                            <i class="sa-world"></i>
                            <?php echo e($locales->first()->slug); ?>

                        </a>
                        <?php if($locales->count() > 1): ?>
                            <ul class="nav-dropdown lang-dropdown">
                                <?php $__currentLoopData = $locales->slice(1); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $locale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <a href="<?php echo e(LaravelLocalization::getLocalizedURL($locale->slug, null, [], true)); ?>"><?php echo e($locale->slug); ?></a>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</section>