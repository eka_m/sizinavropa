<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>Admin Panel <?php echo $__env->yieldContent('title'); ?></title>
    
    <?php $__env->startSection('css'); ?>
        <link rel="stylesheet" href="<?php echo e(asset('/adminpanel/css/app.css')); ?>">
    <?php echo $__env->yieldSection(); ?>
    
</head>
<body>

<div id="app" class="wrapper uk-offcanvas-content">
    <?php echo $__env->make('admin.partials.main-nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php if(session('error')): ?>
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <div class="uk-alert-danger" uk-alert>
                <p><?php echo e(session('error')); ?></p>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php $__env->startSection('content'); ?>
    <?php echo $__env->yieldSection(); ?>
</div>
<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('adminpanel/js/manifest.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('adminpanel/js/vendor.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('adminpanel/js/app.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('adminpanel/js/custom.js')); ?>" type="text/javascript"></script>
<?php echo $__env->yieldSection(); ?>
<?php echo $__env->make('admin.partials.notifications', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>