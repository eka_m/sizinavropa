<section class="row categories light-bg hidden-sm">
    <div class="col pl-lg-5 pr-lg-5 row-content">
        <div class="ui fluid stackable five item massive text menu no-margin-bottom no-margin-top">
            <?php $__currentLoopData = $clearpages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <a href="/<?php echo e($currentlocale); ?>/page/<?php echo e($page->url); ?>" class="item categories_link hv--<?php echo e($page->color); ?> br--<?php echo e($page->color); ?> <?php echo e(request()->is('*/'.$page->url) ? 'tx--white bg--'.$page->color  : ''); ?>"><?php if($currentlocale == env('DEFAULTLOCALE')): ?><?php echo e($page->name); ?><?php else: ?> <?php echo e($page->name_en); ?><?php endif; ?></a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section>
