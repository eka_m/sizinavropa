<?php $__env->startSection('title',$article->name); ?>
<?php $__env->startSection('keywords',$article->keywords); ?>
<?php $__env->startSection('description',$article->description); ?>
<?php $__env->startSection('s-description',$article->description); ?>
<?php $__env->startSection('s-title',$article->name); ?>
<?php $__env->startSection('s-url',Request::fullUrl()); ?>
<?php $__env->startSection('s-image','/uploads'.$article->image); ?>


<?php $__env->startSection('content'); ?>
    <section class="row ">
        <div class="col articleRead__cover p-0 mb-2">
            <div class="articleRead__cover__image imgLiquid">
                <img src="/uploads<?php echo e($article->cover); ?>" alt="">
            </div>
            <div class="articleRead__cover__overlay"></div>
            <div class="articleRead__cover__info">
                <a href="/page/<?php echo e($article->page->url); ?>"
                   class="link_reset articleRead__cover__info__category bg--<?php echo e($article->page->color); ?>">
                    <?php if($currentlocale == env('DEFAULTLOCALE')): ?><?php echo e($article->page->name); ?><?php else: ?> <?php echo e($article->page->name_en); ?><?php endif; ?>
                </a>
                <h1><?php echo e($article->name); ?></h1>
                <div class="articleRead__cover__info__dateviews">
                    <span class="articleRead__cover__info__dateviews__date"><?php echo e($article->created_at); ?></span>
                    <span>/</span>
                    <span class="articleRead__cover__info__dateviews__views"><i
                                class="sa-eye"></i> <?php echo e($article->fakeviews != 0 ? $article->fakeviews : $article->views); ?></span>
                </div>
            </div>
        </div>
    </section>
    <section class="container articleRead__content">
        <div class="row">
            <div class="col articleRead__content cl__white p-3 p-sm-3 p-md-5">
                <?php echo $article->content; ?>

            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.new_base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>