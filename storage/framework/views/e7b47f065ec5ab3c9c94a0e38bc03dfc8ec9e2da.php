<section class="row newsFeed light-bg mt-5 p-3">
    <div class="col row-content">
        <div class="row">
            <div class="col blockheader blockheader--whiteblock">
                <span class="blockheader__name"><span><?php echo e(__('words.interesting')); ?></span></span>
            </div>
        </div>
        <div class="row newsFeed__row no-gutters">
            <?php $__currentLoopData = $news->slice(10, 9); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <article class="newsFeed__row__item imgLiquid" onclick="location.href = '/<?php echo e($currentlocale); ?>/article/<?php echo e($article->url); ?>'">
                    <div class="newsFeed__row__item__image imgLiquid">
                        <img src="<?php echo e(setImage($article->image, 'interesting')); ?>" class="fluid image" alt="">
                    </div>
                    <div class="newsFeed__row__item__overlay gr--<?php echo e($article->page->color); ?>"></div>
                    <header class="newsFeed__row__item__info">
                        <h3><?php echo e($article->name); ?></h3>
                        <div class="newsFeed__row__item__info__dateviews">
                            <span class="newsFeed__row__item__info__dateviews__date"><?php echo e($article->created_at); ?></span>
                            <span>/</span>
                            <span class="newsFeed__row__item__info__dateviews__views"><i class="sa-eye"></i>
                                    <?php echo e($article->fakeviews ? $article->fakeviews : $article->views); ?>

                                </span>
                        </div>
                    </header>
                </article>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        
    </div>
</section>