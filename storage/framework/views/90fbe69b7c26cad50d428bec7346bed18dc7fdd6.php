<!DOCTYPE html>
<html lang="<?php echo e($currentlocale); ?>">

<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121877875-1"></script>
  <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
          dataLayer.push(arguments);
      }

      gtag('js', new Date());
      gtag('config', 'UA-121877875-1');

  </script>

  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo e(asset('/img/favicon/apple-touch-icon-57x57.png')); ?>"/>
  <link rel="apple-touch-icon-precomposed" sizes="114x114"
        href="<?php echo e(asset('/img/favicon/apple-touch-icon-114x114.png')); ?>"/>
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo e(asset('/img/favicon/apple-touch-icon-72x72.png')); ?>"/>
  <link rel="apple-touch-icon-precomposed" sizes="144x144"
        href="<?php echo e(asset('/img/favicon/apple-touch-icon-144x144.png')); ?>"/>
  <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo e(asset('/img/favicon/apple-touch-icon-60x60.png')); ?>"/>
  <link rel="apple-touch-icon-precomposed" sizes="120x120"
        href="<?php echo e(asset('/img/favicon/apple-touch-icon-120x120.png')); ?>"/>
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo e(asset('/img/favicon/apple-touch-icon-76x76.png')); ?>"/>
  <link rel="apple-touch-icon-precomposed" sizes="152x152"
        href="<?php echo e(asset('/img/favicon/apple-touch-icon-152x152.png')); ?>"/>
  <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-196x196.png')); ?>" sizes="196x196"/>
  <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-96x96.png')); ?>" sizes="96x96"/>
  <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-32x32.png')); ?>" sizes="32x32"/>
  <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-16x16.png')); ?>" sizes="16x16"/>
  <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-128.png')); ?>" sizes="128x128"/>
  <meta name="application-name" content="&nbsp;"/>
  <meta name="msapplication-TileColor" content="#FFFFFF"/>
  <meta name="msapplication-TileImage" content="<?php echo e(asset('/img/favicon/mstile-144x144.png')); ?>"/>
  <meta name="msapplication-square70x70logo" content="<?php echo e(asset('/img/favicon/mstile-70x70.png')); ?>"/>
  <meta name="msapplication-square150x150logo" content="<?php echo e(asset('/img/favicon/mstile-150x150.png')); ?>"/>
  <meta name="msapplication-wide310x150logo" content="<?php echo e(asset('/img/favicon/mstile-310x150.png')); ?>"/>
  <meta name="msapplication-square310x310logo" content="<?php echo e(asset('/img/favicon/mstile-310x310.png')); ?>"/>
  <meta name="keywords" content="<?php echo $__env->yieldContent('keywords'); ?>">
  <meta name="description" content="<?php echo $__env->yieldContent('description'); ?>">

  <?php echo $__env->yieldContent('social-tags'); ?>

  <?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.css">
    <link href="https://file.myfontastic.com/yi7HNSNkRQ5uFhbZ4vdnSK/icons.css" rel="stylesheet"> 
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bootstrap-grid/grid.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/megamenu/css/navigation.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/megamenu/css/navigation.skin.rounded-boxed.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/semantic/components/menu.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">
  <?php echo $__env->yieldSection(); ?>
  <link rel="stylesheet" href="<?php echo e(asset('plugins/snowfall/newyear.css')); ?>">

  <style>
    #app {
     height: initial !important;
    }
    .flurry-container {
      max-height: 100% !important;
    }
    .lightrope-container {
      height: 100px;
      width: 100%;
      overflow: hidden;
      background: green;
      padding:0 20px;
      position: relative;
      background: repeating-linear-gradient(
              45deg,
              #E74C3C,
              #E74C3C 10px,
              #C0392B 10px,
              #C0392B 20px);
    }
    .lightrope-box {
      width: 100%;
      height: 100%;
      background: repeating-linear-gradient(
              45deg,
              #2ECC71,
              #2ECC71 10px,
              #27AE60 10px,
              #27AE60 20px
      );

    }
  </style>
  <title>Sizin avropa <?php echo $__env->yieldContent('title'); ?></title>
</head>

<body>

<div id="app" class="container-fluid">
  <div class="row">
    <div class="col p-0">
      <div class="lightrope-container">
        <div class="lightrope-box">
          <ul class="lightrope">
            <li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
          </ul>
          <div id="path">
            <div id="santa"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php echo $__env->make('pages.home.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('pages.home.categories', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->yieldContent('content'); ?>
  <?php echo $__env->make('pages.home.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>

<?php $__env->startSection('js'); ?>
  <script src="<?php echo e(asset('plugins/jquery/jquery-3.2.1.min.js')); ?>"></script>
  <script src="<?php echo e(asset('plugins/megamenu/js/navigation.min.js')); ?>"></script>
  <script src="<?php echo e(asset('plugins/imageliquid/imgliquid.js')); ?>"></script>
  <script src="<?php echo e(asset('plugins/snowfall/jquery.flurry.min.js')); ?>"></script>
  <script type="text/javascript">
      var h = window.innerHeight * (window.innerHeight / document.body.offsetHeight);

      $("#app").flurry({
          character: ".",
          color: ["#ffffff"],
          speed: 100000,
          height: $("#app").height(),
          endOpacity:0.2,
          frequency: 20,
          small: 8,
          large: 50,
          wind: 100,
          windVariance: 100,
          opacityEasing: "cubic-bezier(1,0,.96,.9)"
      });


  </script>
<?php echo $__env->yieldSection(); ?>
<script src="<?php echo e(asset('js/custom.js')); ?>" type="text/javascript"></script>
</body>

</html>