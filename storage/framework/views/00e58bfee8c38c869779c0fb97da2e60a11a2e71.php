<?php $__env->startSection('title',$article->name); ?>
<?php $__env->startSection('keywords',$article->keywords); ?>
<?php $__env->startSection('description',$article->description); ?>


<?php $__env->startSection('social-tags'); ?>

  <?php 
    $image = setImage($article->cover, 'lastnews');
    if($image == '') {
      $image = setImage($article->cover, 'interesting');
    }
    if($image == '') {
      $image = setImage($article->cover, 'cover');
    }
   ?>

  <?php echo $__env->make('partials.social-tags', [
  "title" => $article->name,
  "description" => $article->description,
  "image" =>  $image,
  "url" =>request()->fullUrl(),
  ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
  ##parent-placeholder-2f84417a9e73cead4d5c99e05daff2a534b30132##
  <?php if($gallery): ?>
    <link rel='stylesheet' href='<?php echo e(asset('/plugins/unitegallery/css/unite-gallery.css')); ?>' type='text/css'/>
    <link rel='stylesheet' href='<?php echo e(asset('/plugins/unitegallery/themes/default/ug-theme-default.css')); ?>'
          type='text/css'/>
  <?php endif; ?>
  <style>
    .social-share-container {
      position: fixed;
      right: 0;
      top: 50%;
      padding: 0 5px;
      z-index: 9999;
    }

    .social-share-link {
      display: block;
      width: 50px;
      height: 50px;
      padding: 8px;
      background: #FFFFFF;
      transition: all 0.5s;
      margin-top: 5px;
      margin-bottom: 5px;
    }

    .social-share-link:hover {
      transform: scale(1.1);
    }
  </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?> ##parent-placeholder-93f8bb0eb2c659b85694486c41717eaf0fe23cd4## <?php if($gallery): ?>
  <script type='text/javascript' src='<?php echo e(asset('/plugins/unitegallery/js/unitegallery.min.js')); ?>'></script>
  <?php $__currentLoopData = $gallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <script type='text/javascript'
            src='/plugins/unitegallery/themes/<?php echo e($item->type); ?>/ug-theme-<?php echo e($item->type); ?>.js'></script>
    <script type="text/javascript">
        $("#gallery<?php echo e($item->id); ?>").unitegallery(
                <?php echo '{
                gallery_theme:"'.$item->type.'",
                tile_enable_textpanel: false,
                lightbox_type: "compact",
                }'; ?>

        );
    </script>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
<script type="text/javascript">
    $('.social-share-popup').click(function (e) {
        e.preventDefault();
        var link = $(this).attr("href");
        var myWindow = window.open(link, "", "width=720,height=550px");
    })
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <section class="row">
    <div class="col articleRead__cover p-0 mb-2">
      <div class="articleRead__cover__image imgLiquid">
        <img src="<?php echo e(setImage($article->cover, 'cover')); ?>" alt="">
      </div>
      <div class="articleRead__cover__overlay"></div>
      <div class="articleRead__cover__info">
        <a href="/page/<?php echo e($article->page->url); ?>"
           class="link_reset articleRead__cover__info__category bg--<?php echo e($article->page->color); ?>">
          <?php if($currentlocale == env('DEFAULTLOCALE')): ?><?php echo e($article->page->name); ?><?php else: ?> <?php echo e($article->page->name_en); ?><?php endif; ?>
        </a>
        <h1><?php echo e($article->name); ?></h1>
        <div class="articleRead__cover__info__dateviews">
          <span class="articleRead__cover__info__dateviews__date"><?php echo e($article->created_at); ?></span>
          <span>/</span>
          <span class="articleRead__cover__info__dateviews__views"><i
                    class="sa-eye"></i> <?php echo e($article->fakeviews != 0 ? $article->fakeviews : $article->views); ?></span>
        </div>
      </div>
    </div>
  </section>
  <section class="container articleRead__content">
    <div class="row">
      <div class="col articleRead__content cl__white p-3 p-sm-3 p-md-5">
        <?php echo $article->content; ?>

      </div>
    </div>
    <div class="row">
      <div class="col">
        <?php echo $__env->make('partials.social-share', ['title' => $article->name, "url" => request()->fullUrl()], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>