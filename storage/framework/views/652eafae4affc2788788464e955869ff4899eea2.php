 
<?php $__env->startSection('title',$page->name); ?> 
<?php $__env->startSection('keywords',$page->keywords); ?> 
<?php $__env->startSection('description',$page->description); ?>

<?php $__env->startSection('s-description',$page->description); ?> 
<?php $__env->startSection('s-title',$page->name); ?> 
<?php $__env->startSection('s-url',Request::fullUrl()); ?> 
<?php $__env->startSection('s-image','/uploads'.$page->cover); ?>

<?php $__env->startSection('css'); ?> ##parent-placeholder-2f84417a9e73cead4d5c99e05daff2a534b30132## <?php if($gallery): ?>
<link rel='stylesheet' href='<?php echo e(asset(' /plugins/unitegallery/css/unite-gallery.css ')); ?>' type='text/css' />
<link rel='stylesheet' href='<?php echo e(asset(' /plugins/unitegallery/themes/default/ug-theme-default.css ')); ?>' type='text/css' /> <?php endif; ?>
<?php $__env->stopSection(); ?>
 
<?php $__env->startSection('js'); ?> ##parent-placeholder-93f8bb0eb2c659b85694486c41717eaf0fe23cd4## <?php if($gallery): ?>
<script type='text/javascript' src='<?php echo e(asset(' /plugins/unitegallery/js/unitegallery.min.js ')); ?>'></script>
<?php $__currentLoopData = $gallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<script type='text/javascript' src='/plugins/unitegallery/themes/<?php echo e($item->type); ?>/ug-theme-<?php echo e($item->type); ?>.js'></script>
<script type="text/javascript">
    $("#gallery<?php echo e($item->id); ?>").unitegallery(
                        <?php echo '{
                        gallery_theme:"'.$item->type.'",
                        tile_enable_textpanel: false,
                        lightbox_type: "compact",
                        }'; ?>

                );

</script>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
<?php $__env->stopSection(); ?>
 
<?php $__env->startSection('content'); ?> <?php if($page->cover): ?>
<section class="row">
    <div class="col articleRead__cover p-0 mb-2">
        <div class="articleRead__cover__image imgLiquid">
            <img src="/uploads<?php echo e($page->cover); ?>" alt="">
        </div>
        <div class="articleRead__cover__overlay"></div>
        <div class="articleRead__cover__info">
            <a href="javascript:void(0)" class="link_reset articleRead__cover__info__category bg--<?php echo e($page->color); ?>">
                        <?php if($currentlocale == env('DEFAULTLOCALE')): ?><?php echo e($page->name); ?><?php else: ?> <?php echo e($page->name_en); ?><?php endif; ?></a>
            </a>
        </div>
    </div>
</section>
<?php endif; ?> <?php if($page->content): ?>
<section class="row mt-3">
    <div class="col row-content bg--white pl-md-5 pr-md-5 pt-5 pb-5">
        <?php echo $page->content; ?>

    </div>
</section>
<?php endif; ?> <?php if(count($page->articles) > 0): ?>
<section class="row pageArticles mt-3">
    <div class="col row-content">
        <div class="row">
            <?php $__empty_1 = true; $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div class="col-md-3 col-lg-3 col-sm-6">
                <article class="pageArticles__item">
                    <div class="pageArticles__item__image imgLiquid p-0  br--<?php echo e($page->color); ?>">
                        <img src="<?php echo e(setImage($article->image, 'interesting')); ?>" class="fluid" alt="">
                    </div>
                    <header class="pageArticles__item__info">
                        <h3><?php echo e($article['name']); ?></h3>
                        <p><?php echo e(str_limit($article['short'],200)); ?></p>
                        <a href="/article/<?php echo e($article['url']); ?>" class="pageArticles__item__info__readmore br--<?php echo e($page->color); ?> bg--<?php echo e($page->color); ?>">
                                                &rarr;
                                            </a>
                    </header>
                </article>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <div class="col-12 py-5">
                <div class="text-right py-5" style="text-align:center;">
                    <h4>Ohh.. Sorry..</h4>
                    <h5>0 articles in this language</h5>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col p-3 d-flex align-items-center">
                <div class="d-inline-block m-auto">
                    <?php echo e($articles->links('vendor.pagination.semantic')); ?>

                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>