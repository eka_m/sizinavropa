<?php $__env->startSection('title', 'IMAGINE 2018'); ?>

<?php $__env->startSection('content'); ?>
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" >
        <div class="uk-container-large uk-padding-remove">
            <div class="main-header" uk-grid>
                <div class="burger">
                    <a href="#menu" class="uk-link-reset" uk-scroll>&#9776;</a>
                </div>
                <div class="sa-logo uk-margin-auto" >
                    <a href="<?php echo e(route('home')); ?>"><img src="<?php echo e(asset('img/logo-yellow.png')); ?>"></a>
                </div>
                <div class="date uk-visible@s">
                    <h1>2-17 MAY 2018</h1>
                </div>
                <div class="eu-logo uk-margin-auto uk-width-expand">
                    <img src="<?php echo e(asset('img/imagine/eu.png')); ?>" alt="">
                </div>
                <div class="date uk-hidden@s">
                    <h1>2-17 MAY 2018</h1>
                </div>
            </div>
        </div>
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
                
                        
                <a href="<?php echo e(route('imagine')); ?>"><img src="<?php echo e(asset('img/imagine/imagine-new.png')); ?>" alt=""></a>
            </div>
        </div>
        <div class="uk-container-large bg-white uk-margin-auto uk-padding-large" id="menu">
            <div class="uk-child-width-1-3@m uk-text-center main-menu" uk-grid
                 uk-scrollspy="target: > div > .main-menu-item; cls:uk-animation-scale-down; delay: 500">
                <div>
                    <div class="main-menu-item main-menu-item--1 uk-card uk-car-body uk-card-default uk-card-hover">
                        <div class="main-menu-item__1">
                            <a href="<?php echo e(route('imagine.page','about')); ?>">ABOUT</a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="main-menu-item main-menu-item--2 uk-card uk-car-body uk-card-default uk-card-hover">
                        <div class="main-menu-item__2">
                            <a href="<?php echo e(route('imagine.program')); ?>">Program</a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="main-menu-item main-menu-item--3 uk-card uk-car-body uk-card-default uk-card-hover">
                        <div class="main-menu-item__3">
                            <a href="<?php echo e(route('imagine.films')); ?>">Movies</a>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2@m">
                    <div class="main-menu-item main-menu-item--4 uk-card uk-car-body uk-card-default uk-card-hover">
                        <div class="main-menu-item__4">
                            <a href="<?php echo e(route('imagine.events')); ?>">Events</a>
                        </div>
                    </div>
                </div>
                
                    
                        
                            
                        
                    
                
                <div class="uk-width-1-2@m">
                    <div class="main-menu-item main-menu-item--6 uk-card uk-car-body uk-card-default uk-card-hover">
                        <div class="main-menu-item__6">
                            <a href="<?php echo e(route('imagine.page','partners')); ?>">Partners</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.imagine', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>