<?php $__env->startSection('title','TV'); ?>
<?php $__env->startSection('css'); ?>
    ##parent-placeholder-2f84417a9e73cead4d5c99e05daff2a534b30132##
    <link rel="stylesheet" href="<?php echo e(asset('plugins/plyr/plyr.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    ##parent-placeholder-93f8bb0eb2c659b85694486c41717eaf0fe23cd4##
    <script type="text/javascript" src="<?php echo e(asset('plugins/plyr/plyr.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <section class="container mt-3 cl__white">
        <div class="row">
            <div class="col video__content p-3 p-sm-3 p-md-5">
                <div class="row">
                    <?php $__currentLoopData = $videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class=" col-xs-12 col-md-6 col-lg-4 p-2 video__content__item">
                            <article class="video__content__item__video">
                                <div data-type="<?php echo e($video->type); ?>" data-video-id="<?php echo e($video->link); ?>" class="videosItem"></div>
                                <header class="video__content__item__video__info p-3">
                                    <h3><?php echo e($video->name); ?></h3>
                                    <p><?php echo e($video->description); ?></p>
                                </header>
                            </article>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col p-5 d-flex align-items-center">
                <div class="d-inline-block m-auto">
                    <?php echo e($videos->links('vendor.pagination.semantic')); ?>

                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>