<div class="uk-container uk-container-large">
    <a href="#offcanvas-usage" class="uk-margin-medium-right" uk-toggle uk-icon="icon: menu; ratio: 2"></a>
    <div class="uk-position-top-right">
        <div class="uk-inline uk-margin-medium-right uk-margin-small-top">
            <button class="uk-button uk-button-default" type="button"><span class="uk-margin-small-right"
                                                                            uk-icon="icon: user"></span>
                <?php echo e(Auth::user()->name); ?>::<span class="uk-text-danger"><?php echo e(Auth::user()->role->name); ?></span>
            </button>
            <div uk-dropdown="mode: hover; animation: uk-animation-slide-top-small; duration: 1000"
                 class="uk-padding-remove">
                <ul class="uk-nav uk-dropdown-nav uk-padding-remove">
                    <li>
                        <a href="<?php echo e(route('logout')); ?>"><span class="uk-margin-small-right"
                                                              uk-icon="icon: sign-out"></span> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="offcanvas-usage" uk-offcanvas="mode: reveal; overlay: false" uk-offcanvas>
        <div class="uk-offcanvas-bar">
            <button class="uk-offcanvas-close" type="button" uk-close></button>
            <h1>ECMS</h1>
            <div>
                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                    <?php if(Auth::user()->role->name != 'Editor'): ?>
                        <li class="uk-active uk-parent">
                            <a href="#"><span class="uk-margin-small-right" uk-icon="icon: bookmark"></span>Pages</a>
                            <ul class="uk-nav-sub">
                                <li><a href="<?php echo e(route('pages.index')); ?>"><span class="uk-margin-small-right"
                                                                             uk-icon="icon: list"></span>All</a></li>
                                <li><a href="<?php echo e(route('pages.create')); ?>"><span class="uk-margin-small-right"
                                                                              uk-icon="icon: plus-circle"></span>New</a>
                                </li>
                            </ul>
                        </li>
                    <?php endif; ?>
                    <li class="uk-parent">
                        <a href="#"><span class="uk-margin-small-right" uk-icon="icon: link"></span>Articles</a>
                        <ul class="uk-nav-sub">
                            <li><a href="<?php echo e(route('articles.index')); ?>"><span class="uk-margin-small-right"
                                                                            uk-icon="icon: list"></span>All</a></li>
                            <li><a href="<?php echo e(route('articles.create')); ?>"><span class="uk-margin-small-right"
                                                                             uk-icon="icon: plus-circle"></span>New</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo e(route('videos.index')); ?>"><span class="uk-margin-small-right"
                                                                  uk-icon="icon: play"></span>Videos</a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('slides.index')); ?>"><span class="uk-margin-small-right"
                                                                  uk-icon="icon: image"></span>Slider</a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('galleries.index')); ?>"><span class="uk-margin-small-right" uk-icon="icon: thumbnails"></span>Galleries</a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('admin.imagine.index')); ?>"><span class="uk-margin-small-right"
                                                                         uk-icon="icon: star"></span>IMAGINE</a>
                    </li>
                    <?php if(Auth::user()->role->name != 'Editor' && Auth::user()->role->name != 'Moderator'): ?>
                        <li>
                            <a href="<?php echo e(route('users.index')); ?>"><span class="uk-margin-small-right"
                                                                     uk-icon="icon: user"></span>Users</a>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a href="<?php echo e(route('mediamanager.index')); ?>"><span class="uk-margin-small-right"
                                                                        uk-icon="icon: camera"></span>Mediamanager</a>
                    </li>
                    <?php if(Auth::user()->role->name != 'Editor'): ?>
                        <li>
                            <a href="<?php echo e(route('locale.index')); ?>"><span class="uk-margin-small-right"
                                                                      uk-icon="icon: world"></span>Localization</a>
                        </li>

                        <li>
                            <a href="<?php echo e(route('settings.index')); ?>"><span class="uk-margin-small-right"
                                                                        uk-icon="icon: cog"></span>Settings</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>