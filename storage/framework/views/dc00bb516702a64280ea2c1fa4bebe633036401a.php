<?php if(session('notification')): ?>
    <script>
        var not = <?php echo json_encode(session('notification')); ?>;
        UIkit.notification({
            message: not.message,
            status: not.type,
            pos: 'top-right',
            timeout: 3000
        });
    </script>
<?php endif; ?>

