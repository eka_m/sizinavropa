<!DOCTYPE html>
<html lang="<?php echo e($currentlocale); ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo e(asset('/img/favicon/apple-touch-icon-57x57.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="<?php echo e(asset('/img/favicon/apple-touch-icon-114x114.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo e(asset('/img/favicon/apple-touch-icon-72x72.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="<?php echo e(asset('/img/favicon/apple-touch-icon-144x144.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo e(asset('/img/favicon/apple-touch-icon-60x60.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="120x120"
          href="<?php echo e(asset('/img/favicon/apple-touch-icon-120x120.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo e(asset('/img/favicon/apple-touch-icon-76x76.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="152x152"
          href="<?php echo e(asset('/img/favicon/apple-touch-icon-152x152.png')); ?>"/>
    <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-196x196.png')); ?>" sizes="196x196"/>
    <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-96x96.png')); ?>" sizes="96x96"/>
    <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-32x32.png')); ?>" sizes="32x32"/>
    <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-16x16.png')); ?>" sizes="16x16"/>
    <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-128.png')); ?>" sizes="128x128"/>
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF"/>
    <meta name="msapplication-TileImage" content="<?php echo e(asset('/img/favicon/mstile-144x144.png')); ?>"/>
    <meta name="msapplication-square70x70logo" content="<?php echo e(asset('/img/favicon/mstile-70x70.png')); ?>"/>
    <meta name="msapplication-square150x150logo" content="<?php echo e(asset('/img/favicon/mstile-150x150.png')); ?>"/>
    <meta name="msapplication-wide310x150logo" content="<?php echo e(asset('/img/favicon/mstile-310x150.png')); ?>"/>
    <meta name="msapplication-square310x310logo" content="<?php echo e(asset('/img/favicon/mstile-310x310.png')); ?>"/>
    <meta name="keywords" content="<?php echo $__env->yieldContent('keywords'); ?>">
    <meta name="description" content="<?php echo $__env->yieldContent('description'); ?>">
    <meta property="og:url" content="<?php echo $__env->yieldContent('s-url'); ?>"/>
    <meta property="og:type" content="<?php echo $__env->yieldContent('s-type'); ?>"/>
    <meta property="og:title" content="<?php echo $__env->yieldContent('s-title'); ?>"/>
    <meta property="og:description" content="<?php echo $__env->yieldContent('s-description'); ?>"/>
    <meta property="og:image" content="<?php echo $__env->yieldContent('s-image'); ?>"/>
    <link rel="image_src" href="<?php echo $__env->yieldContent('s-image'); ?>"/>
    <?php $__env->startSection('css'); ?>    
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.css">
        <link href="https://file.myfontastic.com/yi7HNSNkRQ5uFhbZ4vdnSK/icons.css" rel="stylesheet">
        
        <link rel="stylesheet" href="<?php echo e(asset('plugins/bootstrap-grid/grid.min.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('plugins/megamenu/css/navigation.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('plugins/megamenu/css/navigation.skin.rounded-boxed.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('plugins/semantic/components/menu.min.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">
    <?php echo $__env->yieldSection(); ?>
    <title>Sizin avropa <?php echo $__env->yieldContent('title'); ?></title>
</head>
<body>
<div id="app" class="container-fluid">
    <?php echo $__env->make('pages.new_home.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('pages.new_home.categories', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('content'); ?>
    <?php echo $__env->make('pages.new_home.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('plugins/jquery/jquery-3.2.1.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/megamenu/js/navigation.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/imageliquid/imgliquid.js')); ?>"></script>
<?php echo $__env->yieldSection(); ?>
<script src="<?php echo e(asset('js/custom.js')); ?>" type="text/javascript"></script>
</body>
</html>