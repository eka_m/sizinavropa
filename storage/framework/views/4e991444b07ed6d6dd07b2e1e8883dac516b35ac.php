<?php $__env->startSection('title', 'IMAGINE 2018'); ?>

<?php $__env->startSection('content'); ?>
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
        <?php echo $__env->make('pages.imagine.nav',['active' => 'partners'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
                <a href="<?php echo e(route('imagine')); ?>"><img src="<?php echo e(asset('img/imagine/imagine-new.png')); ?>" alt=""></a>
            </div>
        </div>
        <div class="uk-container uk-container-small bg-white uk-margin-auto uk-padding-small">
            <div class="page-title uk-text-center">
                <h1 class="uk-heading-line uk-h1"><span>Partners</span></h1>
            </div>
            <div class="page-content">
               
                <img src="<?php echo e(asset('img/imagine/alllogo.jpg')); ?>" alt="image partners" style="width:100%"> 
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.imagine', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>