<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo e(asset('/img/favicon/apple-touch-icon-57x57.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="<?php echo e(asset('/img/favicon/apple-touch-icon-114x114.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo e(asset('/img/favicon/apple-touch-icon-72x72.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="<?php echo e(asset('/img/favicon/apple-touch-icon-144x144.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo e(asset('/img/favicon/apple-touch-icon-60x60.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="120x120"
          href="<?php echo e(asset('/img/favicon/apple-touch-icon-120x120.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo e(asset('/img/favicon/apple-touch-icon-76x76.png')); ?>"/>
    <link rel="apple-touch-icon-precomposed" sizes="152x152"
          href="<?php echo e(asset('/img/favicon/apple-touch-icon-152x152.png')); ?>"/>
    <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-196x196.png')); ?>" sizes="196x196"/>
    <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-96x96.png')); ?>" sizes="96x96"/>
    <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-32x32.png')); ?>" sizes="32x32"/>
    <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-16x16.png')); ?>" sizes="16x16"/>
    <link rel="icon" type="image/png" href="<?php echo e(asset('/img/favicon/favicon-128.png')); ?>" sizes="128x128"/>
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF"/>
    <meta name="msapplication-TileImage" content="<?php echo e(asset('/img/favicon/mstile-144x144.png')); ?>"/>
    <meta name="msapplication-square70x70logo" content="<?php echo e(asset('/img/favicon/mstile-70x70.png')); ?>"/>
    <meta name="msapplication-square150x150logo" content="<?php echo e(asset('/img/favicon/mstile-150x150.png')); ?>"/>
    <meta name="msapplication-wide310x150logo" content="<?php echo e(asset('/img/favicon/mstile-310x150.png')); ?>"/>
    <meta name="msapplication-square310x310logo" content="<?php echo e(asset('/img/favicon/mstile-310x310.png')); ?>"/>
    <meta name="keywords" content="<?php echo $__env->yieldContent('keywords'); ?>">
    <meta name="description" content="<?php echo $__env->yieldContent('description'); ?>">
    <meta property="og:url" content="<?php echo $__env->yieldContent('s-url'); ?>"/>
    <meta property="og:type" content="<?php echo $__env->yieldContent('s-type'); ?>"/>
    <meta property="og:title" content="<?php echo $__env->yieldContent('s-title'); ?>"/>
    <meta property="og:description" content="<?php echo $__env->yieldContent('s-description'); ?>"/>
    <meta property="og:image" content="<?php echo $__env->yieldContent('s-image'); ?>"/>
    <link rel="image_src" href="<?php echo $__env->yieldContent('s-image'); ?>"/>
    <title>Sizin Avropa - <?php echo $__env->yieldContent('title'); ?></title>
    <?php $__env->startSection('css'); ?>
        <link rel="stylesheet" href="<?php echo e(asset('css/imagine.css')); ?>">
    <?php echo $__env->yieldSection(); ?>
</head>
<body style="overflow-x: hidden;">
<div id="app" class="wrapper uk-offcanvas-content">
    <?php $__env->startSection('content'); ?>
    <?php echo $__env->yieldSection(); ?>
        <div id="footer" class="footer-block uk-section uk-padding-remove">
            <div class="uk-container-large">
                <footer class="footer">
                    <p class="uk-position-bottom-center uk-margin-small-bottom"><?php echo e(date('Y')); ?> &copy; sizinavropa.az</p>
                </footer>
            </div>
        </div>
</div>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('plugins/jquery/jquery-3.2.1.min.js')); ?>" type="text/javascript"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.28/js/uikit.min.js"></script>

<?php echo $__env->yieldSection(); ?>
</body>
</html>