<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>Admin Panel <?php echo $__env->yieldContent('title'); ?></title>
    <link href="https://file.myfontastic.com/he5QifuJcf3PcdNYzuHAg3/icons.css" rel="stylesheet">
    <?php $__env->startSection('css'); ?>
        <link rel="stylesheet" href="<?php echo e(asset('/adminpanel/css/app.css')); ?>">
    <?php echo $__env->yieldSection(); ?>
    
</head>
<body>

<div id="app" class="wrapper uk-offcanvas-content">
    <?php $__env->startSection('content'); ?>

    <?php echo $__env->yieldSection(); ?>
</div>
<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('adminpanel/js/app.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('adminpanel/js/custom.js')); ?>" type="text/javascript"></script>
<?php echo $__env->yieldSection(); ?>
</body>
</html>