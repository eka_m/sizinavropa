<section class="row lastNews light-bg mt-5 p-3 row-content">
    <div class="col">
        <div class="row">
            <div class="col blockheader blockheader--whiteblock">
                <span class="blockheader__name"><?php echo e(__('words.lastnews')); ?></span>
            </div>
        </div>
        <div class="row lastNews__row no-gutters">
            <div class="col">
                <div class="row lastNewsSlider">
                    <?php $__currentLoopData = $news->slice(0, 10); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-3">
                        <article class="lastNews__row__item" onclick="location.href = '/<?php echo e($currentlocale); ?>/article/<?php echo e($article->url); ?>'">
                            <a class="link_reset" href="/<?php echo e($currentlocale); ?>/article/<?php echo e($article->url); ?>" title="<?php echo e($article->name); ?>">
                                <div class="lastNews__row__item__image slick-imgLiquid" data-imgLiquid-horizontalAlign="center" data-imgLiquid-verticalAlign="bottom">
                                    <img src="<?php echo e(setImage($article->image, 'lastnews')); ?>" class="fluid image" alt="">
                                </div>
                            </a>
                            
                            <header class="lastNews__row__item__info gr--<?php echo e($article->page->color); ?>">
                                <a href="/<?php echo e($currentlocale); ?>/page/<?php echo e($article->page->url); ?>" class="lastNews__row__item__info__category d-inline-block cl--<?php echo e($article->page->color); ?>">
                                        <?php if($currentlocale == env('DEFAULTLOCALE')): ?><?php echo e($article->page->name); ?><?php else: ?> <?php echo e($article->page->name_en); ?><?php endif; ?>
                                    </a>
                                <a class="link_reset" href="/<?php echo e($currentlocale); ?>/article/<?php echo e($article->url); ?>" title="<?php echo e($article->name); ?>">
                                    <h3><?php echo e($article->name); ?></h3>
                                    <p><?php echo e(str_limit($article->short,200)); ?></p>
                                    <div class="lastNews__row__item__info__dateviews">
                                        <span class="lastNews__row__item__info__dateviews__date"><?php echo e($article->created_at); ?></span>
                                        <span>/</span>
                                        <span class="lastNews__row__item__info__dateviews__views">
                                            <i class="sa-eye"></i> <?php echo e($article->fakeviews ? $article->fakeviews : $article->views); ?>

                                        </span>
                                    </div>
                                </a>
                            </header>
                        </article>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
        <div class="row blockgo blockgo--white">
            <div class="col pt-0 mt-0">
                <a href="javascript:void(0)" class="blockgo__arrow blockgo__arrow--left lastNewsLeft">&larr;</a>
                <a href="javascript:void(0)" class="blockgo__arrow blockgo__arrow--right lastNewsRight">&rarr;</a>
            </div>
        </div>
    </div>
</section>