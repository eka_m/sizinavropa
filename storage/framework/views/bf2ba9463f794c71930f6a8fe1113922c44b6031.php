<section class="row mainslider light-bg mt-3 row-content">
    <div class="col p-lg-5">
        <div class="row no-gutters">
            <div class="col-md-8 p-0">
                <div class="slickSld mainSliderSlick" data-slick='{
                "arrows": false,
                "asNavFor": ".mainSliderSlickThumbs"
                }'>
                    <?php $__currentLoopData = $slides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div>
                            <article class="mainslider__item mainslider__item--big">
                                <a href="<?php echo e($slide->link); ?>">
                                    <div class="mainslider__item__image slick-imgLiquid">
                                        <img class="fluid" src="/uploads/<?php echo e($slide->image); ?>">
                                    </div>
                                    <header class="mainslider__item__overlay cl--red">
                                        <h3><?php echo e($slide->name); ?></h3>
                                        <?php if($slide->description): ?>
                                            <p><?php echo e($slide->description); ?></p>
                                        <?php endif; ?>
                                    </header>
                                </a>
                            </article>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="col-md-4 p-0 hidden-xs mainSliderSlickThumbsContainer">
                <slick class="slickSld mainSliderSlickThumbs" data-slick='{
                                "arrows": false,
                                "verticalSwiping":true,
                                "vertical": true,
                                "infinite": true,
                                "slidesToShow": 2,
                                "slidesToScroll":1,
                                "adaptiveHeight":true,
                                "autoplay":true,
                                "autoplaySpeed": 3000,
                                "asNavFor": ".mainSliderSlick",
                                "focusOnSelect": true,
                                "responsive": [{
                                        "breakpoint": 768,
                                        "settings": {
                                            "vertical": false,
                                            "arrows": false,
                                            "slidesToShow": 3,
                                            "slidesToScroll": 1
                                        }
                                    },
                                    {
                                        "breakpoint": 560,
                                        "settings": {
                                            "vertical": false,
                                            "slidesToShow": 2,
                                            "slidesToScroll": 1
                                        }
                                    } , {
                                        "breakpoint": 461,
                                        "settings": {
                                            "vertical": false,
                                            "slidesToShow":1,
                                            "slidesToScroll": 1
                                        }
                                    }]
                                }'>
                    <?php $__currentLoopData = $slides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <article class="mainslider__item mainslider__item--small">
                            
                            <div class="mainslider__item__image slick-imgLiquid">
                                <img class="fluid image" src="/uploads/<?php echo e($slide->image); ?>">
                            </div>
                            <header class="mainslider__item__overlay mainslider__item--small--overlay cl--blue">
                                <h3><?php echo e($slide->name); ?></h3>
                                
                                
                                
                            </header>
                            
                        </article>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </slick>
            </div>
        </div>
    </div>
</section>
