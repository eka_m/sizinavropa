<?php $__env->startSection('title','Результаты поиска'); ?>
<?php $__env->startSection('content'); ?>
<section class="row pageArticles mt-3">
    <div class="col row-content bg--white">
        <div class="row">
            <?php $__empty_1 = true; $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div class="col-md-12 col-lg-6">
                    <article class="row pageArticles__item">
                        <div class="pageArticles__item__image imgLiquid p-0 col-sm-6 col-md-6 col-lg-6 br--<?php echo e($article->page->color); ?>">
                            <img src="/uploads<?php echo e($article['image']); ?>" class="fluid" alt="">
                        </div>
                        <header class="pageArticles__item__info col-sm-6 col-md-6 col-lg-6 ">
                            <h3><?php echo e($article['name']); ?></h3>
                            <p><?php echo e(str_limit($article['short'],200)); ?></p>
                            <a href="/article/<?php echo e($article['url']); ?>"
                               class="pageArticles__item__info__readmore br--<?php echo e($article->page->color); ?> bg--<?php echo e($article->page->color); ?>">
                                &rarr;
                            </a>
                        </header>
                    </article>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <div class="col-12 py-5">
                    <div class="text-right py-5" style="text-align:center;">
                        <h4>Ohh.. Sorry..</h4>
                        <h5>Nothink found</h5>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col p-3 d-flex align-items-center">
                <div class="d-inline-block m-auto">
                    <?php echo e($articles->appends(['query' => Request::get('query')])->links('vendor.pagination.semantic')); ?>

                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>