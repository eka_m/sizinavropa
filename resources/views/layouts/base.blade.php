<!DOCTYPE html>
<html lang="{{$currentlocale}}">

<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121877875-1"></script>
  <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
          dataLayer.push(arguments);
      }

      gtag('js', new Date());
      gtag('config', 'UA-121877875-1');

  </script>

  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{asset('/img/favicon/apple-touch-icon-57x57.png')}}"/>
  <link rel="apple-touch-icon-precomposed" sizes="114x114"
        href="{{asset('/img/favicon/apple-touch-icon-114x114.png')}}"/>
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('/img/favicon/apple-touch-icon-72x72.png')}}"/>
  <link rel="apple-touch-icon-precomposed" sizes="144x144"
        href="{{asset('/img/favicon/apple-touch-icon-144x144.png')}}"/>
  <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{asset('/img/favicon/apple-touch-icon-60x60.png')}}"/>
  <link rel="apple-touch-icon-precomposed" sizes="120x120"
        href="{{asset('/img/favicon/apple-touch-icon-120x120.png')}}"/>
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{asset('/img/favicon/apple-touch-icon-76x76.png')}}"/>
  <link rel="apple-touch-icon-precomposed" sizes="152x152"
        href="{{asset('/img/favicon/apple-touch-icon-152x152.png')}}"/>
  <link rel="icon" type="image/png" href="{{asset('/img/favicon/favicon-196x196.png')}}" sizes="196x196"/>
  <link rel="icon" type="image/png" href="{{asset('/img/favicon/favicon-96x96.png')}}" sizes="96x96"/>
  <link rel="icon" type="image/png" href="{{asset('/img/favicon/favicon-32x32.png')}}" sizes="32x32"/>
  <link rel="icon" type="image/png" href="{{asset('/img/favicon/favicon-16x16.png')}}" sizes="16x16"/>
  <link rel="icon" type="image/png" href="{{asset('/img/favicon/favicon-128.png')}}" sizes="128x128"/>
  <meta name="application-name" content="&nbsp;"/>
  <meta name="msapplication-TileColor" content="#FFFFFF"/>
  <meta name="msapplication-TileImage" content="{{asset('/img/favicon/mstile-144x144.png')}}"/>
  <meta name="msapplication-square70x70logo" content="{{asset('/img/favicon/mstile-70x70.png')}}"/>
  <meta name="msapplication-square150x150logo" content="{{asset('/img/favicon/mstile-150x150.png')}}"/>
  <meta name="msapplication-wide310x150logo" content="{{asset('/img/favicon/mstile-310x150.png')}}"/>
  <meta name="msapplication-square310x310logo" content="{{asset('/img/favicon/mstile-310x310.png')}}"/>
  <meta name="keywords" content="@yield('keywords')">
  <meta name="description" content="@yield('description')">

  @yield('social-tags')

  @section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.css">
    <link href="https://file.myfontastic.com/yi7HNSNkRQ5uFhbZ4vdnSK/icons.css" rel="stylesheet"> {{--
    <link href="https://file.myfontastic.com/9zDA9EHJfzT3UPNqg6iTqf/icons.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-grid/grid.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/megamenu/css/navigation.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/megamenu/css/navigation.skin.rounded-boxed.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/semantic/components/menu.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  @show
  <title>Sizin avropa @yield('title')</title>
</head>

<body>

<div id="app" class="container-fluid">
  @include('pages.home.navigation')
  @include('pages.home.categories')
  @yield('content')
  @include('pages.home.footer')
</div>

@section('js')
  <script src="{{asset('plugins/jquery/jquery-3.2.1.min.js')}}"></script>
  <script src="{{asset('plugins/megamenu/js/navigation.min.js')}}"></script>
  <script src="{{asset('plugins/imageliquid/imgliquid.js')}}"></script>
  <script src="{{asset('plugins/snowfall/jquery.flurry.min.js')}}"></script>
@show
<script src="{{asset('js/custom.js')}}" type="text/javascript"></script>
</body>

</html>