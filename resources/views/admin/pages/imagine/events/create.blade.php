@extends('admin.layouts.base')
@section('title', '::IMAGINE - New Event')
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/tinymce/init.js')}}"></script>
@endsection
@section('content')
    <div class="uk-section">
        <div class="uk-container head-font">
            <form action="{{route('admin.imagine.store.event')}}" method="post">
                {{ csrf_field() }}
                <ul class="uk-subnav uk-subnav-pill" uk-switcher="animation: uk-animation-slide-bottom-medium">
                    <li><a href="#">Name & Date & Cover</a></li>
                    <li><a href="#">Content</a></li>
                </ul>
                <ul class="uk-switcher uk-margin">
                    <li>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Name</div>
                            <input type="text" class="uk-input uk-form-large" name="name" placeholder="Event name" value="">
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Date & Time</div>
                            <flatpickr class="uk-input uk-form-large" :conf="{enableTime: true,time_24hr:true}"
                                       currentdate="{!! date('Y-m-d H:s') !!}" inputname="date"></flatpickr>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Venue</div>
                            <input type="text" class="uk-input uk-form-large" name="venue" placeholder="Venue" value="">
                        </div>
                        <div class="uk-margin">
                            <imageinput inputname="image" btn="Choose image"></imageinput>
                        </div>
                    </li>
                    <li>
                        <textarea name="content" id="editor" cols="30" rows="10" class="uk-textarea"></textarea>
                    </li>
                </ul>
                <div class="uk-margin">
                    <button class="uk-button uk-button-success uk-float-right">Save</button>
                </div>
            </form>
        </div>
    </div>

@endsection