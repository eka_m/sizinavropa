@extends('admin.layouts.base')
@section('title','::Imagine - Events')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <div>
                <a href="{{route('admin.imagine.create.event')}}" class="uk-icon-button uk-button-secondary uk-float-right"
                   uk-icon="icon: plus"></a>
            </div>
        </div>
        <div class="uk-container uk-container-small uk-margin-bottom">
            <div class="uk-child-width-1-3@m" uk-grid-parallax>
                @foreach($events as $event)
                    <div>
                        <div class="uk-card uk-card-default uk-card-body">
                            <div class="uk-margin">
                                <span class="uk-display-block">{{$event->date ? date("d F H:i", strtotime($event->date)) : ''}}</span>
                                <span>{{$event->venue}}</span>
                            </div>
                            {{--<div class="uk-card-media-top">--}}
                                {{--<img src="/uploads/{{$event->image}}" alt="">--}}
                            {{--</div>--}}
                            <a href="/imagine/event/{{$event->slug}}" class="uk-text-danger" target="_blank">{{$event->name}} </a>
                            <div class="uk-margin">
                                <a href="{{route('admin.imagine.edit.event', $event->id)}}" class="uk-icon-button uk-button-primary" uk-icon="icon: pencil"></a>
                                <a href="{{route('admin.imagine.delete.event', $event->id)}}" class="uk-icon-button uk-button-danger" uk-icon="icon: trash"></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection