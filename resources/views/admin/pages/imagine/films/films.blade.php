@extends('admin.layouts.base')
@section('title','::Imagine - Films')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <div>
                <a href="{{route('admin.imagine.create.film')}}" class="uk-icon-button uk-button-secondary uk-float-right"
                   uk-icon="icon: plus"></a>
            </div>
        </div>
        <div class="uk-container uk-container-small uk-margin-bottom">
            <ul class="uk-list uk-list-striped">
                @foreach($films as $film)
                    <li>
                        <a href="/imagine/movie/{{$film->slug}}" class="uk-button-text uk-text-danger" target="_blank">{{$film->title}} </a>
                        <span>{{$film->date ? date("d F g:i a", strtotime($film->date)) : ''}}</span> / {{$film->venue}}
                        <div class="uk-display-inline-block uk-float-right">
                            <a href="{{route('admin.imagine.edit.film', $film->id)}}" class="uk-icon-button uk-button-primary" uk-icon="icon: pencil"></a>
                            <a href="{{route('admin.imagine.delete.film', $film->id)}}" class="uk-icon-button uk-button-danger" uk-icon="icon: trash"></a>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection