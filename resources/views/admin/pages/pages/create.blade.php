@extends('admin.layouts.base')
@section('title', '::New Page')
{{--@section('css')--}}
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/tinymce/init.js')}}"></script>
@endsection
@section('content')
    <div class="uk-section">
        <div class="uk-container head-font">
            <form action="{{route('pages.store')}}" method="post">
                {{ csrf_field() }}
                <ul class="uk-subnav uk-subnav-pill" uk-switcher="animation: uk-animation-slide-bottom-medium">
                    <li><a href="#">Title & Cover</a></li>
                    <li><a href="#">Content</a></li>
                    <li><a href="#">SEO</a></li>
                </ul>
                <ul class="uk-switcher uk-margin">
                    <li>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Name</div>
                            <input type="text" class="uk-input uk-form-large" id="translit-it" name="name" placeholder="Page name"
                                   value="">
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Slug</div>
                            <slug inputname="url"></slug>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Placement</div>
                            <select class="uk-select uk-form-large" name="place">
                                <option value="mainmenu">Main menu</option>
                                <option value="topmenu">Top menu</option>
                            </select>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Type</div>
                            <select class="uk-select uk-form-large" name="type">
                                <option value="regular">Regular</option>
                                <option value="route">Route</option>
                                <option value="custom">Custom</option>
                            </select>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Parent</div>
                            <select class="uk-select uk-form-large" name="parent_id">
                                <option value="0">No parent</option>
                                @foreach($pages as $page)
                                    <option value="{{$page->id}}">{{$page->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="uk-margin">
                            <imageinput inputname="cover" btn="Choose Cover"></imageinput>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Cover overlay opacity</div>
                            <range class="uk-form-width-small" name="coveropacity" min="0" max="1" step="0.1" default="0.5"></range>
                        </div>
                    </li>
                    <li>
                        {{--  <medium width="100%" name="content"></medium>  --}}
                        <textarea name="content" id="editor" cols="30" rows="10" class="uk-textarea"></textarea>
                    </li>
                    <li>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Keywords</div>
                            <input type="text" class="uk-input uk-form-large" name="keywords" value=""
                                   placeholder="Keywords">
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Description</div>
                            <textarea name="description" class="uk-textarea uk-form-large" rows="5"
                                      placeholder="Description"></textarea>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Social buttons</div>
                            <select name="social" class="uk-select uk-form-width-xsmall">
                                <option value="0">OFF</option>
                                <option value="1">ON</option>
                            </select>
                        </div>
                    </li>
                </ul>
                <div class="uk-margin">
                    <button class="uk-button uk-button-secondary uk-float-right">Save</button>
                </div>
            </form>
        </div>
    </div>

@endsection