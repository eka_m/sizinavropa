@extends('admin.layouts.base') 
@section('title', '::New article') 
@section('js') @parent
<script type="text/javascript" src="{{asset('plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/tinymce/init.js')}}"></script>
@endsection
 
@section('content')
<div class="uk-section">
    <div class="uk-container head-font">
        <form action="{{route('articles.store')}}" method="post">
            {{ csrf_field() }}
            <ul class="uk-subnav uk-subnav-pill" uk-switcher="animation: uk-animation-slide-bottom-medium">
                <li><a href="#">Main</a></li>
                <li><a href="#">Pictures</a></li>
                <li><a href="#">Content</a></li>
                <li><a href="#">SEO</a></li>
                <li><a href="#">Event</a></li>
            </ul>
            <ul class="uk-switcher uk-margin">
                <li>
                    <div class="uk-margin">
                        <div class="uk-form-label uk-animation-slide-bottom">Name</div>
                        <input type="text" class="uk-input uk-form-large" name="name" placeholder="Article name" value="{{old('name')}}">
                        <input type="hidden" name="status" value="0">
                    </div>
                    <div class="uk-margin">
                        <div class="uk-form-label uk-animation-slide-bottom">Short text</div>
                        <textarea name="short" class="uk-textarea uk-form-large" rows="5" placeholder="Short text">{{old('short')}}</textarea>
                    </div>
                    <div class="uk-margin">
                        <div class="uk-form-label uk-animation-slide-bottom">Page</div>
                        <select class="uk-select uk-form-large" name="page_id">
                                @foreach($pages as $page)
                                    <option value="{{$page->id}}">{{$page->name}}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="uk-margin">
                        <translate locales="{{$locales}}"></translate>
                    </div>
                    <div class="uk-margin">
                        <div class="uk-form-label uk-animation-slide-bottom">Cover overlay opacity</div>
                        <range class="uk-form-width-small" name="coveropacity" min="0" max="1" step="0.1" default="0.5"></range>
                    </div>
                </li>
                <li>
                    <div class="uk-margin" uk-grid>
                        <div>
                            <label>Last News</label>
                            <img-crop name="image[lastnews]" aspect-ratio="5:8" :min-size="{width: 300, height:480}" :to-all="true"></img-crop>
                        </div>
                        <div>
                            <label>Interesting,category,footer</label>
                            <img-crop name="image[interesting]" aspect-ratio="1:1" :min-size="{width: 350, height:350}"></img-crop>
                        </div>
                        <div>
                            <label>Cover</label>
                            <img-crop name="cover[cover]" aspect-ratio="683:250" :min-size="{width: 1366, height:500}"></img-crop>
                        </div>
                    </div>
                </li>
                <li>
                    <textarea name="content" id="editor" cols="30" rows="10" class="uk-textarea">{!! old('content') !!}</textarea>
                </li>
                <li>
                    <div class="uk-margin">
                        <div class="uk-form-label uk-animation-slide-bottom">Keywords</div>
                        <input type="text" class="uk-input uk-form-large" name="keywords" value="{{old('keywords')}}" placeholder="Keywords">
                    </div>
                    <div class="uk-margin">
                        <div class="uk-form-label uk-animation-slide-bottom">Description</div>
                        <textarea name="description" class="uk-textarea uk-form-large" rows="5" placeholder="Description">{{old('description')}}</textarea>
                    </div>
                </li>
                <li>
                    <div class="uk-margin uk-child-width-1-2 uk-grid">
                        <div>
                            <div class="uk-form-label uk-animation-slide-bottom">Start date</div>
                            <flatpickr class="uk-input uk-form-large" :conf="{enableTime: false,time_24hr:true}" currentdate="{!! date('Y-m-d') !!}"
                                inputname="date"></flatpickr>
                        </div>
                        <div>
                            <div class="uk-form-label uk-animation-slide-bottom">End date</div>
                            <flatpickr class="uk-input uk-form-large" :conf="{enableTime: false,time_24hr:true}" currentdate="{!! date('Y-m-d') !!}"
                                inputname="end_date"></flatpickr>
                        </div>
                    </div>
                    <div class="uk-margin uk-child-width-1-2 uk-grid">
                        <div>
                            <div class="uk-form-label uk-animation-slide-bottom">Start</div>
                            <flatpickr class="uk-input uk-form-large" :conf="{enableTime: true,time_24hr:true, noCalendar:true}" currentdate="{!! date('H:s') !!}"
                                inputname="start"></flatpickr>
                        </div>
                        <div>
                            <div class="uk-form-label uk-animation-slide-bottom">End</div>
                            <flatpickr class="uk-input uk-form-large" :conf="{enableTime: true,time_24hr:true, noCalendar:true}" currentdate="{!! date('H:s') !!}"
                                inputname="end"></flatpickr>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <div class="uk-form-label uk-animation-slide-bottom">Venue</div>
                        <input type="text" class="uk-input uk-form-large" name="venue" value="" placeholder="Venue">
                    </div>
                    <div class="uk-margin">
                        <div class="uk-form-label uk-animation-slide-bottom">Event name</div>
                        <editor inputname="event_name"></editor>
                    </div>
                </li>
            </ul>
            <div class="uk-margin">
                <button class="uk-button uk-button-secondary uk-float-right">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection