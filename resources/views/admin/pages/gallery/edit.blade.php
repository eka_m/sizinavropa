@extends('admin.layouts.base')
@section('title','::Create Gallery')
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <h3>Edit Gallery</h3>
        </div>
        <div class="uk-container uk-container-small">
            <form action="{{route('galleries.update',$gallery->id)}}" method="post">
                {{csrf_field()}}
                {{method_field('PUT')}}
                <div class="uk-magin">
                    <div class="uk-padding-small">
                        <label class="uk-form-label uk-animation-slide-bottom">Gallery name</label>
                        <input type="text" class="uk-input uk-form-large" name="name" value="{{$gallery->name}}"
                               placeholder="Gallery name">
                    </div>
                </div>
                <div class="uk-magin">
                    <div class="uk-padding-small">
                        <label class="uk-form-label uk-animation-slide-bottom">Gallery description</label>
                        <textarea name="description" class="uk-textarea uk-form-large" id="" cols="30" rows="5"
                                  placeholder="Gallery description">{{$gallery->description}}</textarea>
                    </div>
                </div>
                <div class="uk-magin">
                    <div class="uk-padding-small">
                        <label class="uk-form-label uk-animation-slide-bottom">Gallery type</label>
                        <select class="uk-select uk-form-large" name="type">
                            <option value="default" @if($gallery->type == 'default') selected @endif>Default</option>
                            <option value="tiles" @if($gallery->type == 'tiles') selected @endif>Tiles</option>
                            <option value="tilesgrid" @if($gallery->type == 'tilesgrid') selected @endif>Tiles grid
                            </option>
                            <option value="carousel" @if($gallery->type == 'carousel') selected @endif>Carousel</option>
                            <option value="compact" @if($gallery->type == 'compact') selected @endif>Compact</option>
                            <option value="grid" @if($gallery->type == 'grid') selected @endif>Grid</option>
                            <option value="slider" @if($gallery->type == 'slider') selected @endif>Slider</option>
                        </select>
                    </div>
                </div>
                <div class="uk-margin">
                    <div class="uk-padding-small">
                        <gallery-images :current="{{json_encode($gallery->images)}}"></gallery-images>
                    </div>
                </div>
                <div class="uk-margin">
                    <div class="uk-padding-small">
                        <button class="uk-button uk-button-primary uk-float-right">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection