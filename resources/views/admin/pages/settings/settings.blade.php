@extends('admin.layouts.base')
@section('title','::Settings')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small">
            <div class="uk-child-width-1-3@m" uk-grid>
            <popup settings="{{json_encode($settings['popup'])}}"></popup>
            </div>
        </div>
    </div>
@endsection