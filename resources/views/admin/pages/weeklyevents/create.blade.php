@extends('admin.layouts.base')
@section('title', '::New Event')
@section('content')
    <div class="uk-section">
        <div class="uk-container head-font">
            <form action="{{route('events.store')}}" method="post">
                {{ csrf_field() }}
                <div class="uk-margin">
                    <flatpickr class="uk-input uk-form-large" :conf="{enableTime: true,time_24hr:true}"
                               currentdate="{!! date('Y-m-d H:s') !!}" inputname="date"></flatpickr>
                </div>
                <div class="uk-margin">
                    <label>Name</label>
                    <input type="text" name="name" class="uk-input uk-input-large">
                </div> <div class="uk-margin">
                    <label>Article</label>
                    <attach-article></attach-article>
                </div>
                <div class="uk-margin">
                    <button class="uk-button uk-button-secondary uk-float-right">Save</button>
                </div>
            </form>
        </div>
    </div>

@endsection