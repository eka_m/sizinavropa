@extends('admin.layouts.base')
@section('title','::Events')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <a href="{{route('events.create')}}" class="uk-icon-button uk-button-secondary uk-float-right"
               uk-icon="icon: plus"></a>
        </div>
        <div class="uk-container uk-container-small">
            <events data="{{json_encode($events)}}"></events>
        </div>
    </div>
@endsection