@extends('admin.layouts.base')
@section('title', '::New Page')
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection
@section('content')
    <div class="uk-section">
        <div class="uk-container head-font">
            <form action="{{route('pages.update', $page->id)}}" method="POST">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <ul class="uk-subnav uk-subnav-pill" uk-switcher="animation: uk-animation-slide-bottom-medium">
                    <li><a href="#">Title & Cover</a></li>
                    <li><a href="#">Content</a></li>
                    <li><a href="#">SEO</a></li>
                </ul>
                <ul class="uk-switcher uk-margin">
                    <li>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Name</div>
                            <input type="text" class="uk-input uk-form-large" id="translit-it" name="name" placeholder="Page name"
                                   value="{{$page->name}}">
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Slug</div>
                            <slug inputname="url" oldval="{{$page->url}}"></slug>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Placement</div>
                            <select class="uk-select uk-form-large" name="place">
                                <option value="mainmenu"   @if($page->place == 'mainmenu') selected @endif>Main menu</option>
                                <option value="topmenu" @if($page->place == 'topmenu') selected @endif>Top menu</option>
                            </select>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Type</div>
                            <select class="uk-select uk-form-large" name="type">
                                <option value="regular" @if($page->type == 'regular') selected @endif>Regular</option>
                                <option value="route" @if($page->type == 'route') selected @endif>Route</option>
                                <option value="custom" @if($page->type == 'custom') selected @endif>Custom</option>
                            </select>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Parent</div>
                            <select class="uk-select uk-form-large" name="parent_id">
                                <option value="0">No parent</option>
                                @foreach($pages as $item)
                                    @continue($page->id === $item->id)
                                    <option value="{{$item->id}}"
                                            @if($page->parent && $page->parent->id === $item->id) selected @endif>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="uk-margin">
                            <imageinput inputname="cover" img="{{$page->cover}}" btn="Choose cover"></imageinput>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Cover overlay opacity</div>
                            <range class="uk-form-width-small" name="coveropacity" min="0" max="1" step="0.1" default="{{$page->coveropacity}}"></range>
                        </div>
                    </li>
                    <li>
                        <medium width="100%" name="content">{!! $page->content !!}</medium>
                    </li>
                    <li>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Keywords</div>
                            <input type="text" class="uk-input uk-form-large" name="keywords" placeholder="Keywords"
                                   value="{{$page->keywords}}">
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Description</div>
                            <textarea name="description" class="uk-textarea uk-form-large" rows="5"
                                      placeholder="Description">{{$page->description}}</textarea>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Social buttons</div>
                            <select name="social" class="uk-select uk-form-width-xsmall">
                                <option value="0" @if(!$page->social) selected @endif >OFF</option>
                                <option value="1" @if($page->social) selected @endif>ON</option>
                            </select>
                        </div>
                    </li>
                </ul>
                <div class="uk-margin">
                    <a href="{{route('pages.index')}}" class="uk-button uk-button-danger uk-float-left">Exit without
                        saving</a>
                    <button class="uk-button uk-button-secondary uk-float-right">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection