@extends('admin.layouts.base')
@section('title','::Fantazia')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <a href="{{route('admin.fantazia.program')}}" class="uk-button uk-button-primary">Program</a>
            <a href="{{route('admin.fantazia.films')}}" class="uk-button uk-button-primary">Films</a>
            <a href="{{route('admin.fantazia.events')}}" class="uk-button uk-button-primary">Events</a>
            <a href="{{route('admin.fantazia.masterclasses')}}" class="uk-button uk-button-primary">MasterClasses</a>
        </div>
    </div>
@endsection