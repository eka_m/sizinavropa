@extends('admin.layouts.base')
@section('title','::Fantazia - Masterclasses')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <div>
                <a href="{{route('admin.fantazia.create.masterclass')}}" class="uk-icon-button uk-button-secondary uk-float-right"
                   uk-icon="icon: plus"></a>
            </div>
        </div>
        <div class="uk-container uk-container-small uk-margin-bottom">
            <div class="uk-child-width-1-3@m" uk-grid-parallax>
                @foreach($masterclasses as $masterclass)
                    <div>
                        <div class="uk-card uk-card-default uk-card-body">
                            <div class="uk-margin">
                                <span class="uk-display-block">{{$masterclass->date ? date("d F H:i", strtotime($masterclass->date)) : ''}}</span>
                                <span>{{$masterclass->venue}}</span>
                            </div>
                            {{--<div class="uk-card-media-top">--}}
                                {{--<img src="/uploads/{{$masterclass->image}}" alt="">--}}
                            {{--</div>--}}
                            <a href="/fantazia/masterclass/{{$masterclass->slug}}" class="uk-text-danger" target="_blank">{{$masterclass->name}} </a>
                            <div class="uk-margin">
                                <a href="{{route('admin.fantazia.edit.masterclass', $masterclass->id)}}" class="uk-icon-button uk-button-primary" uk-icon="icon: pencil"></a>
                                <a href="{{route('admin.fantazia.delete.masterclass', $masterclass->id)}}" class="uk-icon-button uk-button-danger" uk-icon="icon: trash"></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection