@extends('admin.layouts.base')
@section('title','::Fantazia - Create Film')
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <form method="post" action="{{route('admin.fantazia.store.film')}}">
                {{csrf_field()}}
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Title</div>
                    <input type="text" class="uk-input uk-form-large" name="title" placeholder="Film title"
                           value="">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Original Title</div>
                    <input type="text" class="uk-input uk-form-large" name="org_title" placeholder="Film original title"
                           value="">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Year</div>
                    <input type="text" class="uk-input uk-form-large" name="year" placeholder="Year"
                           value="">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Country</div>
                    <input type="text" class="uk-input uk-form-large" name="country" placeholder="Country"
                           value="">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Length</div>
                    <input type="text" class="uk-input uk-form-large" name="length" placeholder="Length"
                           value="">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Genre</div>
                    <input type="text" class="uk-input uk-form-large" name="genre" placeholder="Genre"
                           value="">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Director</div>
                    <input type="text" class="uk-input uk-form-large" name="director" placeholder="Director"
                           value="">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Writers</div>
                    <input type="text" class="uk-input uk-form-large" name="writers" placeholder="Writers"
                           value="">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Cast</div>
                    <input type="text" class="uk-input uk-form-large" name="cast" placeholder="Cast"
                           value="">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Synopsis</div>
                    <textarea name="synopsis" class="uk-textarea uk-form-large" id="" cols="30" rows="10" placeholder="Synopsis"></textarea>
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Date</div>
                    <flatpickr class="uk-input uk-form-large" :conf="{enableTime: true,time_24hr:true}"
                               currentdate="{!! date('Y-m-d H:s') !!}" inputname="date"></flatpickr>
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Venue</div>
                    <input type="text" class="uk-input uk-form-large" name="venue" placeholder="Venue"
                           value="">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Language</div>
                    <input type="text" class="uk-input uk-form-large" name="lang" placeholder="Language"
                           value="">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Subtitle</div>
                    <input type="text" class="uk-input uk-form-large" name="subtitle" placeholder="Subtitle"
                           value="">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Poster</div>
                    <imageinput inputname="poster" btn="Choose poster"></imageinput>
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Trailer</div>
                    <input type="text" class="uk-input uk-form-large" name="trailer" placeholder="Trailer"
                           value="">
                </div>
                <button class="uk-button uk-button-success uk-float-right">Save</button>
            </form>
        </div>
    </div>
@endsection