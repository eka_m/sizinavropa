@extends('admin.layouts.base')
@section('title','::Fantazia Program')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <div>
                <a href="{{route('admin.fantazia.create.program.event')}}"
                   class="uk-icon-button uk-button-secondary uk-float-right"
                   uk-icon="icon: plus"></a>
            </div>
        </div>
        <div class="uk-container uk-container-small uk-margin-bottom">
            <div class="uk-child-width-1-2@m" uk-grid-parallax>

                @foreach($program as $key => $group)
                    <div>
                        <div class="uk-card uk-card-body uk-card-default">
                            <h3 class="uk-text-danger">{{date("l", strtotime($key))}} {{date("d F", strtotime($key))}}</h3>
                            @foreach($group as $k => $value)
                                <h4 class="uk-text-primary">{{$k}}</h4>
                                @foreach($value->sortBy('start') as $item)
                                    <div class="programitems">
                                        <span class="uk-text-small uk-text-success">{{$item->start ? date("H:i", strtotime($item->start)).'-' : ''}}{{$item->end ? date("H:i", strtotime($item->end)) : ''}}</span> {!! $item->title !!}
                                        <a href="{{route('admin.fantazia.edit.program.event',$item->id)}}" class="uk-text-primary uk-text-small" uk-icon="icon:pencil"></a>
                                        <a href="{{route('admin.fantazia.delete.program.event',$item->id)}}" class="uk-text-danger uk-text-small" uk-icon="icon:trash"></a>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

<style>
    .programitems p {
        display: inline !important;
    }
</style>