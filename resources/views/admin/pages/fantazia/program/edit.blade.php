@extends('admin.layouts.base')
@section('title','::Fantazia - Edit Program')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <form  method="post" action="{{route('admin.fantazia.update.program.event',$program->id)}}">
                {{method_field('PUT')}}
                {{csrf_field()}}
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Date</div>
                    <flatpickr class="uk-input uk-form-large" :conf="{enableTime: false,time_24hr:true}"
                               currentdate="{{$program->date}}" inputname="date"></flatpickr>
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Start</div>
                    <flatpickr class="uk-input uk-form-large" :conf="{enableTime: true,time_24hr:true, noCalendar:true}"
                               currentdate="{{$program->start}}" inputname="start"></flatpickr>
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">End</div>
                    <flatpickr class="uk-input uk-form-large" :conf="{enableTime: true,time_24hr:true, noCalendar:true}"
                               currentdate="{{$program->end}}" inputname="end"></flatpickr>
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Venue</div>
                    <input type="text" class="uk-input uk-form-large" name="venue" value="{{$program->venue}}" placeholder="Venue">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Title</div>
                    <editor inputname="title">{!! $program->title !!}</editor>
                </div>
                <button class="uk-button uk-button-success uk-float-right">Save</button>
            </form>
        </div>
    </div>
@endsection