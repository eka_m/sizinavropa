@extends('admin.layouts.base')
@section('title','::Fantazia - Create Program')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <form  method="post" action="{{route('admin.fantazia.store.program.event')}}">
                {{csrf_field()}}
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Date</div>
                    <flatpickr class="uk-input uk-form-large" :conf="{enableTime: false,time_24hr:true}"
                               currentdate="{!! date('Y-m-d') !!}" inputname="date"></flatpickr>
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Start</div>
                    <flatpickr class="uk-input uk-form-large" :conf="{enableTime: true,time_24hr:true, noCalendar:true}"
                               currentdate="{!! date('H:s') !!}" inputname="start"></flatpickr>
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">End</div>
                    <flatpickr class="uk-input uk-form-large" :conf="{enableTime: true,time_24hr:true, noCalendar:true}"
                               currentdate="{!! date('H:s') !!}" inputname="end"></flatpickr>
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Venue</div>
                    <input type="text" class="uk-input uk-form-large" name="venue" value="" placeholder="Venue">
                </div>
                <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Title</div>
                    <editor inputname="title"></editor>
                </div>
                <button class="uk-button uk-button-success uk-float-right">Save</button>
            </form>
        </div>
    </div>
@endsection