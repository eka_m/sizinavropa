@extends('admin.layouts.base')
@section('title', '::Edit User')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small">
            <h3>Edit User</h3>
            <form action="{{route('users.update',$user->id)}}" method="post" class="uk-margin-auto uk-form-stacked">
                {{csrf_field()}}
                {{ method_field('PUT') }}
                <div class="uk-margin">
                    <label class="uk-form-label">Name</label>
                    <input type="text" class="uk-input  uk-form-large" name="name"
                           placeholder="Name" value="{{$user->name}}" required>
                    @if ($errors->has('name'))
                        <div class="uk-alert-danger" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="uk-margin">
                    <label class="uk-form-label">Email</label>
                    <input class="uk-input  uk-form-large" name="email" type="email"
                           placeholder="Email" value="{{$user->email}}" required>
                    @if ($errors->has('email'))
                        <div class="uk-alert-danger" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="uk-margin">
                    <label class="uk-form-label">Password</label>
                    <input class="uk-input  uk-form-large" name="password" type="password"
                           placeholder="Password" required>
                    @if ($errors->has('password'))
                        <div class="uk-alert-danger" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="uk-margin">
                    <label class="uk-form-label">Confirm Password</label>
                    <input class="uk-input  uk-form-large" name="password_confirmation"
                           type="password" placeholder="Password Confirmation" required>
                </div>
                <div class="uk-margin">
                    <label class="uk-form-label">Role</label>
                    <select name="role_id" class="uk-select">
                        @foreach($roles as $role)
                            <option value="{{$role->id}}" @if($user->id_role == $role->id) selected @endif >{{$role->name}}</option>
                        @endforeach
                    </select>
                </div>
                <button class="uk-button uk-button-primary uk-float-right">Save</button>
            </form>
        </div>
    </div>
@endsection
