@extends('admin.layouts.base')
@section('title', '::Slider')
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection
@section('content')
    <slider data="{{json_encode($slides)}}"></slider>
@endsection
