<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin Panel @yield('title')</title>
    {{--<link href="https://file.myfontastic.com/he5QifuJcf3PcdNYzuHAg3/icons.css" rel="stylesheet">--}}
    @section('css')
        <link rel="stylesheet" href="{{asset('/adminpanel/css/app.css')}}">
    @show
    {{--<script src="{{asset('plugins/jquery/jquery-3.2.1.min.js')}}" type="text/javascript"></script>--}}
</head>
<body>

<div id="app" class="wrapper uk-offcanvas-content">
    @include('admin.partials.main-nav')
    @if (session('error'))
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <div class="uk-alert-danger" uk-alert>
                <p>{{ session('error') }}</p>
            </div>
        </div>
    </div>
    @endif
    @section('content')
    @show
</div>
@section('js')
    <script src="{{asset('adminpanel/js/manifest.js')}}" type="text/javascript"></script>
    <script src="{{asset('adminpanel/js/vendor.js')}}" type="text/javascript"></script>
    <script src="{{asset('adminpanel/js/app.js')}}" type="text/javascript"></script>
    <script src="{{asset('adminpanel/js/custom.js')}}" type="text/javascript"></script>
@show
@include('admin.partials.notifications')
</body>
</html>