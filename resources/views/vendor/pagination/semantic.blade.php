@if ($paginator->hasPages())
    <div class="sa-pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a class="disabled item" href="javascript:void(0)"><i class="sa-angle-left"></i></a>
        @else
            <a class="item" href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="sa-angle-left"></i></a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <a href="javascript:void(0)" class="disabled item">
                    ...
                </a>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a href="javascript:void(0)" class="active item">{{ $page }}</a>
                    @else
                        <a class="item" href="{{ $url }}">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a class="item" href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="sa-angle-right"></i></a>
        @else
            <a class="disabled item" href="javascript:void(0)"><i class="sa-angle-right"></i></a>
        @endif
    </div>
@endif
