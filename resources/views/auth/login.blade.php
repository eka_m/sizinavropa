@extends('admin.layouts.login')

@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-flex uk-flex-center">
            <div class="uk-card uk-card-body uk-card-default uk-width-1-3@m uk-animation-scale-down ">
                <div class="uk-card-title">
                    <h2>Ecms</h2>
                </div>
                <form role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="uk-margin  uk-width-1-1">
                        <label class="uk-form-label uk-animation-slide-bottom-small">Email</label>
                        <div class="uk-form-controls">
                            <div class="uk-inline">
                                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: user"></span>
                                <input id="email" type="email"
                                       class="uk-input uk-form-width-large {{$errors->has('email') ? 'uk-form-danger' : ''}}"
                                       name="email" value="{{ old('email') }}" required
                                       autofocus>
                            </div>
                            @if ($errors->has('email'))
                                <span class="uk-text-danger">
                                        {{ $errors->first('email') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label class="uk-form-label uk-animation-slide-bottom-small">Password</label>
                        <div class="uk-form-controls">
                            <div class="uk-inline">
                                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                                <input id="password" type="password"
                                       class="uk-input uk-form-width-large {{$errors->has('password') ? 'uk-form-danger' : ''}}"
                                       name="password"
                                       required>
                            </div>
                            @if ($errors->has('password'))
                                <span class="uk-text-danger">
                                    {{ $errors->first('password') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="uk-margin">
                        <input type="checkbox" class="uk-checkbox"
                               name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="uk-form-label">Remember Me</label>
                    </div>
                    <div class="uk-margin">
                        <button type="submit" class="uk-button uk-button-primary uk-float-right uk-width-1-1">
                            Login
                        </button>
                    </div>
                    {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                    {{--Forgot Your Password?--}}
                    {{--</a>--}}
                </form>
            </div>
        </div>
    </div>
@endsection
