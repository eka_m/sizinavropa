<section class="row newsFeed light-bg mt-5 p-3">
    <div class="col row-content">
        <div class="row">
            <div class="col blockheader blockheader--whiteblock">
                <span class="blockheader__name"><span>{{__('words.interesting')}}</span></span>
            </div>
        </div>
        <div class="row newsFeed__row no-gutters">
            @foreach($news->slice(10, 9) as $article)
            <div class="col-xs-12 col-sm-6 col-md-4">
                <article class="newsFeed__row__item imgLiquid" onclick="location.href = '/{{$currentlocale}}/article/{{$article->url}}'">
                    <div class="newsFeed__row__item__image imgLiquid">
                        <img src="{{setImage($article->image, 'interesting')}}" class="fluid image" alt="">
                    </div>
                    <div class="newsFeed__row__item__overlay gr--{{$article->page->color}}"></div>
                    <header class="newsFeed__row__item__info">
                        <h3>{{$article->name}}</h3>
                        <div class="newsFeed__row__item__info__dateviews">
                            <span class="newsFeed__row__item__info__dateviews__date">{{$article->created_at}}</span>
                            <span>/</span>
                            <span class="newsFeed__row__item__info__dateviews__views"><i class="sa-eye"></i>
                                    {{$article->fakeviews ? $article->fakeviews : $article->views}}
                                </span>
                        </div>
                    </header>
                </article>
            </div>
            @endforeach
        </div>
        {{--
        <div class="row blockgo blockgo--white">
            <div class="col pt-0 mt-0">
                <a href="" class="blockgo__arrow blockgo__arrow--right">&rarr;</a>
            </div>
        </div> --}}
    </div>
</section>