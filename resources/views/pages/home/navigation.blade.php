<section class="row mainmenu gradient-bg">
  <div class="col pl-md-5 pr-md-5 row-content">
    <nav id="navigation" class="navigation">
      <div class="nav-header">
        {{--<a class="nav-brand" href="{{route('home')}}">--}}
        <a class="nav-logo" href="{{route('home')}}">
          <img src="{{asset('img/logo3.png')}}" alt="logo" style="height: 80px; margin: 5px 0; float: left">
        </a>
        <div class="nav-toggle"></div>
      </div>
      <div class="nav-search">
        <div class="nav-search-button">
          <i class="nav-search-icon sa-search"></i>
        </div>
        <form action="{{route('search')}}" method="get">
          <div class="nav-search-inner">
            <input type="search" name="query" placeholder="{{__('words.search')}}"/>
          </div>
        </form>
      </div>
      <div class="nav-menus-wrapper">
        <ul class="nav-menu nav-menu-social">
          <li><a href="https://www.facebook.com/SizinAvropa" target="_blank"><i class="sa-facebook"></i></a></li>
          <li><a href="https://www.youtube.com/channel/UCRRbADrlDJfBgJGPLvStCfA" target="_blank"><i
                      class="sa-youtube"></i></a></li>
        </ul>
        <ul class="nav-menu align-to-right">
          <li><a href="{{route('imagine')}}" class="bg--purple"
                 style="padding:10px 20px; border-radius:0; line-height:20px; display:inline-block; text-align:center;">IMAGINE</a>
          </li>
          <li><a href="{{route('fantazia')}}" class="bg--blue"
                 style="padding:10px 20px; border-radius:0; line-height:20px; display:inline-block; text-align:center;">FANTAZIA</a>
          </li>
          @foreach($toppages as $page)
            <li class="{{ request()->is('*/'.$page->url) ? 'active' : '' }}">
              <a href="/{{$currentlocale}}/page/{{$page->url}}" style="line-height:20px;">
                @if($currentlocale == env('DEFAULTLOCALE')){{$page->name}}@else {{$page->name_en}}@endif</a>
            </li>
          @endforeach
          <li>
            <a href="{{ LaravelLocalization::getLocalizedURL($locales->first()->slug, null, [], true) }}"
               class="up-text ">
              <i class="sa-world"></i>
              {{$locales->first()->slug}}
            </a>
            @if($locales->count() > 1)
              <ul class="nav-dropdown lang-dropdown">
                @foreach($locales->slice(1) as $locale)
                  <li>
                    <a href="{{ LaravelLocalization::getLocalizedURL($locale->slug, null, [], true) }}">{{$locale->slug}}</a>
                  </li>
                @endforeach
              </ul>
            @endif
          </li>
        </ul>
      </div>
    </nav>
  </div>
</section>