<section class="row categories light-bg hidden-sm">
    <div class="col pl-lg-5 pr-lg-5 row-content">
        <div class="ui fluid stackable five item massive text menu no-margin-bottom no-margin-top">
            @foreach($clearpages as $page)
                <a href="/{{$currentlocale}}/page/{{$page->url}}" class="item categories_link hv--{{$page->color}} br--{{$page->color}} {{ request()->is('*/'.$page->url) ? 'tx--white bg--'.$page->color  : '' }}">@if($currentlocale == env('DEFAULTLOCALE')){{$page->name}}@else {{$page->name_en}}@endif</a>
            @endforeach
        </div>
    </div>
</section>
