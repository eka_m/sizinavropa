<section class="row snowCollect mainslider light-bg mt-3">
    <div class="col p-lg-5 row-content">

        <div class="row no-gutters">
            <div class="col-sm-8 p-0">
                <div data-options="{
                        autoplay: 5000,
                        slidesPerView: 1,
                        mousewheelControl: true,
                        nextButton: '.events__button--next',
                        prevButton: '.events__button--prev',
                }" class="slickSld swiperTop">
                    @foreach($slides as $slide)
                            <article class="mainslider__item mainslider__item--big">
                                <a href="{{$slide->link}}">
                                    <div class="mainslider__item__image imgLiquid">
                                        <img class="ui fluid image" src="/uploads/{{$slide->image}}">
                                    </div>
                                    <header class="mainslider__item__overlay cl--red">
                                        <h3>{{$slide->name}}</h3>
                                        @if($slide->description)
                                            <p>{{$slide->description}}</p>
                                        @endif
                                    </header>
                                </a>
                            </article>
                    @endforeach
                </div>
            </div>
            <div class="col-sm-4 p-0" >
                <swiper class="slickSld" data-options="{
                        spaceBetween: 0,
                        slidesPerView: 2,
                        direction:'vertical',
                        slideToClickedSlide: true,
                        setWrapperSize:500,
                        autoHeight:false,
                        height:550,
              }" class="swiperThumbs" style="overflow: hidden;">
                    @foreach($slides as $slide)
                            <div class="row">
                                <div class="col">
                                    <article class="mainslider__item mainslider__item--small">
                                        <a href="{{$slide->link}}">
                                            <div class="mainslider__item__image imgLiquid">
                                                <img class="ui fluid image" src="/uploads/{{$slide->image}}">
                                            </div>
                                            <header class="mainslider__item__overlay mainslider__item--small--overlay cl--orange">
                                                <h3>{{$slide->name}}</h3>
                                                @if($slide->description)
                                                    <p>{{$slide->description}}</p>
                                                @endif
                                            </header>
                                        </a>
                                    </article>
                                </div>
                            </div>
                    @endforeach
                </swiper>
            </div>
        </div>
    </div>
</section>
