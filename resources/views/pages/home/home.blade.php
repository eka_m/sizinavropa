@extends('layouts.base')
@section('css')
    @parent
    <link rel="stylesheet" href="{{asset('plugins/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/plyr/plyr.css')}}">
@endsection
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/slick/slick.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/plyr/plyr.js')}}"></script>
@endsection
@section('content')
    @include('pages.home.slick')
    @include('pages.home.subscribes')
    @include('pages.home.mainnews')
    @include('pages.home.tv')
    @include('pages.home.interestingfeed')
@endsection