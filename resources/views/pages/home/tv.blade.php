<section class="row videoChannel gradient-bg mt-5 p-3">
    <div class="col row-content">
        <div class="row">
            <div class="col blockheader blockheader--gradientblock">
                    <span class="blockheader__name"><span>SİZİN AVROPA</span><div
                                class="tv"><span>TV</span></div></span>
            </div>
        </div>
        <div class="row videoChannel__row no-gutters">
            <div class="col-sm-12 snowCollect col-lg-5 order-2 order-lg-1">
                <div class="row videoChannel__row__items videoChannel__row__items--small no-gutters">
                    @foreach($videos as $video)
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-6">
                            <article class="videoChannel__row__items__item videoChannel__row__items__item--small smallTvVideo" data-video="{{json_encode($video)}}">
                                <div class="videoChannel__row__items__item--small__image imgLiquid">
                                    <img src="https://img.youtube.com/vi/{{$video->link}}/mqdefault.jpg" class="fluid image" alt="">
                                </div>
                                <header class="videoChannel__row__items__item__info videoChannel__row__items__item--small__info cl--{{$video->color}}" data-description="{{$video->description}}" data-color="{{$video->color}}">
                                    <h3>{{$video->name}}</h3>
                                </header>
                                <button class="videoChannel__row__items__item--small__playbutton"><i class="sa-play"></i>
                                </button>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-sm-12 col-lg-7 order-1 order-lg-2">
                <div class="row no-gutters">
                    <div class="col">
                        <article class="videoChannel__row__items__item videoChannel__row__items__item--big snowCollect" style="background-color:{{$videos->first()->color}};">
                            <div class="row no-gutters">
                                <div class="col">
                                    <div class="videoChannel__row__items__item--big__video">
                                        <div data-type="{{$videos->first()->type}}" data-video-id="{{$videos->first()->link}}" class="tvvideo"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row no-gutters">
                                <div class="col">
                                    <header class="videoChannel__row__items__item videoChannel__row__items__item__info videoChannel__row__items__item--big__info tvvideoinfo">
                                        <h3 class="mb-1">{{$videos->first()->name}}</h3>
                                        <p>{{$videos->first()->description}}</p>
                                    </header>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        <div class="row blockgo blockgo--gradient">
            <div class="col pt-0 mt-0">
                <a href="{{route('videos')}}" class="blockgo__arrow blockgo__arrow--right">&rarr;</a>
            </div>
        </div>
    </div>
</section>
