<section class="row lastNews light-bg mt-5 p-3 row-content">
    <div class="col">
        <div class="row">
            <div class="col blockheader blockheader--whiteblock">
                <span class="blockheader__name">{{__('words.lastnews')}}</span>
            </div>
        </div>
        <div class="row lastNews__row no-gutters">
            <div class="col">
                <div class="row lastNewsSlider">
                    @foreach($news->slice(0, 10) as $article)
                    <div class="col-3">
                        <article class="lastNews__row__item" onclick="location.href = '/{{$currentlocale}}/article/{{$article->url}}'">
                            <a class="link_reset" href="/{{$currentlocale}}/article/{{$article->url}}" title="{{$article->name}}">
                                <div class="lastNews__row__item__image slick-imgLiquid" data-imgLiquid-horizontalAlign="center" data-imgLiquid-verticalAlign="bottom">
                                    <img src="{{setImage($article->image, 'lastnews')}}" class="fluid image" alt="">
                                </div>
                            </a>
                            {{--
                            <div class="lastNews__row__item__overlay gr--{{$article->page->color}}"></div> --}}
                            <header class="lastNews__row__item__info gr--{{$article->page->color}}">
                                <a href="/{{$currentlocale}}/page/{{$article->page->url}}" class="lastNews__row__item__info__category d-inline-block cl--{{$article->page->color}}">
                                        @if($currentlocale == env('DEFAULTLOCALE')){{$article->page->name}}@else {{$article->page->name_en}}@endif
                                    </a>
                                <a class="link_reset" href="/{{$currentlocale}}/article/{{$article->url}}" title="{{$article->name}}">
                                    <h3>{{$article->name}}</h3>
                                    <p>{{str_limit($article->short,200)}}</p>
                                    <div class="lastNews__row__item__info__dateviews">
                                        <span class="lastNews__row__item__info__dateviews__date">{{$article->created_at}}</span>
                                        <span>/</span>
                                        <span class="lastNews__row__item__info__dateviews__views">
                                            <i class="sa-eye"></i> {{$article->fakeviews ? $article->fakeviews : $article->views}}
                                        </span>
                                    </div>
                                </a>
                            </header>
                        </article>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row blockgo blockgo--white">
            <div class="col pt-0 mt-0">
                <a href="javascript:void(0)" class="blockgo__arrow blockgo__arrow--left lastNewsLeft">&larr;</a>
                <a href="javascript:void(0)" class="blockgo__arrow blockgo__arrow--right lastNewsRight">&rarr;</a>
            </div>
        </div>
    </div>
</section>