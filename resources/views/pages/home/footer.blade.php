<footer class="row footer gradient-bg mt-5 p-3">
    <div class="col footer__col row-content">
        <div class="row footer__row">
            <div class="col-xs-12 col-md-4 about order-2 order-md-1">
                <div class="row footer__row__header justify-content-center">
                    <div class="col">
                        <h3>{{__('words.about')}}</h3>
                    </div>
                </div>
                <div class="row about__content pr-2 pb-3">
                    <div class="col" style="font-size: 1.2em;">
                        {!!__('words.short-about')!!}
                    </div>
                </div>
                <div class="row about__social">
                    <div class="col">
                        <a href="" class="about__social__link about__social__link--fb"><i class="sa-facebook"></i></a>
                        <a href="" class="about__social__link about__social__link--yt"><i class="sa-youtube"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 mostRead order-1 order-md-2">
                <div class="row footer__row__header">
                    <div class="col">
                        <h3>{{__('words.mostread')}}</h3>
                    </div>
                </div>
                <div class="row mostRead__items">
                    <div class="col">
                        @foreach($interesting->slice(0, 3) as $article)
                            <article class="row mostRead__items__item">
                                <div class="col-4">
                                    <img src="{{setImage($article->image, 'interesting')}}" class="fluid image" alt="">
                                </div>
                                <header class="col-8 mostRead__items__item__info">
                                    <a class="link_reset" href="/article/{{$article->url}}"><h3>{{$article->name}}</h3></a>
                                </header>
                            </article>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-3 offset-md-1 footerLinks order-3 order-md-3">
                <div class="row footer__row__header">
                    <div class="col">
                        <h3>{{__('words.links')}}</h3>
                    </div>
                </div>
                <div class="row footerLinks__row">
                    <div class="col footerLinks__row__links">
                        @foreach($clearpages as $page)
                                <a class="footerLinks__row__links__link" href="/{{$currentlocale}}/page/{{$page->url}}">
                                    @if($currentlocale == env('DEFAULTLOCALE')){{$page->name}}@else {{$page->name_en}}@endif
                                </a>
                        @endforeach
                        @foreach($toppages as $toppage)
                                <a class="footerLinks__row__links__link" href="/{{$currentlocale}}/page/{{$toppage->url}}">
                                    @if($currentlocale == env('DEFAULTLOCALE')){{$toppage->name}}@else {{$toppage->name_en}}@endif
                                </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="row footer__row">
            <div class="col tx-center">
                <hr class="cl--white snowCollect">
                <span class="d-block">{{__('words.copyright')}}</span>
                <span class="d-block"> 2017 © SizinAvropa.az</span>
            </div>
        </div>
    </div>
</footer>
