@extends('layouts.imagine')
@section('title', 'Masterclasss IMAGINE')
@section('content')
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
        @include('pages.imagine.nav',['active' => 'masterclasses'])
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
               @include('pages.imagine.cover')
            </div>
        </div>
        <div class="uk-container uk-box-shadow-small bg-white uk-margin-auto uk-padding-small">
            <div class="page-title uk-text-center">
                <span class="imagineMasterclass__date uk-margin-remove-bottom uk-h3">{{date("d F  H:i ", strtotime($masterclass->date))}}</span>
                <span class="imagineMasterclass__venue uk-margin-remove-bottom uk-h3">{{$masterclass->venue}}</span>
                <h1 class="uk-h1 uk-margin-remove-top ">{{$masterclass->name}}</h1>
            </div>
            <div class="page-content uk-padding-small">
                <img src="/uploads{{$masterclass->image}}" style="float: right; width: 350px; margin-left: 10px;" alt="{{$masterclass->name}}">
                {!! $masterclass->content !!}
            </div>
        </div>
    </div>
@endsection