@extends('layouts.imagine') 
@section('title', 'European Year of Cultural Heritage') 
@section('css') @parent()
<style>
  .about-text .list p {
    font-size: 16px;
    margin: 2px;
    font-family: 'Exo 2', sans-serif;
  }

  .about-text .list {
    margin-bottom: 20px;
  }

  .about-text .list p .tire {
    font-weight: bold;
    display: inline-block;
    margin-right: 10px;
  }

  .about-text h5 {
    margin: 5px;
    font-family: 'Exo 2', sans-serif;
  }
  p {
    text-align: justify;
	}
	.after-title {
		font-size: 0.6em;
	}
</style>
@endsection
 
@section('content')
<div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
  @include('pages.imagine.nav',['active' => 'european-heritage'])
  <div class="uk-container-large uk-padding-remove">
    <div class="main-image">
  @include('pages.imagine.cover')
    </div>
  </div>
  <div class="uk-container-small bg-white uk-margin-auto uk-padding-small">

    <div class="page-title uk-text-center">
      <h1 class="uk-heading-line uk-h1"><span>European Year of Cultural Heritage
			</span>
			<span class="after-title">Our heritage: where the past meets the future</span>
		</h1>
    </div>
    <p>
			<img src="{{asset('img/fantazia/baku-heritage/european-heritage.png')}}" style="float:right; margin:0 0 20px 20px; max-width:250px;">
			During the year <strong>2018 </strong>, the European Union is celebrating cultural heritage across all of Europe. This year is the first year for the European Year of Cultural Heritage. The aim of the European Year of Cultural Heritage is to “<strong>encourage </strong> more people to <strong>discover</strong> and <strong>engage</strong> with Europe’s cultural heritage” and to “<strong>reinforce</strong> a sense of belonging to a <strong>common European space</strong>.” 
    </p>
    <p>
				To celebrate this year, numerous events and initiatives will be organized throughout Europe, so that individuals can engage in traditions learned from their ancestors. The European Union believes that cultural heritage is of universal value at an individual level and a societal level.  Likewise, in order to build Europe’s future, specifically young individuals need to be reached out to.
    </p>
    <p>
				Cultural heritage comes in a <strong>range of forms</strong>.
				<strong>Tangible</strong> cultural heritage, which includes buildings, monuments, artefacts, clothing, artwork, books, machines, historic towns, archaeological sites and more. </br>
				<strong>Intangible</s cultural heritage includes practices, representations, expressions, knowledge, skills - and the associated instruments and cultural spaces (language and oral traditions, performing arts, social practices and traditional craftsmanship). <strong>Natural</strong> cultural heritage includes landscapes, flora and fauna. </br>
				<strong>Digital</strong> cultural heritage includes resources that were created in digital form (for example digital art or animation) or that have been digitalised as a way to preserve them (including text, images, video, records).
    </p>
    <p>
				<strong>So, what is actually happening in 2018?</strong>
				This year will allow everyone to join thousands of activities that take place in Europe. Each Member State of the European Union has appointed a <strong>National Coordinator</strong> to organize the year and coordinate events at both local and national levels. 
    </p>
    <p>
				All EU institutions are dedicated to fulfilling this year with meaningful. This includes the <strong>European Commission</strong>, the <strong>European Parliament</strong>, the <strong>Council of the European Union</strong>, the <strong>Committee of the Regions</strong> and the <strong>European Economic and Social Committee</strong>, all of which will organize and launch the events. The EU will also be funding projects that are supportive of cultural heritage.
    </p>
    <p>
				For a closer look, the Commission (working with the <strong>Council of Europe, UNESCO</strong> and more) has set out <strong>10 long-term impact projects</strong>. These projects will be based on engagement, sustainability, protection and innovation. More specifically, these will include school activities, research on innovative solutions for the reuse of heritage buildings and the fight against illicit trafficking of cultural goods. This will hopefully leave a positive impact that will benefit citizens in a long-term scale. 
    </p>
    <p>
      </br>
    </p>
    <p>
      </br>
    </p>
  </div>
</div>
@endsection