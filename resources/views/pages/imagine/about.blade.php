@extends('layouts.imagine') 
@section('title', 'About Fantazia') 
@section('css') @parent()
<style>
  .about-text .list p {
    font-size: 16px;
    margin: 2px;
    font-family: 'Exo 2', sans-serif;
  }

  .about-text .list {
    margin-bottom: 20px;
  }

  .about-text .list p .tire {
    font-weight: bold;
    display: inline-block;
    margin-right: 10px;
  }

  .about-text h5 {
    margin: 5px;
    font-family: 'Exo 2', sans-serif;
  }
  p {
    text-align: justify;
  }
</style>
@endsection
 
@section('content')
<div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
  @include('pages.imagine.nav',['active' => 'about'])
  <div class="uk-container-large uk-padding-remove">
    <div class="main-image">
  @include('pages.imagine.cover')
    </div>
  </div>
  <div class="uk-container-small bg-white uk-margin-auto uk-padding-small">

    <div class="page-title uk-text-center">
      <h1 class="uk-heading-line uk-h1"><span>European Union to conduct Baku Cultural Heritage Festival</span></h1>
    </div>


    <p>
      <strong>
      On the occasion of the European Year of Cultural Heritage, the Delegation of the European Union to Azerbaijan in cooperation
      with the Embassies of EU Member States and in particular Germany, France, the UK, Greece, Poland, Croatia, the Netherlands,
      Hungary will organize the First "Fantazia" Festival aimed at raising awareness about the importance of preserving cultural
      heritage in Baku and Azerbaijan. The Embassies of Mexico, Moldova and Morocco are also participating. United Cultures
      will provide organizational support under a contract financed by the European Union.
    </strong>
    </p>
    <p>
      Fantazia Festival will be held in Baku from 1 to 10 November 2018 and will present a diverse calendar of events, including
      concerts, guided tours, workshops, film screenings, exhibitions, art installations, competitions and debates with internationally
      and locally acclaimed experts.
    </p>
    <p>
      During the Festival, the emphasis will be on the area of the Icheri Sheher, former “Sovetskaya” area and the neighbourhood
      around “Fantazia” hamam, the latter was taken also as a name for the Festival. The famous “Fantazia” hamam was built
      in the end of the 19th century and brought together European architecture with local specific elements.
    </p>
    <p>
      The Festival is supported by “The Landmark”. Official partners of the initiative are: State Tourism Agency, Union of Architects,
      Administration of the State Historical-Architectural Reservation “Icheri Sheher”, Azerbaijan Carpet Museum, “Maksud
      Ibrahimbeyov” Creative Centre, “Yarat” Contemporary Art Center. Scientific partners are: Ca’ Foscari University of
      Venice, ADA University, Azerbaijan National University of Culture and Arts, Azerbaijan University of Architecture and
      Construction.
    </p>
    <p>
      </br>
    </p>
    <p>
      </br>
    </p>
  </div>
</div>
@endsection