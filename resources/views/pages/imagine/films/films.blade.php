@extends('layouts.imagine')
@section('title', 'Imagine Films')
{{--@section('css')--}}
    {{--@parent--}}
    {{--<link rel="stylesheet" href="https://cdn.plyr.io/2.0.13/plyr.css">--}}
{{--@endsection--}}
{{--@section('js')--}}
    {{--@parent--}}
    {{--<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>--}}
    {{--<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>--}}
    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--$('#films').imagesLoaded( function() {--}}
                {{--$('.grid').masonry({--}}
                    {{--// options--}}
                    {{--itemSelector: '.grid-item',--}}
                {{--});--}}
            {{--});--}}

        {{--});--}}
    {{--</script>--}}
{{--@endsection--}}
@section('content')
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
        @include('pages.imagine.nav',['active' => 'movies'])
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
                @include('pages.imagine.cover')
            </div>
        </div>
        <div class="uk-container bg-white uk-margin-auto uk-padding-small">
            <div class="page-title uk-text-center">
                <h1 class="uk-heading-line uk-h2"><span>Movies</span></h1>
            </div>
            <div class="page-content">
                <div id="films" class="films uk-flex uk-child-width-1-4@l uk-child-width-1-3@m  uk-child-width-1-2@s uk-child-width-1-1@xs grid"
                     uk-grid-parallax uk-scrollspy="target: > div > .uk-card; cls:uk-animation-fade; delay: 500">
                    @foreach($films as $film)
                        <div class="grid-item">
                            <div class="film-item uk-card-hover uk-card uk-card-default"
                                 onclick="window.open('{{route('imagine.film',$film->slug)}}','_self')">
                                <a href="{{route('imagine.film',$film->slug)}}">{{$film->title}}</a>
                                <span class="uk-display-block">
                                    {{$film->date ? date("d F g:i a", strtotime($film->date)) : ''}}
                                </span>
                                @if($film->poster !== null)
                                    <img src="{{asset('uploads'.$film->poster)}}" alt="">
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection