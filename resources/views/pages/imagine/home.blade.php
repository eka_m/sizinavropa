@extends('layouts.imagine')
@section('title', 'Fantazia Festival')
@section('css')
 @parent()
<style>
.main-menu-item {
    background-repeat: no-repeat;
    background-position: center center;
}
.main-menu-item--1:hover {
    background-image: url('img/fantazia/1.jpg');
}
.main-menu-item--2:hover {
    background-image: url('img/fantazia/2.jpg');
}
.main-menu-item--3:hover {
    background-image: url('img/fantazia/3.jpg');
}
.main-menu-item--4:hover {
    background-image: url('img/fantazia/4.jpg');
}
.main-menu-item--6:hover {
    background-image: url('img/fantazia/baku-heritage.jpg');
}
.main-menu-item--7:hover {
    background-image: url('img/fantazia/european-heritage.jpg');
}
.main-menu-item--8:hover {
    background-image: url('img/fantazia/partners.jpg');
}
</style>
@endsection
@section('content')
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" >
        <div class="uk-container-large uk-padding-remove">
            <div class="main-header" uk-grid>
                <div class="burger">
                    <a href="#menu" class="uk-link-reset" uk-scroll>&#9776;</a>
                </div>
                <div class="sa-logo uk-margin-auto" >
                    <a href="{{route('home')}}"><img src="{{asset('img/logo-yellow.png')}}"></a>
                </div>
                {{--<div class="date uk-visible@s">--}}
                    {{--<h1>1-10 NOVEMBER 2018</h1>--}}
                {{--</div>--}}
                <div class="eu-logo uk-margin-auto uk-width-expand">
                    <img src="{{asset('img/imagine/eu.png')}}" alt="">
                </div>
                {{--<div class="date uk-hidden@s">--}}
                    {{--<h1>1-10 NOVEMBER 2018</h1>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
               @include('pages.imagine.cover')
            </div>
        </div>
        <div class="uk-container-large bg-white uk-margin-auto uk-padding-large" id="menu">
            <div class="uk-child-width-1-3@m uk-text-center main-menu" uk-grid
                 uk-scrollspy="target: > div > .main-menu-item; cls:uk-animation-scale-down; delay: 500">
                <div>
                    <div class="main-menu-item main-menu-item--1 uk-card uk-car-body uk-card-default uk-card-hover" >
                        <div class="main-menu-item__1">
                            {{--<a href="{{route('imagine.page','about')}}">ABOUT</a>--}}
                            <a href="javascript:;">ABOUT</a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="main-menu-item main-menu-item--2 uk-card uk-car-body uk-card-default uk-card-hover">
                        <div class="main-menu-item__2">
                            {{--<a href="{{url('/imagine/program?page=2')}}">Program</a>--}}
                            <a href="javascript:;">Program</a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="main-menu-item main-menu-item--3 uk-card uk-car-body uk-card-default uk-card-hover">
                        <div class="main-menu-item__3">
                            {{--<a href="{{route('imagine.films')}}">Movies</a>--}}
                            <a href="javascript:;">Movies</a>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2@m">
                    <div class="main-menu-item main-menu-item--4 uk-card uk-car-body uk-card-default uk-card-hover">
                        <div class="main-menu-item__4">
                            {{--<a href="{{route('imagine.events')}}">Events</a>--}}
                            <a href="javascript:;">Events</a>
                        </div>
                    </div>
                </div>
                {{--<div>--}}
                    {{--<div class="main-menu-item main-menu-item--5 uk-card uk-car-body uk-card-default uk-card-hover">--}}
                        {{--<div class="main-menu-item__5">--}}
                            {{--<a href="{{route('imagine.masterclasses')}}">Master classes</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="uk-width-1-2@m">
                    <div class="main-menu-item main-menu-item--8 uk-card uk-car-body uk-card-default uk-card-hover">
                        <div class="main-menu-item__8">
                            {{--<a href="{{route('imagine.page','partners')}}">Partners</a>--}}
                            <a href="javascript:;">Partners</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection