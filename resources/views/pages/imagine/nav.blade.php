<div class="uk-container-large uk-padding-remove">
    <div class="main-nav">
        <div class="topnav" id="myTopnav">
            <a href="{{route('imagine')}}">Home</a>
            <a href="{{route('imagine.page','about')}}" @if ($active == 'about') class="active" @endif>About</a>
            <a href="{{route('imagine.program')}}" @if ($active == 'programs') class="active" @endif>Program</a>
            <a href="{{route('imagine.films')}}" @if ($active == 'movies') class="active" @endif>Movies</a>
            <a href="{{route('imagine.events')}}" @if ($active == 'events') class="active" @endif>Events</a>
            <a href="{{route('imagine.bakuheritage')}}" @if ($active == 'baku-heritage') class="active" @endif>Baku heritage</a>
            <a href="{{route('imagine.europeanheritage')}}" @if ($active == 'european-heritage') class="active" @endif>European year</a>
            {{--<a href="{{route('imagine.masterclasses')}}" @if ($active == 'masterclasses') class="active" @endif>Master classes</a>--}}
            <a href="{{route('imagine.page','partners')}}" @if ($active == 'partners') class="active" @endif>Partners</a>
            <a href="javascript:void(0);" style="font-size:32px;" class="icon" onclick="myFunction()">&#9776;</a>
        </div>
    </div>
</div>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
</script>
