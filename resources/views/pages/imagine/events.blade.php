@extends('layouts.imagine')
@section('title', 'Imagine Events')

@section('content')
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
        @include('pages.imagine.nav',['active' => 'events'])
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
                <a href="{{route('imagine')}}"><img src="{{asset('img/imagine/imaginestaticred.jpg')}}" alt=""></a>
            </div>
        </div>
        <div class="uk-container-small bg-white uk-margin-auto uk-padding-small" >
            <div class="page-title uk-text-center">
                <h1 class="uk-heading-line uk-h1"><span>Events</span></h1>
            </div>
            <div class="page-content">
                <p class="uk-h1 uk-text-center">Coming soon</p>
            </div>
        </div>
    </div>
@endsection