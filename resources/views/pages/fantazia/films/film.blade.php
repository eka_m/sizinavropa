@extends('layouts.imagine')
@section('title', 'Fantazia  - '.$film->title)
@section('css')
    @parent
    <link rel="stylesheet" href="https://cdn.plyr.io/2.0.13/plyr.css">
@endsection
@section('js')
    @parent
    <script src="https://cdn.plyr.io/2.0.13/plyr.js"></script>
    <script>
        $(document).ready(function () {
            plyr.setup(document.querySelectorAll('.player'));
        });
    </script>
@endsection
@section('content')
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
        @include('pages.fantazia.nav',['active' => 'movies'])
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
                @include('pages.fantazia.cover')
            </div>
        </div>
        <div class="uk-container bg-white uk-margin-auto uk-padding-small">
            <div class="page-title uk-text-center">
                <h1 class="uk-h2 film-heading"><span>{{$film->title}}</span></h1>
            </div>
            <div class="page-content">
                <div class="film uk-flex uk-flex-center " uk-grid>
                    <div class="film__poster uk-width-2-5@m uk-width-1-1@s">
                        @if($film->poster !== null)
                            <img src="{{asset('uploads'.$film->poster)}}" class="uk-float-right"
                                 width="350px" alt="{{$film->title}}">
                        @else
                            <div class="film__info__trailer">
                                <p>Trailer</p>
                                <div class="player" data-type="youtube" data-video-id="{{$film->trailer}}"></div>
                            </div>
                        @endif
                    </div>
                    <div class="uk-padding uk-padding-remove-top film__info uk-width-3-5@m uk-width-1-1@s">
                        <p><span>Title:</span> {{$film->title}}</p>
                        <p><span>Original Title:</span> {{$film->org_title}}</p>
                        <p><span>Year:</span> {{$film->year}}</p>
                        <p><span>Country:</span> {{$film->country}}</p>
                        <p><span>Length:</span> {{$film->length}}</p>
                        <p><span>Genre:</span> {{$film->genre}}</p>
                        <p><span>Movie Director:</span> {{$film->director}}</p>
                        <p><span>Writers:</span> {{$film->writers}}</p>
                        <p><span>Cast:</span> {{$film->cast}}</p>
                        <p><span>Synopsis:</span> {{$film->synopsis}}</p>
                        <p><span>Date:</span>{{$film->date ? date("d F", strtotime($film->date)) : ''}}</p>
                        <p><span>Time:</span>{{$film->date ? date("g:i a", strtotime($film->date)) : ''}}</p>
                        <p><span>Venue:</span> {{$film->venue}}</p>
                        <p><span>Language:</span> {{$film->lang}}</p>
                        <p><span>Subtitle:</span> {{$film->subtitle}}</p>
                        @if($film->poster !== null)
                            <div class="film__info__trailer">
                                <p>Trailer</p>
                                <div class="player" data-type="{{is_numeric($film->trailer) ? 'vimeo' : 'youtube'}}" data-video-id="{{$film->trailer}}"></div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection