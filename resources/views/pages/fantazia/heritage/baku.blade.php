@extends('layouts.imagine')
@section('title', 'Baku heritage')
@section('css') @parent()
<style>
  .about-text .list p {
    font-size: 16px;
    margin: 2px;
    font-family: 'Exo 2', sans-serif;
  }

  .about-text .list {
    margin-bottom: 20px;
  }

  .about-text .list p .tire {
    font-weight: bold;
    display: inline-block;
    margin-right: 10px;
  }

  .about-text h5 {
    margin: 5px;
    font-family: 'Exo 2', sans-serif;
  }
  p {
    text-align: justify;
  }
</style>
<link rel='stylesheet' href='{{asset('/plugins/unitegallery/css/unite-gallery.css ')}}' type='text/css' />
<link rel='stylesheet' href='{{asset('/plugins/unitegallery/themes/default/ug-theme-default.css ')}}' type='text/css' /> 
@endsection
@section('js')
@parent
 <script type='text/javascript' src="{{asset('/plugins/unitegallery/js/unitegallery.min.js ')}}"></script>
 <script type='text/javascript' src='{{asset("/plugins/unitegallery/themes/slider/ug-theme-slider.js")}}'></script>
 <script type="text/javascript">
		 $("#gallery-baku-heritage").unitegallery({
												 gallery_theme:"slider",
												 tile_enable_textpanel: false,
												 lightbox_type: "compact",
												 slider_control_zoom:false
												 });
 </script>
 @endsection
@section('content')
<div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
  @include('pages.fantazia.nav',['active' => 'baku-heritage'])
  <div class="uk-container-large uk-padding-remove">
    <div class="main-image">
  @include('pages.fantazia.cover')
    </div>
  </div>
  <div class="uk-container-small bg-white uk-margin-auto uk-padding-small">

    <div class="page-title uk-text-center">
      <img src="{{asset('img/fantazia/baku-heritage/main.png')}}">
    </div>


    <p>
			<strong>Baku</strong> is the capital and largest city of Azerbaijan, as well as the largest city on the Caspian Sea and of the Caucasus region, with a population of 2,262,600 (January 1, 2018). Baku is located 28 metres  below sea level, which makes it the lowest lying national capital in the world and also the largest city in the world located below sea level. It is located on the southern shore of the Absheron Peninsula, alongside the Bay of Baku. 
    </p>
    <p>
      The Inner City of Baku, along with the Shirvanshah's Palace and Maiden Tower, were inscribed as a UNESCO World Heritage Site in 2000. According to the Lonely Planet's ranking, Baku is also among the world's top ten destinations for urban nightlife.
    </p>
    <p>
			The city is the scientific, cultural, and industrial center of Azerbaijan. Many sizeable Azerbaijani institutions have their headquarters there. The city is renowned for its harsh winds, which is reflected in its nickname, the "City of Winds".
    </p>
    <p>
      Drilling for oil began in the mid-1800s, with the first oil well drilled in the Bibi-Heybat suburb of Baku in 1846. It was mechanically drilled, though a number of hand-dug wells predate it. Large-scale oil exploration started in 1872 when Russian imperial authorities auctioned the parcels of oil-rich land around Baku to private investors. The pioneer of oil extracting from the bottom of the sea was Polish geologist Witold Zglenicki. Soon after that Swiss, British, French, Belgian, German, Swedish and American investors appeared in Baku. Among them were the firms of the Nobel brothers together with the family von Börtzell-Szuch (Carl Knut Börtzell, who also owned the Livadia Palace) and the Rothschild family. An industrial oil belt, better known as Black City, was established near Baku.
    </p>
    <div class="uk-text-center">
      <img src="{{asset('img/fantazia/baku-heritage/bh1.png')}}" class="" alt="">
    </div>
		<p>
			By the beginning of the 20th century, half of the oil sold in international markets was being extracted in Baku. The oil boom contributed to the massive growth of Baku. Between 1856 and 1910 Baku's population grew at a faster rate than that of London, Paris or New York.
		</p>
		<p>
			The city has many amenities that offer a wide range of cultural activities, drawing both from a rich local dramatic portfolio and an international repertoire. It also boasts many museums such as Baku Museum of Modern Art and Azerbaijan State Museum of History, most notably featuring historical artifacts and art. Many of the city's cultural sites.
		</p>
		<p>
			The city has many amenities that offer a wide range of cultural activities, drawing both from a rich local dramatic portfolio and an international repertoire. It also boasts many museums such as Baku Museum of Modern Art and Azerbaijan State Museum of History, most notably featuring historical artifacts and art. Many of the city's cultural sites.
    </p>
    <p>Baku has wildly varying architecture, ranging from the Old City core to modern buildings and the spacious layout of the Baku port. Many of the city's most impressive buildings were built during the early 20th century, when architectural elements of the European styles were combined in eclectic style. Baku has an original and unique appearance, earning it a reputation as the 'Paris of the East'</p>
    <div class="uk-text-center">
      <img src="{{asset('img/fantazia/baku-heritage/bh2.png')}}" class="" alt="">
    </div>
		<p>
			Due to the oil boom in the 19th century, Baku became a rapidly developing city and grew rapidly. The large-scale construction of the city was directly tied to the increase of the city's population. Eventually, this brought numerous German (Adolf Eichler and Nicolaus von der Nonne), Polish (Józef Gosławski and Józef Plośko) and Russian architects to the city, who ultimately influenced the city's architectural profile. Much of these architects were educated in Russia and, in particular, in St. Petersburg, Russia's capital city of the time. These included a number of high-profile designers, such as Zivar bey Ahmadbeyov, Nikolai Bayev, Mammad Hasan Hajinski. 
		</p>
		<div id="gallery-baku-heritage" class="unite-gallery" style="display:none; margin: 0 auto">
			@foreach($images as $item)
					<img src="/uploads/{{$item}}" data-image="/uploads/{{$item}}">
			@endforeach
	</div>
    <p>
      </br>
    </p>
    <p>
      </br>
    </p>
  </div>
</div>
@endsection