@extends('layouts.imagine')
@section('title', 'Masterclasses Fantazia')
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/imageliquid/imgliquid.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".imgLiquidFill").imgLiquid();
        });
    </script>
@endsection
@section('content')
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
        @include('pages.fantazia.nav',['active' => 'masterclasses'])
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
                <a href="{{route('fantazia')}}"><img src="{{asset('img/imagine/imagine-new.png')}}" alt=""></a>
            </div>
        </div>
        <div class="uk-container-large bg-white uk-margin-auto uk-padding-small">
            <div class="page-title uk-text-center">
                <h1 class="uk-heading-line uk-h1"><span>Masterclasses</span></h1>
            </div>
            <div class="page-content">
                <div class="imagineMasterclasses uk-grid uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid>
                    @foreach($masterclasses as $masterclass)
                        <div>
                            <article class="imagineMasterclasses__masterclass imgLiquidFill imgLiquid uk-card uk-card-default" onclick="window.open('{{route('fantazia.masterclass',$masterclass->slug)}}','_self')">
                                <header class="uk-card-media-top">
                                    <div class="imagineMasterclasses__masterclass__info">
                                        <span class="imagineMasterclasses__masterclass__info__date">
                                            {{date("d F / H:i", strtotime($masterclass->date))}}
                                        </span>
                                        <br>
                                       <span class="imagineMasterclasses__masterclass__info__venue">
                                           {{$masterclass->venue}}
                                       </span>
                                    </div>
                                    <img src="/uploads/{{$masterclass->image}}" alt="{{$masterclass->name}}">
                                </header>
                                <a href="{{route('fantazia.masterclass',$masterclass->slug)}}" class="imagineMasterclasses__masterclass__title">
                                    <span>
                                        {{$masterclass->name}}
                                    </span>
                                </a>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection