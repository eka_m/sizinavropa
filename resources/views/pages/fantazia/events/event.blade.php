@extends('layouts.imagine')
@section('title', 'Fantazia - '.$event->name)
@section('content')
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
        @include('pages.fantazia.nav',['active' => 'events'])
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
               @include('pages.fantazia.cover')
            </div>
        </div>
        <div class="uk-container uk-box-shadow-small bg-white uk-margin-auto uk-padding-small">
            <div class="page-title uk-text-center">
                <span class="imagineEvent__date uk-margin-remove-bottom uk-h3">{{date("d F  H:i ", strtotime($event->date))}}</span>
                <span class="imagineEvent__venue uk-margin-remove-bottom uk-h3">{{$event->venue}}</span>
                <h1 class="uk-h1 uk-margin-remove-top ">{{$event->name}}</h1>
            </div>
            <div class="page-content uk-padding-small">
                <img src="/uploads{{$event->image}}" style="float: right; width: 350px; margin-left: 10px;" alt="{{$event->name}}">
                {!! $event->content !!}
            </div>
        </div>
    </div>
@endsection