@extends('layouts.imagine')
@section('title', 'Fantazia Events')
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/imageliquid/imgliquid.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".imgLiquidFill").imgLiquid();
        });
    </script>
@endsection
@section('content')
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
        @include('pages.fantazia.nav',['active' => 'events'])
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
               @include('pages.fantazia.cover')
            </div>
        </div>
        <div class="uk-container-large bg-white uk-margin-auto uk-padding-small">
            <div class="page-title uk-text-center">
                <h1 class="uk-heading-line uk-h1"><span>Events</span></h1>
            </div>
            <div class="page-content">
                <div class="imagineEvents uk-grid uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid>
                    @foreach($events as $event)
                        <div>
                            <article class="imagineEvents__event imgLiquidFill imgLiquid uk-card uk-card-default" onclick="window.open('{{route('fantazia.event',$event->slug)}}','_self')">
                                <header class="uk-card-media-top">
                                    <div class="imagineEvents__event__info">
                                        <span class="imagineEvents__event__info__date">
                                            {{date("d F / H:i", strtotime($event->date))}}
                                        </span>
                                        <br>
                                       <span class="imagineEvents__event__info__venue">
                                           {{$event->venue}}
                                       </span>
                                    </div>
                                    <img src="/uploads/{{$event->image}}" alt="{{$event->name}}">
                                </header>
                                <a href="{{route('fantazia.event',$event->slug)}}" class="imagineEvents__event__title">
                                    <span>
                                        {{$event->name}}
                                    </span>
                                </a>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection