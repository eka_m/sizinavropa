@extends('layouts.imagine')
@section('title', 'Fantazia Programs')
@section('css')
@parent
<style>
.pr-prev, .pr-next {
    text-decoration: none;
    background: #18c6ff;
    transition: all .5s;
    
}
.pr-prev:hover, .pr-next:hover {
    background: #BE30A0;
} 
.pr-prev:hover{
    border-radius: 0 50px 50px 50px;
}
.pr-next {
    float:right;
}
.pr-next:hover{
    border-radius: 50px 0 50px 50px;
}
</style>

@endsection
@section('content')
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
        @include('pages.fantazia.nav',['active' => 'programs'])
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
               @include('pages.fantazia.cover')
            </div>
        </div>
        <div class="uk-container-large bg-white uk-margin-auto uk-padding-small">
            {{--  <div class="page-title uk-text-center">
                <h1 class="uk-heading-line uk-h1"><span>Program</span></h1>
                <a href="{{asset('img/imagine/Imagine2018Flyer.png')}}" class="pr-download" download>Download</a>
            </div>  --}}
            <div class="uk-container bg-white uk-margin-auto uk-padding-small uk-clearfix">
                @if(\Request::get('page') < 2)
                <a href="/fantazia/program?page={{\Request::get('page') ? \Request::get('page')  + 1 : 2}}" class="pr-next uk-button uk-button-primary uk-button-small">Next page <span>&rarr;</span></a>
                @endif
                @if(\Request::get('page') > 1)
                <a href="/fantazia/program?page={{\Request::get('page') ? \Request::get('page') - 1 : 1}}" class="pr-prev uk-button uk-button-primary uk-button-small"><span>&larr;</span> Previous page</a>
                @endif
            </div>
            <div class="page-content-program">
                <div class="program-block uk-child-width-expand@l uk-child-width-1-6@m uk-child-width-1-5@s grid uk-grid-small"
                     uk-grid uk-scrollspy="target: > div > .uk-card; cls:uk-animation-fade; delay: 500">
                    @foreach($program as $key => $group)
                        <div class="grid-item">
                            <div class="uk-card ">
                                <div class="programItem">
                                    <p class="pr-head">{{date("l", strtotime($key))}} {{date("d.m.", strtotime($key))}}</p>
                                    @foreach($group as $k => $value)
                                        <p class="programPlace">{{$k}}</p>
                                        <ul class="program uk-list ">
                                            @foreach($value->sortBy('start') as $item)
                                                <li>
                                                    @if($item->start && $item->end)
                                                        <span class="time"><span>{{date("H:i", strtotime($item->start)).'-'}}{{date("H:i", strtotime($item->end))}}</span></span>
                                                    @endif
                                                    <div class="pr-title">{!! $item->title !!}</div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection