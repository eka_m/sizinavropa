@extends('layouts.imagine')
@section('title', 'Fantazia Partners')

@section('content')
    <div class="uk-section uk-section-large uk-padding-remove-top uk-padding-remove-bottom" id="page">
        @include('pages.fantazia.nav',['active' => 'partners'])
        <div class="uk-container-large uk-padding-remove">
            <div class="main-image">
               @include('pages.fantazia.cover')
            </div>
        </div>
        <div class="uk-container uk-container-small bg-white uk-margin-auto uk-padding-small">
            <div class="page-title uk-text-center">
                <h1 class="uk-heading-line uk-h1"><span>Partners</span></h1>
            </div>
            <div class="page-content">
               {{-- <div class="partners uk-child-width-1-3@m uk-child-width-1-2@s" uk-grid uk-scrollspy="target: > div > .uk-card; cls:uk-animation-fade; delay: 500">
                   <div>
                       <div class="uk-card uk-card-hover">
                           <img src="{{asset('uploads/images/imagine/partners/imagine2018/eu.jpg')}}" alt="">
                       </div>
                   </div>
                   <div>
                       <div class="uk-card uk-card-hover">
                           <img src="{{asset('uploads/images/imagine/partners/imagine2018/uc.jpg')}}" alt="">
                       </div>
                   </div>
                   <div>
                       <div class="uk-card uk-card-hover">
                           <img src="{{asset('uploads/images/imagine/partners/imagine2018/lm.jpg')}}" alt="">
                       </div>
                   </div>
                   <div>
                       <div class="uk-card uk-card-hover">
                           <img src="{{asset('uploads/images/imagine/partners/park_cinema_logo.jpg')}}" alt="">
                       </div>
                   </div>
                   <div>
                       <div class="uk-card uk-card-hover">
                           <img src="{{asset('uploads/images/imagine/partners/imagine2018/ami.jpg')}}" alt="">
                       </div>
                   </div>
                   <div>
                       <div class="uk-card uk-card-hover">
                           <img src="{{asset('uploads/images/imagine/partners/imagine2018/cp.jpg')}}" alt="">
                       </div>
                   </div>
                   <div>
                       <div class="uk-card uk-card-hover">
                            <img src="{{asset('uploads/images/imagine/partners/imagine2018/trend.jpg')}}" alt="">
                       </div>
                   </div>
                   <div>
                       <div class="uk-card uk-card-hover">
                           <img src="{{asset('uploads/images/imagine/partners/imagine2018/cbc.jpg')}}" alt="">
                       </div>
                   </div>
                   <div>
                       <div class="uk-card uk-card-hover">
                           <img src="{{asset('uploads/images/imagine/partners/imagine2018/ar.jpg')}}" alt="">
                       </div>
                   </div>
                   <div>
                    <div class="uk-card uk-card-hover">
                         <img src="{{asset('uploads/images/imagine/partners/imagine2018/ada.jpg')}}" alt="">
                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-hover">
                        <img src="{{asset('uploads/images/imagine/partners/imagine2018/sa.jpg')}}" alt="">
                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-hover">
                        <img src="{{asset('uploads/images/imagine/partners/imagine2018/yarat.jpg')}}" alt="">
                    </div>
                </div>
               </div> --}}
                <img src="{{asset('img/fantazia/logos.jpg')}}" alt="image partners" style="width:100%"> 
            </div>
        </div>
    </div>
@endsection