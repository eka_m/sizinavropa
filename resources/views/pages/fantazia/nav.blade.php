<div class="uk-container-large uk-padding-remove">
    <div class="main-nav">
        <div class="topnav" id="myTopnav">
            <a href="{{route('fantazia')}}">Home</a>
            <a href="{{route('fantazia.page','about')}}" @if ($active == 'about') class="active" @endif>About</a>
            <a href="{{route('fantazia.program')}}" @if ($active == 'programs') class="active" @endif>Program</a>
            <a href="{{route('fantazia.films')}}" @if ($active == 'movies') class="active" @endif>Movies</a>
            <a href="{{route('fantazia.events')}}" @if ($active == 'events') class="active" @endif>Events</a>
            <a href="{{route('fantazia.bakuheritage')}}" @if ($active == 'baku-heritage') class="active" @endif>Baku heritage</a>
            <a href="{{route('fantazia.europeanheritage')}}" @if ($active == 'european-heritage') class="active" @endif>European year</a>
            {{--<a href="{{route('fantazia.masterclasses')}}" @if ($active == 'masterclasses') class="active" @endif>Master classes</a>--}}
            <a href="{{route('fantazia.page','partners')}}" @if ($active == 'partners') class="active" @endif>Partners</a>
            <a href="javascript:void(0);" style="font-size:32px;" class="icon" onclick="myFunction()">&#9776;</a>
        </div>
    </div>
</div>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
</script>
