@extends('layouts.base')
@section('title','TV')
@section('css')
    @parent
    <link rel="stylesheet" href="{{asset('plugins/plyr/plyr.css')}}">
@endsection
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/plyr/plyr.js')}}"></script>
@endsection

@section('content')
    <section class="container mt-3 cl__white">
        <div class="row">
            <div class="col video__content p-3 p-sm-3 p-md-5">
                <div class="row">
                    @foreach($videos as $video)
                        <div class=" col-xs-12 col-md-6 col-lg-4 p-2 video__content__item">
                            <article class="video__content__item__video">
                                <div data-type="{{$video->type}}" data-video-id="{{$video->link}}" class="videosItem"></div>
                                <header class="video__content__item__video__info p-3">
                                    <h3>{{$video->name}}</h3>
                                    <p>{{$video->description}}</p>
                                </header>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col p-5 d-flex align-items-center">
                <div class="d-inline-block m-auto">
                    {{$videos->links('vendor.pagination.semantic')}}
                </div>
            </div>
        </div>
    </section>
@endsection