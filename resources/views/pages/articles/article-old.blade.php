@extends('layouts.base')
@section('title',$article->name)
@section('keywords',$article->keywords)
@section('description',$article->description)

@section('s-description',$article->description)
@section('s-title',$article->name)
@section('s-url',Request::fullUrl())
@section('s-image',setImage($article->image,'interesting'))

@section('css')
  @parent @if($gallery)
    <link rel='stylesheet' href='{{asset('/plugins/unitegallery/css/unite-gallery.css')}}' type='text/css'/>
    <link rel='stylesheet' href='{{asset('/plugins/unitegallery/themes/default/ug-theme-default.css')}}'
          type='text/css'/>
  @endif
  <style>
    .social-share-container {
      position: fixed;
      right: 0;
      top: 50%;
      padding:0 5px;
      z-index: 9999;
    }
    .social-share-link {
      display: block;
      width: 50px;
      height: 50px;
      padding: 8px;
      background: #FFFFFF;
      transition: all 0.5s;
      margin-top: 5px;
      margin-bottom: 5px;
    }
    .social-share-link:hover {
      transform: scale(1.1);
    }
  </style>
@endsection
@section('js')
  @parent @if($gallery)
    <script type='text/javascript' src='{{asset('/plugins/unitegallery/js/unitegallery.min.js')}}'></script>
    @foreach($gallery as $item)
      <script type='text/javascript'
              src='/plugins/unitegallery/themes/{{$item->type}}/ug-theme-{{$item->type}}.js'></script>
      <script type="text/javascript">
          $("#gallery{{$item->id}}").unitegallery(
                  {!! '{
                  gallery_theme:"'.$item->type.'",
                  tile_enable_textpanel: false,
                  lightbox_type: "compact",
                  }' !!}
          );
      </script>
    @endforeach
  @endif
@endsection

@section('content')
  <section class="row">
    <div class="col articleRead__cover p-0 mb-2">
      <div class="articleRead__cover__image imgLiquid">
        <img src="{{setImage($article->cover, 'cover')}}" alt="">
      </div>
      <div class="articleRead__cover__overlay"></div>
      <div class="articleRead__cover__info">
        <a href="/page/{{$article->page->url}}"
           class="link_reset articleRead__cover__info__category bg--{{$article->page->color}}">
          @if($currentlocale == env('DEFAULTLOCALE')){{$article->page->name}}@else {{$article->page->name_en}}@endif
        </a>
        <h1>{{$article->name}}</h1>
        <div class="articleRead__cover__info__dateviews">
          <span class="articleRead__cover__info__dateviews__date">{{$article->created_at}}</span>
          <span>/</span>
          <span class="articleRead__cover__info__dateviews__views"><i
                    class="sa-eye"></i> {{$article->fakeviews != 0 ? $article->fakeviews : $article->views}}</span>
        </div>
      </div>
    </div>
  </section>
  <section class="container articleRead__content">
    <div class="row">
      <div class="col articleRead__content cl__white p-3 p-sm-3 p-md-5">
        {!! $article->content !!}
      </div>
    </div>
  </section>
@endsection