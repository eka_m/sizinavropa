@extends('layouts.base')
@section('title','Weekly Events')

@section('content')

    <section class="row row-content events mt-3 no-gutters">
        <div class="col">
            <div class="row">
                <div class="col">
                <a href="/{{$currentlocale}}/full-calendar" style="font-size:20px; float:right;" class="tx--white d-inline-block p-3 m-2 bg--blue">{{__('words.calendar')}} > </a>
                </div>
            </div>
            <div class="row events">
                @foreach(trans('words.week') as $daynumber => $dayname)
                <div class="col  events__item p-1">
                    <div class="events__item__day bg--orange tx--white">
                        <h3>{{$dayname}}</h3>
                        {{Carbon\Carbon::createFromTimestamp(strtotime($daynumber.' day this week') - (24*60*60))->day}}
                        {{__('words.months.'.Carbon\Carbon::createFromTimestamp(strtotime($daynumber.' day this week') - (24*60*60))->month)}}
                    </div>
                    @if(isset($events[$daynumber]))
                        @foreach($events[$daynumber] as $k => $value)
                            <div class="events__item__venue">
                                <p class="events__item__venue__name">{{$k}}</p>
                                <ul class="events__item__venue__event">
                                    @foreach($value as $item)
                                        <li>
                                            @if($item->start && $item->end)
                                                <span class="events__item__venue__event__time">
                                                <span>
                                                    {{date("H:i", strtotime($item->start)).' - '}}{{date("H:i", strtotime($item->end))}}
                                                </span>
                                            </span>
                                            @endif
                                            <div class="events__item__venue__event__name">
                                                @if($item->event_name && $item->event_name != '<p><br></p>')
                                                    {!!$item->event_name!!}
                                                @else
                                                    <a class="uk-link-reset" style="color:#1b1b1b;" href="{{route('article',$item->url)}}"><p>{{$item->name}}</p></a>
                                                @endif
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    @endif
                </div>
            @endforeach
            </div>
            @if($events->isEmpty())
            <div class="row">
                <div class="col tx-center p-5">
                    <p>{{__('words.noevents')}}</p>
                </div>
            </div>
            @endif
        </div>

    </section>
@endsection