@extends('layouts.base')
@section('title','Eventa Calendar')
@section('css')
    @parent
    <link rel="stylesheet" href="{{asset('plugins/calendario/calendario.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/calendario/custom_calendario_theme_2.min.css')}}">
@endsection
@section('js')
    @parent
    <script src="{{asset('plugins/calendario/jquery.calendario.min.js')}}"></script>
    <script src="{{asset('plugins/calendario/calendario-setup.min.js')}}"></script>
    <script src="{{asset('plugins/sticky/sticky.min.js')}}"></script>
    <script>
        $(".custom-calendar-full").stick_in_parent({
            offset_top:30
        });
    </script>
@endsection
@section('content')
    <section class="row row-content gradient-bg mt-3 no-gutters">
        <div class="col-12 col-md-6 px-3 py-3 px-lg-5 py-lg-5 custom-calendar-full-container">
            <div class="custom-calendar-full">
                <div class="custom-month-year">
                        <span id="custom-prev" class="custom-prev"><i class="sa-angle-left"></i></span>
                        <span id="custom-month" class="custom-month"></span>
                        <span id="custom-year" class="custom-year"></span>
                        <span id="custom-next" class="custom-next"><i class="sa-angle-right"></i></span>
                        <nav>
                            <span id="custom-current" class="custom-current" title="Go to current date">---</span>
                        </nav>
                </div>
                <div id="fullcalendar" class="fc-calendar-container"></div>
            </div>
        </div>
        <div class="col-12 col-md-6 px-3 py-3 px-lg-5 py-lg-5 custom-calendar-day">
            <div class="custom-calendar-day-current_date">
                @php
                    $day = date('N') != 0 ? date('N') : 7;
                @endphp
                <span class="fullcalendar-current-weekday">{{__('words.week.'.$day)}}</span>,
                <span class="fullcalendar-current-day">{{date('j')}}<span>
                <span class="fullcalendar-current-month">{{__('words.months.'.date('n'))}}</span>
            </div>
            <div class="custom-calendar-day-events">
                @forelse($venuesandevents as $venue => $events)
                <div class="fullcalendar-venue">{{$venue}}</div>
                    @foreach($events as $event)
                        <div class="fullcalendar-event">
                            <span>
                                {{$event->start ? $event->start.' / ' : '-- '}}
                                {{$event->end ? $event->end : '--'}}
                            </span>
                        <a href="/article/{{$event->url}}">{!!$event->event_name ? $event->event_name : $event->name !!}</a>
                        </div>
                        @endforeach
                    @empty
                    <span class="fullcalendar-noevents">{{__('words.noevents')}}</span>
                @endforelse
            </div>
            
           
            <div id="fullcalendar-spinner">
                <div class="sk-cube-grid">
                    <div class="sk-cube sk-cube1"></div>
                    <div class="sk-cube sk-cube2"></div>
                    <div class="sk-cube sk-cube3"></div>
                    <div class="sk-cube sk-cube4"></div>
                    <div class="sk-cube sk-cube5"></div>
                    <div class="sk-cube sk-cube6"></div>
                    <div class="sk-cube sk-cube7"></div>
                    <div class="sk-cube sk-cube8"></div>
                    <div class="sk-cube sk-cube9"></div>
                  </div>
            </div>
           
        </div>
    </section>
    <div class="clearfix"></div>    
@endsection