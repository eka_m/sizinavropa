@extends('layouts.base') 
@section('title',$page->name) 
@section('keywords',$page->keywords) 
@section('description',$page->description)

@section('s-description',$page->description) 
@section('s-title',$page->name) 
@section('s-url',Request::fullUrl()) 
@section('s-image','/uploads'.$page->cover)

@section('css') @parent @if($gallery)
<link rel='stylesheet' href='{{asset(' /plugins/unitegallery/css/unite-gallery.css ')}}' type='text/css' />
<link rel='stylesheet' href='{{asset(' /plugins/unitegallery/themes/default/ug-theme-default.css ')}}' type='text/css' /> @endif
@endsection
 
@section('js') @parent @if($gallery)
<script type='text/javascript' src='{{asset(' /plugins/unitegallery/js/unitegallery.min.js ')}}'></script>
@foreach($gallery as $item)
<script type='text/javascript' src='/plugins/unitegallery/themes/{{$item->type}}/ug-theme-{{$item->type}}.js'></script>
<script type="text/javascript">
    $("#gallery{{$item->id}}").unitegallery(
                        {!! '{
                        gallery_theme:"'.$item->type.'",
                        tile_enable_textpanel: false,
                        lightbox_type: "compact",
                        }' !!}
                );

</script>
@endforeach @endif
@endsection
 
@section('content') @if($page->cover)
<section class="row">
    <div class="col articleRead__cover p-0 mb-2">
        <div class="articleRead__cover__image imgLiquid">
            <img src="/uploads{{$page->cover}}" alt="">
        </div>
        <div class="articleRead__cover__overlay"></div>
        <div class="articleRead__cover__info">
            <a href="javascript:void(0)" class="link_reset articleRead__cover__info__category bg--{{$page->color}}">
                        @if($currentlocale == env('DEFAULTLOCALE')){{$page->name}}@else {{$page->name_en}}@endif</a>
            </a>
        </div>
    </div>
</section>
@endif @if($page->content)
<section class="row mt-3">
    <div class="col row-content bg--white pl-md-5 pr-md-5 pt-5 pb-5">
        {!! $page->content !!}
    </div>
</section>
@endif @if(count($page->articles) > 0)
<section class="row pageArticles mt-3">
    <div class="col row-content">
        <div class="row">
            @forelse($articles as $article)
            <div class="col-md-3 col-lg-3 col-sm-6">
                <article class="pageArticles__item">
                    <div class="pageArticles__item__image imgLiquid p-0  br--{{$page->color}}">
                        <img src="{{setImage($article->image, 'interesting')}}" class="fluid" alt="">
                    </div>
                    <header class="pageArticles__item__info">
                        <h3>{{$article['name']}}</h3>
                        <p>{{str_limit($article['short'],200)}}</p>
                        <a href="/article/{{$article['url']}}" class="pageArticles__item__info__readmore br--{{$page->color}} bg--{{$page->color}}">
                                                &rarr;
                                            </a>
                    </header>
                </article>
            </div>
            @empty
            <div class="col-12 py-5">
                <div class="text-right py-5" style="text-align:center;">
                    <h4>Ohh.. Sorry..</h4>
                    <h5>0 articles in this language</h5>
                </div>
            </div>
            @endforelse
        </div>
        <div class="row">
            <div class="col p-3 d-flex align-items-center">
                <div class="d-inline-block m-auto">
                    {{$articles->links('vendor.pagination.semantic')}}
                </div>
            </div>
        </div>
    </div>
</section>
@endif
@endsection