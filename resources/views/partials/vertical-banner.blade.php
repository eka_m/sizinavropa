@include('partials.sidebar-header', ['header' => 'Реклама'])
<div class="row">
    <div class="col-12">
        <div class="banner vertical-banner">
            <swiper class="banner-swiper"
                    :options="{
                                    autoplay: 6000,
                                    autoHeight: true,
                                    effect: 'flip',
                                    loop: true,
                                 }">
                <swiper-slide>
                    <img src="{{asset('img/banners/b1.JPEG')}}" alt="banner-1" class="responsive">
                </swiper-slide>
                <swiper-slide>
                    <img src="{{asset('img/banners/b2.PNG')}}" alt="banner-2" class="responsive">
                </swiper-slide>
                <swiper-slide>
                    <img src="{{asset('img/banners/b3.PNG')}}" alt="banner-3" class="responsive">
                </swiper-slide>
            </swiper>
        </div>
    </div>
</div>