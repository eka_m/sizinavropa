<div class="row">
    <div class="col-12">
        <div class="blockHeader blockHeader--main">
            <span class="blockHeader__name uk-visible@s">{{$header}}</span>
            <span class="blockHeader__name uk-display-block uk-hidden@s">{{$header}}</span>
            <span class="blockHeader__line uk-visible@s"></span>
        </div>
    </div>
</div>