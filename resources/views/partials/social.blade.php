@include('partials.sidebar-header', ['header' => 'BİZİ İZLƏYİN'])
<section class="row">
    <div class="col-12 social">
            <ul class="social__list">
                <li class="social__list__item">
                    <a href="https://www.facebook.com/SizinAvropa/" target="_blank" class="social__list__item__link social__list__item__link--facebook">
                        <img src="{{asset('img/facebook.svg')}}" alt="facebook-icon">
                        <i class="i-angle-right social__list__item__link__icon"></i>
                    </a>
                </li>
                <li class="social__list__item">
                    <a href="https://www.youtube.com/channel/UCRRbADrlDJfBgJGPLvStCfA"  target="_blank" class="social__list__item__link social__list__item__link--youtube">
                        <img src="{{asset('img/youtube.svg')}}"  alt="youtube-icon">
                        <i class="i-angle-right social__list__item__link__icon"></i>
                    </a>
                </li>
            </ul>
        </div>
</section>