@if(!empty($popular))
    @include('partials.sidebar-header',['header' => 'Ən çox oxunan'])
    <section class="row">
        <div class="col-12 popularBlock uk-margin-large-bottom">
            <div v-if="$mq.resize && $mq.above($mv.md)">
                <div class="popularBlock__button popularBlock__button--next">
                    <i class="i-angle-up"></i>
                </div>
                <div class="popularBlock__button popularBlock__button--mouse">
                    <img src="{{asset('img/mouse.svg')}}" class="responsive">
                </div>
                <div class="popularBlock__button popularBlock__button--prev">
                    <i class="i-angle-down"></i>
                </div>
            </div>
            <div class="popularBlock__popular">
                <swiper :options="{
                autoplay: 8000,
                slidesPerView: 'auto',
                direction:'vertical',
                mousewheelControl: true,
                nextButton: '.popularBlock__button--next',
                prevButton: '.popularBlock__button--prev',
                height: 820
        }">
                    @foreach($popular as $article)
                        <swiper-slide>
                            <article class="popularBlock__popular__post">
                                <a href="/article/{{$article->url}}">
                                    <div class="popularBlock__popular__post__image">
                                        <img src="/uploads/{{$article->image}}" class="responsive">
                                    </div>
                                    <div class="popularBlock__popular__post__readmore">
                                        <div class="popularBlock__popular__post__readmore__icon"><i
                                                    class="i-angle-right"></i></div>
                                    </div>
                                    <header class="popularBlock__popular__post__data">
                                        <div class="popularBlock__popular__post__data__category">
                                <span class="popularBlock__popular__post__data__category__name">
                                    {{$article->page->name}}
                                </span>
                                        </div>
                                        <div class="popularBlock__popular__post__data__name">
                                            <h3>{{$article->name}}</h3>
                                        </div>
                                        <div class="popularBlock__popular__post__data__info">
                                <span class="popularBlock__popular__post__data__info__views">
                                    <i class="i-eye"></i> {{$article->fakeviews ? $article->fakeviews : $article->views}}
                                </span>
                                            <span class="popularBlock__popular__post__data__info__date">
                                    <i class="i-calendar"></i> {{$article->created_at}}
                                </span>
                                        </div>
                                    </header>
                                </a>
                            </article>
                        </swiper-slide>
                    @endforeach

                </swiper>

            </div>

        </div>
    </section>
@endif