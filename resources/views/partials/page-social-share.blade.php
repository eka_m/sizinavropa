<div class="uk-flex">
    <div class="uk-inline uk-margin-auto">
        <a href="#" class="uk-link-reset" data-social="facebook">
            <img src="{{url('img/facebook-page.svg')}}" class="social-button">
        </a>
        &nbsp;
        &nbsp;
        <a href="#" class="uk-link-reset" data-social="twitter">
            <img src="{{url('img/twitter-page.svg')}}" class="social-button">
        </a>
    </div>
</div>