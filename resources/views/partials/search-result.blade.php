@extends('layouts.base')
@section('title','Результаты поиска')
@section('content')
<section class="row pageArticles mt-3">
    <div class="col row-content bg--white">
        <div class="row">
            @forelse($articles as $article)
                <div class="col-md-12 col-lg-6">
                    <article class="row pageArticles__item">
                        <div class="pageArticles__item__image imgLiquid p-0 col-sm-6 col-md-6 col-lg-6 br--{{$article->page->color}}">
                            <img src="/uploads{{$article['image']}}" class="fluid" alt="">
                        </div>
                        <header class="pageArticles__item__info col-sm-6 col-md-6 col-lg-6 ">
                            <h3>{{$article['name']}}</h3>
                            <p>{{str_limit($article['short'],200)}}</p>
                            <a href="/article/{{$article['url']}}"
                               class="pageArticles__item__info__readmore br--{{$article->page->color}} bg--{{$article->page->color}}">
                                &rarr;
                            </a>
                        </header>
                    </article>
                </div>
                @empty
                <div class="col-12 py-5">
                    <div class="text-right py-5" style="text-align:center;">
                        <h4>Ohh.. Sorry..</h4>
                        <h5>Nothink found</h5>
                    </div>
                </div>
            @endforelse
        </div>
        <div class="row">
            <div class="col p-3 d-flex align-items-center">
                <div class="d-inline-block m-auto">
                    {{$articles->appends(['query' => Request::get('query')])->links('vendor.pagination.semantic')}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection