


<div id="gallery{{$gallery->id}}" class="gallery{{$gallery->id}} unite-gallery" style="display:none; margin: 0 auto">
    @foreach(json_decode($gallery->images) as $item)
        <img src="/uploads{{$item->path}}"
             alt="{{$item->title}}"
             data-image="/uploads{{$item->path}}">
    @endforeach
</div>

{{--data-description="{{item.description}}"--}}