<div class="row">
    <div class="col-md-8 offset-md-2">
        <div class="banner horizontal-banner">
            <swiper :options="{autoplay: 6000, loop: true, effect: 'fade'}">
                <swiper-slide>
                    <div class="banner-item">
                        <img src="{{asset('img/banners/bv1.PNG')}}" alt="banner-1" class="responsive">
                    </div>
                </swiper-slide>
                <swiper-slide>
                    <div class="banner-item">
                        <img src="{{asset('img/banners/bv2.PNG')}}" alt="banner-2" class="responsive">
                    </div>
                </swiper-slide>
                <swiper-slide>
                    <div class="banner-item">
                        <img src="{{asset('img/banners/bv3.PNG')}}" alt="banner-3" class="responsive">
                    </div>
                </swiper-slide>
            </swiper>
        </div>
    </div>
</div>