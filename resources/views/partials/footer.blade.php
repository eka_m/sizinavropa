<footer class="row footer">
    <div class="col-md-4 footer__left">
        <div class="row">
            <div class="col-6 footer__left__logo uk-margin-bottom">
                <img src="{{asset('img/logo3.png')}}" alt="footer-logo" class="responsive">
            </div>
            <div class="col-12 ooter__left__menu">
                <ul class="footer__left__menu__list">
                    @foreach($clearpages as $page)
                        <li class="footer__left__menu__list__item">
                            <a href="/page/{{$page->url}}" class="footer__left__menu__list__item__link uk-button-text">
                                {{$page->name}}
                            </a>
                        </li>
                    @endforeach
                    @foreach($toppages as $toppage)
                        <li class="footer__left__menu__list__item">
                            <a href="/page/{{$toppage->url}}"
                               class="footer__left__menu__list__item__link uk-button-text">
                                {{$toppage->name}}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-4 footer__center">
        <div class="row">
            <div class="col-12 footer__center__header uk-margin-bottom">
                <h5>Maraqlıdır</h5>
            </div>
            <div class="col-12 footer__center__interesting">
                <swiper :options="{
                            autoplay: 8000,
                            effect: 'flip',
                            mousewheelControl: true

                }">
                    @foreach($interesting as $article)
                        <swiper-slide>
                            <article class="footer__center__interesting__article">
                                <div class="footer__center__interesting__article__image">
                                    {{-- <img src="{{setImage($article->image, 'interesting')}}" alt="footer-image"
                                         class="responsive"> --}}
                                </div>
                                <header class="footer__center__interesting__article__data">
                                    <div class="footer__center__interesting__article__data__name">
                                        <h5>{{$article->name}}</h5>
                                        <a href="/article/{{$article->url}}" class="footer__center__interesting__article__data__link"><i
                                                    class="i-link"></i></a>
                                    </div>
                                </header>
                            </article>
                        </swiper-slide>
                    @endforeach
                </swiper>
            </div>
        </div>
    </div>
    <div class="col-md-4 footer__right">
        <div class="row footer__right__form">
            <div class="col-12 footer__right__form__header">
                <h5>
                    Bİzə yazın
                </h5>
            </div>
            <div class="col-12 footer__right__form__inputs">
                <input type="text" placeholder="Ad, Soyad">
                <input type="email" placeholder="Email">
                <textarea cols="30" rows="10" placeholder="Mesaj"></textarea>
                <a href="" class="btn footer__right__form__inputs__sendbtn">Göndər</a>
            </div>
        </div>
    </div>
    <div class="col-12 footer__copyright">
        <p>Sayt Avropa Birliyinin Azərbaycandakı Nümayəndəliyinin dəstəyi ilə hazırlanıb.</p>
        <p>2017 &copy; SizinAvropa.az</p>
    </div>
</footer>