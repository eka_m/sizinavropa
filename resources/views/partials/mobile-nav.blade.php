<div id="mobile-menu" uk-offcanvas="overlay: true;">
    <div class="uk-offcanvas-bar mobileMenu">
        <div class="uk-margin-large-bottom ">
            <button class="uk-offcanvas-close " type="button" uk-close></button>
        </div>
        <div class="uk-margin mobileMenu__auth uk-margin-small-left">
            <ul class="uk-list mobileMenu__auth__list">
                <li class="mobileMenu__auth__list__item">
                    <a href="#" class="uk-button-text mobileMenu__auth__link mobileMenu__auth__list__item__link--login"><span uk-icon="icon:sign-in"></span>Daxil ol /</a>
                    <a href="#" class="uk-button-text mobileMenu__auth__link mobileMenu__auth__link--register">Qeydiyyat</a>
                </li>
            </ul>
        </div>
        <div class="uk-margin mobileMenu__first">
            <nav id="navigation">
                {!! $pages !!}
            </nav>
        </div>
        <div class="uk-margin mobileMenu__second">
            <ul class="uk-list mobileMenu__second__list uk-margin-small-left">
                @foreach($toppages as $page)
                    <li class="mobileMenu__second__list__item">
                        <a href="/page/{{$page->url}}"
                           class="uk-button-text mobileMenu__second__list__item__link {{ Request::is('page/'.$page->url) ? 'mobileMenu__second__list__item__link--active' : '' }} ">
                            {{$page->name}}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>