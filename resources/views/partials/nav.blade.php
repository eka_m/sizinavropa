<div class="row navBlock">
    <div class="col-12">
        <nav id="navigation" class="uk-visible@m" v-if="$mq.resize && $mq.above($mv.md)">
            {!! $pages !!}
        </nav>

    </div>
</div>
