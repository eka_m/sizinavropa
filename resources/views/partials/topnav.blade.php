<div class="topNav uk-visible@s">
    <ul class="topNav__list">
        <li class="topNav__list__item topNav__list__item--imagine">
            <a href="{{route('imagine')}}">European Film Festival</a>
        </li>
        @foreach($toppages as $page)
            <li class="topNav__list__item">
                <a href="/page/{{$page->url}}"
                   class="topNav__list__item__link {{ Request::is('page/'.$page->url) ? 'topNav__list__item__link--active' : '' }} ">
                    {{$page->name}}
                </a>
            </li>
        @endforeach
    </ul>
</div>