<div class="row">
    <div class="uk-hidden@s col-12 no-padding">
        <swiper>
            @foreach($slides as $slide)
                <swiper-slide class="uk-card">
                    <div class="uk-card-media-top">
                        <img src="/uploads/{{$slide->image}}" alt="main-image" class="responsive slide__item">
                    </div>
                    <div class="uk-card-body">
                        <a href="{{$slide->link ? $slide->link : 'javascript:void(0)'}}">
                            <h3 class="brand-tx-color-6 uk-text-center">
                                {{$slide->name}}
                            </h3>
                        </a>
                    </div>

                </swiper-slide>
            @endforeach
        </swiper>
    </div>
    <div class="col-12 slider-pro no-padding uk-visible@s uk-height-large" id="my-slider">
        <div class="sp-slides mainSlider">
            @foreach($slides as $slide)
                <div class="sp-slide">
                    <img class="sp-image mainSlider__cover uk-visible@s" src="/uploads{{$slide->image}}"
                         data-src="/uploads{{$slide->image}}">
                    <a href="{{$slide->link ? $slide->link : 'javascript:void(0)'}}">
                        <h3 class="uk-h1 sp-layer mainSlider__title"
                            data-position="topLeft" data-horizontal="15%" data-vertical="15%"
                            data-show-transition="right" data-show-delay="500"
                            data-hide-transition="right">
                            {{$slide->name}}
                        </h3>
                    </a>
                    @if($slide->description)
                        <a href="{{$slide->link ? $slide->link : 'javascript:void(0)'}}">
                            <p class="sp-layer mainSlider__description"
                               data-position="bottomLeft"
                               data-horizontal="15%"
                               data-vertical="20%"
                               data-show-transition="down" data-show-delay="1000"
                               data-hide-transition="down">
                                {{$slide->description}}
                            </p>
                        </a>
                    @endif
                    <div class="sp-layer sp-padding mainSlider__imageBlock"
                         data-width="450px"
                         data-position="topRight"
                         data-horizontal="10%"
                         data-vertical="20%"
                         data-show-transition="up" data-show-delay="500"
                         data-hide-transition="down">
                        <a href="{{$slide->link ? $slide->link : 'javascript:void(0)'}}">
                            <img class="mainSlider__imageBlock__image" src="/uploads/{{$slide->image}}"
                                 data-src="/uploads/{{$slide->image}}">
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
        {{--<div class="sp-thumbnails">--}}
        {{--@foreach($slides as $slide)--}}
        {{--<div class="sp-thumbnail">--}}
        {{--<img src="/uploads/{{$slide->image}}"--}}
        {{--data-src="/uploads/{{$slide->image}}"/>--}}
        {{--</div>--}}
        {{--@endforeach--}}
        {{--</div>--}}
    </div>
</div>
