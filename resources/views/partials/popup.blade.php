@if($popup['status'])
    <div class="uk-section uk-section-xsmall">
        <div class="uk-container uk-container-small">
            <popup>
                <div class="uk-card popup uk-flex" uk-grid>
                    <figure class="uk-width-1-2@m">
                        <a href="/article/{{$popup['items'][0]->url}}">
                            <img src="/uploads/{{$popup['items'][0]->image}}"
                                 class="responsive uk-margin-auto uk-margin-auto-vertical uk-responsive-width">
                        </a>
                    </figure>
                    <header class="uk-card-body uk-margin-remove-top@S uk-width-1-2@m uk-flex-first@m uk-padding-remove-bottom">
                        <div class="head-font uk-text-medium">
                            <a href="/article/{{$popup['items'][0]->url}}" class="uk-link-reset">
                                <h2 class="head-font brand-tx-color-1 uk-margin-remove-bottom">{{$popup['items'][0]->name}}</h2>
                            </a>
                            <h6 class="uk-heading-line brand-tx-color-3 uk-margin-remove-top"><span>Ən maraqlı</span>
                            </h6>
                            <div class="uk-margin uk-text-small brand-tx-color-6 uk-visible@s">
                                <a href="/article/{{$popup['items'][0]->url}}" class="uk-link-reset">
                                    {{$popup['items'][0]->short}}
                                </a>
                            </div>
                            <div class="uk-margin uk-text-small brand-tx-color-6 uk-hidden@s">
                                <a href="/article/{{$popup['items'][0]->url}}" class="uk-link-reset">
                                    {{str_limit($popup['items'][0]->short,100,'...')}}
                                </a>
                            </div>
                        </div>
                        <br>
                        <div>
                            <ul class="uk-list">
                                @foreach(collect($popup['items'])->splice(1,5) as $item)
                                    <li><a href="/article/{{$item->url}}" class="uk-button-text uk-h5 brand-tx-color-1"><span
                                                    class="head-font">{{$item->name}}</span></a></li>
                                @endforeach
                            </ul>
                        </div>
                    </header>

                </div>
            </popup>
        </div>
    </div>
@endif