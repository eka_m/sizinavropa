<div class="uk-section uk-section-xsmall ">
    <div class="uk-container uk-container-large">
        @if($cover)
            <div class="uk-inline uk-margin uk-width-1-1 uk-margin-small-top">
                    <img src="/uploads/{{$cover}}" alt="page-cover" style="width: 100%; min-height: 200px;">
                    <div class="uk-overlay-cover uk-position-cover"
                         style="background: RGBA(0, 0, 0, {{$opacity}});"></div>
                <div class="uk-position-center uk-light uk-flex uk-flex-center uk-flex-middle">
                    <h1 class="uk-heading-primary uk-text-center head-font"><span>{{$name}}</span></h1>
                    @if($social)
                        @include('partials.page-social-share')
                    @endif
                </div>
            </div>
        @else
            <div class="uk-margin">
                <h1 class="uk-heading-primary uk-text-center head-font"><span>{{$name}}</span></h1>
            </div>
        @endif
    </div>
</div>