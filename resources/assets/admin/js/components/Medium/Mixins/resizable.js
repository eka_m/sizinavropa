import resizable from 'jquery-resizable-dom';
export const mdresizable = {
    data () {
        return {
            resizable: false
        }
    },
    methods: {
        initResizable () {
            const elem = $(this.$refs.editor).children('figure');
            if (!this.resizable) {
                const inst = this;
                elem.each((i, element) => {
                    element = $(element);
                    element.append('<i class="dj-caret-down resize-handle"></i>');
                    const height = element.data('resizableheight') ? element.data('resizableheight') : false;
                    this.resizable = element.resizable({
                        handleSelector: "> .resize-handle",
                        resizeHeight: height,
                        onDragStart () {
                        },
                        onDrag (e, $el, newWidth, newHeight, opt) {
                            if (newWidth < 300) {
                                newWidth = 300;
                            }
                            if (height) {
                                $el.height(newHeight);
                            }
                            $el.width("100%");
                            $el.css("maxWidth", newWidth);
                            return false;
                        },
                        onDragEnd (){
                            inst.setResult();
                        }
                    });
                });
            } else {
                elem.children('.resize-handle').remove();
                this.resizable = false;
            }
        }
    }
};