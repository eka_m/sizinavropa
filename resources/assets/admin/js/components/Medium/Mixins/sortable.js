export const mdsortable = {
    data () {
        return {
            sortable: false
        }
    },
    methods: {
        initSortable () {
            if (!this.sortable) {
                this.sortable = UIkit.sortable(this.$refs.editor);
            } else {
                this.sortable.$destroy();
                this.sortable = false;
            }
        },
    }
};