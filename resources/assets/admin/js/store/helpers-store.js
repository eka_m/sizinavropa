export default {
	basePath: document.head.querySelector('base').href,
	csrf: document.head.querySelector('meta[name="csrf-token"]').content
}
