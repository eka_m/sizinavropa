import slimImage from "./store/slim-image-store";
export default {
    slimImage:slimImage,
    init(obj, value) {
        return _.merge(this[obj],value);
    },
    set(obj, key, value) {
        return _.set(this[obj], key, value);
    },
    get(obj, key, value = false) {
        return _.get(this[obj], key, value);
    }
};
