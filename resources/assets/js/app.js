import es6Promise from 'es6-promise'
es6Promise.polyfill();
import Vue from 'vue';
import vMediaQuery from 'v-media-query'
import VueContentPlaceholders from 'vue-content-placeholders'
Vue.use(VueContentPlaceholders)
Vue.use(vMediaQuery, {
    variables: {
        xxs: '321px',
        xs: '575px',
        sm: '769px',
        md: '991px',
        lg: '1199px',
        xl: '1600px'
    }
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('slick', r => require.ensure([], () => r(require('./components/Slick/slick.vue')), 'slick'));
Vue.component('plyr', r => require.ensure([], () => r(require('./components/Plyr/Plyr.vue')), 'plyrjs'));
Vue.component('videoChannel', r => require.ensure([], () => r(require('./components/Videochannel/videochannel.vue')), 'videochannel'));

const app = new Vue({
    el: '#app'
});
