$(document).ready(function() {
  $('#navigation').navigation({
    showDuration: 200,
    effect: 'slide',
    mobileBreakpoint: '960'
  });
  if ($('.imgLiquid').length) {
    $('.imgLiquid').imgLiquid();
  }
  if ($('.slickSld').length) {
    $('.slickSld').each(function(i, item) {
      var elem = $(item);
      elem.slick();
      $('.slick-imgLiquid').imgLiquid();
    });
  }
  if ($('.videosItem').length) {
    $('.videosItem').each(function(i, item) {
      plyr.setup(item);
    });
  }
  if ($('.lastNewsSlider').length) {
    $('.lastNewsSlider').slick({
      arrows: true,
      slidesToShow: 4,
      slidesToScroll: 4,
      prevArrow: '.lastNewsLeft',
      nextArrow: '.lastNewsRight',
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 531,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  }
  if ($('.tvvideo').length) {
    var instance = plyr.setup('.tvvideo');
    var currentvideo = instance[0];
  }
  var colors = {
    blue: '#009BDE',
    green: '#00AF00',
    yellow: '#FFE53B',
    orange: '#F5811F',
    red: '#FF2525',
    purple: '#aa34c6',
    tiffany: '#00C4B1',
    white: '#FFFFFF',
    black: '#000000'
  };
  $('.smallTvVideo').on('click', function(event) {
    var elem = $(this);
    var video = elem.data('video');
    var color = elem.children('header').data('color');

    var title = elem
      .children('header')
      .children('h3')
      .text();
    var description = elem.children('header').data('description');
    $('.tvvideoinfo').html(
      '<h3 class="mb-1">' + title + '</h3><p>' + description + '</p>'
    );
    $('.videoChannel__row__items__item--big').css(
      'background-color',
      colors[color]
    );
    console.log(color);
    currentvideo.source({
      sources: [
        {
          src: video.link,
          type: video.type
        }
      ]
    });
    currentvideo.on('ready', function() {
      currentvideo.play();
    });
  });
});
