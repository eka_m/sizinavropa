/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// import './bootstrap.js'
// window.Vue = require('vue');
import Vue from 'vue';
import VueLazyload from 'vue-lazyload';
import UIkit from 'uikit';
import vMediaQuery from 'v-media-query'
import {swiper, swiperSlide} from 'vue-awesome-swiper'

window.UIkit = UIkit;
Vue.use(VueLazyload, {
    preLoad: 1.3,
    // error: 'dist/error.png',
    loading: '../img/ring.gif',
    attempt: 3,
    lazyComponent: true
});

Vue.use(vMediaQuery, {
    variables: {
        xxs: '320px',
        xs: '575px',
        sm: '640px',
        md: '960px',
        lg: '1200px',
        xl: '1600px'
    }
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('plyr', r => require.ensure([], () => r(require('./components/Plyr.vue')), 'plyr'));
Vue.component('video-channel', r => require.ensure([], () => r(require('./components/VideoChannel.vue')), 'vchannel'));
Vue.component('swiper', r => require.ensure([], () => r(swiper), 'swiper'));
Vue.component('swiper-slide', r => require.ensure([], () => r(swiperSlide), 'swiper'));
Vue.component('pagination', r => require.ensure([], () => r(require('laravel-vue-pagination')), 'pagination'));
Vue.component('lazy-img', r => require.ensure([], () => r(require('./components/Imglazy.vue')), 'lazy-img'));
Vue.component('article-locale', r => require.ensure([], () => r(require('./components/article-locale.vue')), 'article-locale'));
Vue.component('popup', r => require.ensure([], () => r(require('./components/Popup.vue')), 'popup'));

const app = new Vue({
    el: '#app'
});

