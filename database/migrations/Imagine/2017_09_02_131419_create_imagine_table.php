<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagine', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('org_title')->nullable();
            $table->string('year')->nullable();
            $table->string('country')->nullable();
            $table->string('length')->nullable();
            $table->string('genre')->nullable();
            $table->string('director')->nullable();
            $table->string('writers')->nullable();
            $table->string('cast')->nullable();
            $table->string('synopsis')->nullable();
            $table->string('date')->nullable();
            $table->string('time')->nullable();
            $table->string('venue')->nullable();
            $table->string('poster')->nullable();
            $table->string('trailer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagines');
    }
}
