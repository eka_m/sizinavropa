<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('page_id');
            $table->text('name')->nullable();
            $table->text('short')->nullable();
            $table->text('content')->nullable();
            $table->text('url')->nullable();
            $table->string('image')->nullable();
            $table->text('keywords')->nullable();
            $table->text('description')->nullable();
            $table->string('cover')->nullable();
            $table->smallInteger('locale_id');
            $table->smallInteger('parent_id')->default(0);
            $table->string('coveropacity',3)->default(0.5);
            $table->boolean('person')->default(false);
            $table->boolean('status')->default(false);
            $table->boolean('interesting')->default(false);
            $table->boolean('popup')->default(false);
            $table->integer('pos')->default(999);
            $table->integer('views')->default(0)->nullable();
            $table->integer('fakeviews')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
