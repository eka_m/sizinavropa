<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('parent_id')->default(0);
            $table->text('name')->nullable();
            $table->text('content')->nullable();
            $table->text('url')->nullable();
            $table->string('image')->nullable();
            $table->boolean('social')->default(false);
            $table->text('keywords')->nullable();
            $table->text('description')->nullable();
            $table->string('cover')->nullable();
            $table->string('place')->default('mainmenu');
            $table->string('coveropacity',3)->default(0.5);
            $table->enum('status', ['active','passive','draft'])->default('passive')->nullable();
            $table->integer('pos')->default(999);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
