<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
//        factory(\App\Models\Admin\Page::class, 5)->create();
//        factory(\App\Models\Admin\Article::class, 20)->create();
        factory(\App\Models\Admin\Video::class, 20)->create();
    }
}
