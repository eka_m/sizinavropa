<?php
Route::group(['prefix' => "fantazia"], function () {
    Route::get('/', 'Admin\FantaziaController@index')->name('admin.fantazia.index');
    Route::get('/films', 'Admin\FantaziaController@films')->name('admin.fantazia.films');
    Route::get('/films/create', 'Admin\FantaziaController@createfilm')->name('admin.fantazia.create.film');
    Route::post('/films/store', 'Admin\FantaziaController@storefilm')->name('admin.fantazia.store.film');
    Route::get('/films/{id}/edit', 'Admin\FantaziaController@editfilm')->name('admin.fantazia.edit.film');
    Route::put('/films/update/{id}', 'Admin\FantaziaController@updatefilm')->name('admin.fantazia.update.film');
    Route::get('/films/delete/{id}', 'Admin\FantaziaController@deletefilm')->name('admin.fantazia.delete.film');
    Route::get('/program', 'Admin\FantaziaController@program')->name('admin.fantazia.program');
    Route::get('/program/create', 'Admin\FantaziaController@createprogramevent')->name('admin.fantazia.create.program.event');
    Route::post('/program/store', 'Admin\FantaziaController@storeprogramevent')->name('admin.fantazia.store.program.event');
    Route::get('/program/{id}/edit', 'Admin\FantaziaController@editprogramevent')->name('admin.fantazia.edit.program.event');
    Route::put('/program/update/{id}', 'Admin\FantaziaController@updateprogramevent')->name('admin.fantazia.update.program.event');
    Route::get('/program/{id}', 'Admin\FantaziaController@deleteprogramevent')->name('admin.fantazia.delete.program.event');

    Route::get('/events', 'Admin\FantaziaController@events')->name('admin.fantazia.events');
    Route::get('/event/create', 'Admin\FantaziaController@createevent')->name('admin.fantazia.create.event');
    Route::post('/event/store', 'Admin\FantaziaController@storeevent')->name('admin.fantazia.store.event');
    Route::get('/event/{id}/edit', 'Admin\FantaziaController@editevent')->name('admin.fantazia.edit.event');
    Route::put('/event/update/{id}', 'Admin\FantaziaController@updateevent')->name('admin.fantazia.update.event');
    Route::get('/event/{id}', 'Admin\FantaziaController@deleteevent')->name('admin.fantazia.delete.event');

    Route::get('/masterclasses', 'Admin\FantaziaController@masterclasses')->name('admin.fantazia.masterclasses');
    Route::get('/masterclass/create', 'Admin\FantaziaController@createmasterclass')->name('admin.fantazia.create.masterclass');
    Route::post('/masterclass/store', 'Admin\FantaziaController@storemasterclass')->name('admin.fantazia.store.masterclass');
    Route::get('/masterclass/{id}/edit', 'Admin\FantaziaController@editmasterclass')->name('admin.fantazia.edit.masterclass');
    Route::put('/masterclass/update/{id}', 'Admin\FantaziaController@updatemasterclass')->name('admin.fantazia.update.masterclass');
    Route::get('/masterclass/{id}', 'Admin\FantaziaController@deletemasterclass')->name('admin.fantazia.delete.masterclass');
});