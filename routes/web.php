<?php


use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::get('/pini', function () {
    phpinfo();
});
Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
    Route::get('/', 'HomeController@show')->name('home');
    // Route::get('/home', 'HomeController@show')->name('home');
    Route::get('/article/{url}', 'ArticlesController@show')->name('article');
    Route::get('/page/{url}', 'PagesController@show')->name('page');
    Route::get('/videos', 'VideosController@show')->name('videos');
    Route::get('/events-calendar', 'EventsController@show')->name('events-show');
    Route::get('/full-calendar', 'EventsController@calendar')->name('full-calendar');
    Route::get('/async-one-day-events/{day}', 'EventsController@asyncOneDayEvents');
    Route::get('/allEvents', 'EventsController@allEvents');
});

Route::group(['prefix' => "fantazia"], function () {
    Route::get('/', 'FantaziaController@show')->name('fantazia');
    Route::get('/page/{page}', 'FantaziaController@page')->name('fantazia.page');
    Route::get('/movie/{slug}', 'FantaziaController@film')->name('fantazia.film');
    Route::get('/movies', 'FantaziaController@films')->name('fantazia.films');
    Route::get('/program', 'FantaziaController@program')->name('fantazia.program');
    Route::get('/events', 'FantaziaController@events')->name('fantazia.events');
    Route::get('/event/{slug}', 'FantaziaController@event')->name('fantazia.event');


    Route::get('/baku-heritage', 'FantaziaController@bakuHeritage')->name('fantazia.bakuheritage');
    Route::get('/european-year-of-cultural-heritage', 'FantaziaController@europeanHeritage')->name('fantazia.europeanheritage');
});
Route::group(['prefix' => "imagine"], function () {
    Route::get('/', 'ImagineController@show')->name('imagine');
    Route::get('/page/{page}', 'ImagineController@page')->name('imagine.page');
    Route::get('/movie/{slug}', 'ImagineController@film')->name('imagine.film');
    Route::get('/movies', 'ImagineController@films')->name('imagine.films');
    Route::get('/program', 'ImagineController@program')->name('imagine.program');
    Route::get('/events', 'ImagineController@events')->name('imagine.events');
    Route::get('/event/{slug}', 'ImagineController@event')->name('imagine.event');


    Route::get('/baku-heritage', 'ImagineController@bakuHeritage')->name('imagine.bakuheritage');
    Route::get('/european-year-of-cultural-heritage', 'ImagineController@europeanHeritage')->name('imagine.europeanheritage');
});
//Route::get('/home', 'HomeController@show')->name('home');
//Route::get('/page/{url}', 'PagesController@show')->name('page');
//Route::get('/article/{url}', 'ArticlesController@show')->name('article');
Route::get('/home/search', 'HomeController@search')->name('search');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Auth::routes();

/* Admin Routes */


Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'Admin\HomeController@show');
    Route::post('/pages/sort', 'Admin\PagesController@sort');
    Route::resource('/pages', 'Admin\PagesController');
    Route::get('/articles/search', 'Admin\ArticlesController@search')->name('articles.search');
    Route::get('/articles/searcharticle', 'Admin\ArticlesController@searchArticle');
    Route::get('/articles/setperson/{id}', 'Admin\ArticlesController@setPerson')->name('articles.setperson');
    Route::post('/articles/searchparent', 'Admin\ArticlesController@searchparent');
    Route::get('/articles/setstatus/{id}', 'Admin\ArticlesController@setStatus')->name('articles.setstatus');
    Route::get('/articles/setinteresting/{id}', 'Admin\ArticlesController@setInteresting')->name('articles.setinteresting');
    Route::post('/articles/sort/{id}', 'Admin\ArticlesController@sort')->name('articles.sort');
    Route::post('/articles/fakeviews/{id}', 'Admin\ArticlesController@fakeViews')->name('articles.fakeviews');
    Route::get('/articles/popup/{id}', 'Admin\ArticlesController@popup')->name('articles.popup');
    Route::resource('/articles', 'Admin\ArticlesController');
    Route::get('/videos/search', 'Admin\VideosController@search')->name('videos.search');
    Route::get('/videos/async', 'Admin\VideosController@async')->name('videos.async');
    Route::resource('/videos', 'Admin\VideosController');
    Route::get('/slides/search', 'Admin\SlidesController@search')->name('slides.search');
    Route::get('/slides/async', 'Admin\SlidesController@async')->name('slides.async');
    Route::resource('/slides', 'Admin\SlidesController');
    Route::get('/banners/search', 'Admin\BannersController@search')->name('banners.search');
    Route::get('/banners/async', 'Admin\BannersController@async')->name('banners.async');
    Route::resource('/banners', 'Admin\BannersController');
    Route::get('/locale/setstatus/{id}', 'Admin\LocaleController@setStatus');
    Route::post('/locale/sort', 'Admin\LocaleController@sort');
    Route::resource('/locale', 'Admin\LocaleController');
    Route::get('/settings/setstatus/{settingname}', 'Admin\SettingController@setStatus')->name('setting.setstatus');
    Route::resource('/settings', 'Admin\SettingController');
    Route::get('/users/delete/{id}', 'Admin\UsersController@destroy')->name('users.delete');
    Route::resource('/users', 'Admin\UsersController');
    Route::get('/mediamanager', 'Admin\MediaController@show')->name('mediamanager.index');
    Route::get('/galleries/getAll', 'Admin\GalleryController@getAllGalleriesInJson');
    Route::resource('/galleries', 'Admin\GalleryController');


    /*IMAGINE*/
    Route::group(['prefix' => "imagine"], function () {
        Route::get('/', 'Admin\ImagineController@index')->name('admin.imagine.index');
        Route::get('/films', 'Admin\ImagineController@films')->name('admin.imagine.films');
        Route::get('/films/create', 'Admin\ImagineController@createfilm')->name('admin.imagine.create.film');
        Route::post('/films/store', 'Admin\ImagineController@storefilm')->name('admin.imagine.store.film');
        Route::get('/films/{id}/edit', 'Admin\ImagineController@editfilm')->name('admin.imagine.edit.film');
        Route::put('/films/update/{id}', 'Admin\ImagineController@updatefilm')->name('admin.imagine.update.film');
        Route::get('/films/delete/{id}', 'Admin\ImagineController@deletefilm')->name('admin.imagine.delete.film');
        Route::get('/program', 'Admin\ImagineController@program')->name('admin.imagine.program');
        Route::get('/program/create', 'Admin\ImagineController@createprogramevent')->name('admin.imagine.create.program.event');
        Route::post('/program/store', 'Admin\ImagineController@storeprogramevent')->name('admin.imagine.store.program.event');
        Route::get('/program/{id}/edit', 'Admin\ImagineController@editprogramevent')->name('admin.imagine.edit.program.event');
        Route::put('/program/update/{id}', 'Admin\ImagineController@updateprogramevent')->name('admin.imagine.update.program.event');
        Route::get('/program/{id}', 'Admin\ImagineController@deleteprogramevent')->name('admin.imagine.delete.program.event');

        Route::get('/events', 'Admin\ImagineController@events')->name('admin.imagine.events');
        Route::get('/event/create', 'Admin\ImagineController@createevent')->name('admin.imagine.create.event');
        Route::post('/event/store', 'Admin\ImagineController@storeevent')->name('admin.imagine.store.event');
        Route::get('/event/{id}/edit', 'Admin\ImagineController@editevent')->name('admin.imagine.edit.event');
        Route::put('/event/update/{id}', 'Admin\ImagineController@updateevent')->name('admin.imagine.update.event');
        Route::get('/event/{id}', 'Admin\ImagineController@deleteevent')->name('admin.imagine.delete.event');

        Route::get('/masterclasses', 'Admin\ImagineController@masterclasses')->name('admin.imagine.masterclasses');
        Route::get('/masterclass/create', 'Admin\ImagineController@createmasterclass')->name('admin.imagine.create.masterclass');
        Route::post('/masterclass/store', 'Admin\ImagineController@storemasterclass')->name('admin.imagine.store.masterclass');
        Route::get('/masterclass/{id}/edit', 'Admin\ImagineController@editmasterclass')->name('admin.imagine.edit.masterclass');
        Route::put('/masterclass/update/{id}', 'Admin\ImagineController@updatemasterclass')->name('admin.imagine.update.masterclass');
        Route::get('/masterclass/{id}', 'Admin\ImagineController@deletemasterclass')->name('admin.imagine.delete.masterclass');
    });
    /*END IMAGINE*/


    /*FANTAZIA*/
    Route::group(['prefix' => "fantazia"], function () {
        Route::get('/', 'Admin\FantaziaController@index')->name('admin.fantazia.index');
        Route::get('/films', 'Admin\FantaziaController@films')->name('admin.fantazia.films');
        Route::get('/films/create', 'Admin\FantaziaController@createfilm')->name('admin.fantazia.create.film');
        Route::post('/films/store', 'Admin\FantaziaController@storefilm')->name('admin.fantazia.store.film');
        Route::get('/films/{id}/edit', 'Admin\FantaziaController@editfilm')->name('admin.fantazia.edit.film');
        Route::put('/films/update/{id}', 'Admin\FantaziaController@updatefilm')->name('admin.fantazia.update.film');
        Route::get('/films/delete/{id}', 'Admin\FantaziaController@deletefilm')->name('admin.fantazia.delete.film');
        Route::get('/program', 'Admin\FantaziaController@program')->name('admin.fantazia.program');
        Route::get('/program/create', 'Admin\FantaziaController@createprogramevent')->name('admin.fantazia.create.program.event');
        Route::post('/program/store', 'Admin\FantaziaController@storeprogramevent')->name('admin.fantazia.store.program.event');
        Route::get('/program/{id}/edit', 'Admin\FantaziaController@editprogramevent')->name('admin.fantazia.edit.program.event');
        Route::put('/program/update/{id}', 'Admin\FantaziaController@updateprogramevent')->name('admin.fantazia.update.program.event');
        Route::get('/program/{id}', 'Admin\FantaziaController@deleteprogramevent')->name('admin.fantazia.delete.program.event');

        Route::get('/events', 'Admin\FantaziaController@events')->name('admin.fantazia.events');
        Route::get('/event/create', 'Admin\FantaziaController@createevent')->name('admin.fantazia.create.event');
        Route::post('/event/store', 'Admin\FantaziaController@storeevent')->name('admin.fantazia.store.event');
        Route::get('/event/{id}/edit', 'Admin\FantaziaController@editevent')->name('admin.fantazia.edit.event');
        Route::put('/event/update/{id}', 'Admin\FantaziaController@updateevent')->name('admin.fantazia.update.event');
        Route::get('/event/{id}', 'Admin\FantaziaController@deleteevent')->name('admin.fantazia.delete.event');

        Route::get('/masterclasses', 'Admin\FantaziaController@masterclasses')->name('admin.fantazia.masterclasses');
        Route::get('/masterclass/create', 'Admin\FantaziaController@createmasterclass')->name('admin.fantazia.create.masterclass');
        Route::post('/masterclass/store', 'Admin\FantaziaController@storemasterclass')->name('admin.fantazia.store.masterclass');
        Route::get('/masterclass/{id}/edit', 'Admin\FantaziaController@editmasterclass')->name('admin.fantazia.edit.masterclass');
        Route::put('/masterclass/update/{id}', 'Admin\FantaziaController@updatemasterclass')->name('admin.fantazia.update.masterclass');
        Route::get('/masterclass/{id}', 'Admin\FantaziaController@deletemasterclass')->name('admin.fantazia.delete.masterclass');
    });
    /*END FANTAZIA*/

});