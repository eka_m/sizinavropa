webpackJsonp([0],{

/***/ 26:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(46)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(33),
  /* template */
  __webpack_require__(42),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Volumes/Files/Work/SIZIN_AVROPA/resources/assets/js/components/Imagetilt.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Imagetilt.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6be5e7db", Component.options)
  } else {
    hotAPI.reload("data-v-6be5e7db", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 29:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(30)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 30:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__plugins_imagetilt_tiltfx__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__plugins_imagetilt_tiltfx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__plugins_imagetilt_tiltfx__);
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['image', 'alt', 'effect'],
    data: function data() {
        return {
            effects: ['{ "movement": { "perspective" : 700, "translateX" : -15, "translateY" : -15, "translateZ" : 20, "rotateX" : 2, "rotateY" : 10 } }', '{ "opacity" : 0.3, "extraImgs" : 3, "movement": { "perspective" : 1200, "translateX" : -5, "translateY" : -5, "rotateX" : -5, "rotateY" : -5 } }', '{ "opacity" : 0.6, "extraImgs" : 4, "movement": { "perspective" : 500, "translateX" : 15, "translateY" : 0, "translateZ" : 10, "rotateX" : 3, "rotateY" : 4, "rotateZ" : 1 } }', '{ "opacity" : 0.8, "bgfixed" : false, "extraImgs" : 3, "movement": { "perspective" : 1500, "translateX" : 80, "translateY" : 80, "translateZ" : 0, "rotateY" : 20 } }', '{ "extraImgs" : 2, "opacity" : 0.7, "bgfixed" : false, "customImgsOpacity" : [0.2, 0.5, 0.03], "movement": { "perspective" : 1000, "translateX" : 30, "translateY" : 30, "translateZ" : -50, "rotateX" : 3, "rotateY" : 3, "rotateZ" : 10 } }', '{ "extraImgs" : false, "bgfixed" : false, "movement": { "perspective" : 1000, "translateX" : 30, "translateY" : 30, "translateZ" : -50, "rotateX" : 3, "rotateY" : 3, "rotateZ" : 10 } }', '{ "extraImgs" : 2, "opacity" : 0.7, "bgfixed" : false, "resetOnLeave" : false, "movement": { "perspective" : 1000, "translateX" : 30, "translateY" : 30, "translateZ" : -50, "rotateX" : 3, "rotateY" : 3, "rotateZ" : 10 } }', '{ "extraImgs" : 2, "extraImgsScaleGrade": -0.05, "opacity" : 0.7, "bgfixed" : false, "movement": { "perspective" : 1000, "translateX" : 30, "translateY" : 30, "translateZ" : -50, "rotateX" : 3, "rotateY" : 3, "rotateZ" : 10 } }', '{ "bgfixed" : false, "movement": { "perspective" : 1000, "translateX" : 30, "translateY" : 30, "translateZ" : -50, "rotateX" : 3, "rotateY" : 3, "rotateZ" : 10 } }', '{ "extraImgs" : 4, "opacity" : 0.5, "movement": { "perspective" : 500, "translateX" : -15, "translateY" : -15, "translateZ" : 20, "rotateX" : 15, "rotateY" : 15 } }', '{ "opacity" : 0.6, "movement": { "perspective" : 1500, "translateX" : 10, "translateY" : 10, "translateZ" : 2, "rotateX" : 3, "rotateY" : 3 } }']
        };
    }
});

/***/ }),

/***/ 36:
/***/ (function(module, exports) {

/**
 * tiltfx.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2015, Codrops
 * http://www.codrops.com
 */
$('document').ready(function () {

	'use strict';

	/**
  * **************************************************************************
  * utils
  * **************************************************************************
  */

	// from https://gist.github.com/desandro/1866474

	var lastTime = 0;
	var prefixes = 'webkit moz ms o'.split(' ');
	// get unprefixed rAF and cAF, if present
	var requestAnimationFrame = window.requestAnimationFrame;
	var cancelAnimationFrame = window.cancelAnimationFrame;
	// loop through vendor prefixes and get prefixed rAF and cAF
	var prefix;
	for (var i = 0; i < prefixes.length; i++) {
		if (requestAnimationFrame && cancelAnimationFrame) {
			break;
		}
		prefix = prefixes[i];
		requestAnimationFrame = requestAnimationFrame || window[prefix + 'RequestAnimationFrame'];
		cancelAnimationFrame = cancelAnimationFrame || window[prefix + 'CancelAnimationFrame'] || window[prefix + 'CancelRequestAnimationFrame'];
	}

	// fallback to setTimeout and clearTimeout if either request/cancel is not supported
	if (!requestAnimationFrame || !cancelAnimationFrame) {
		requestAnimationFrame = function requestAnimationFrame(callback, element) {
			var currTime = new Date().getTime();
			var timeToCall = Math.max(0, 16 - (currTime - lastTime));
			var id = window.setTimeout(function () {
				callback(currTime + timeToCall);
			}, timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};

		cancelAnimationFrame = function cancelAnimationFrame(id) {
			window.clearTimeout(id);
		};
	}

	function extend(a, b) {
		for (var key in b) {
			if (b.hasOwnProperty(key)) {
				a[key] = b[key];
			}
		}
		return a;
	}

	// from http://www.quirksmode.org/js/events_properties.html#position
	function getMousePos(e) {
		var posx = 0;
		var posy = 0;
		if (!e) var e = window.event;
		if (e.pageX || e.pageY) {
			posx = e.pageX;
			posy = e.pageY;
		} else if (e.clientX || e.clientY) {
			posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
			posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
		}
		return {
			x: posx,
			y: posy
		};
	}

	// from http://www.sberry.me/articles/javascript-event-throttling-debouncing
	function throttle(fn, delay) {
		var allowSample = true;

		return function (e) {
			if (allowSample) {
				allowSample = false;
				setTimeout(function () {
					allowSample = true;
				}, delay);
				fn(e);
			}
		};
	}

	/***************************************************************************/

	/**
  * TiltFx fn
  * @param {HTMLElement} img element
  * @param {object} options
  */
	function TiltFx(el, options) {
		if (el) {
			this.el = el;
			this.options = extend({}, this.options);
			extend(this.options, options);
			this._init();
			this._initEvents();
		}
	}

	/**
  * TiltFx options.
  */
	TiltFx.prototype.options = {
		// number of extra image elements (div with background-image) to add to the DOM - min:0, max:64 (for a higher number, it's recommended to remove the transitions of .tilt__front in the stylesheet.
		extraImgs: 2,
		// set scale factor - value what use to set scale gradients for each extra img
		extraImgsScaleGrade: 0,
		// the opacity value for all the image elements.
		opacity: 0.7,
		// when use set array of opacity for each image from bottom to top
		customImgsOpacity: false,
		// by default the first layer does not move.
		bgfixed: true,
		// use reset style for mouseleave event
		resetOnLeave: true,
		// image element's movement configuration
		movement: {
			perspective: 1000, // perspective value
			translateX: -10, // a relative movement of -10px to 10px on the x-axis (setting a negative value reverses the direction)
			translateY: -10, // a relative movement of -10px to 10px on the y-axis 
			translateZ: 20, // a relative movement of -20px to 20px on the z-axis (perspective value must be set). Also, this specific translation is done when the mouse moves vertically.
			rotateX: 2, // a relative rotation of -2deg to 2deg on the x-axis (perspective value must be set)
			rotateY: 2, // a relative rotation of -2deg to 2deg on the y-axis (perspective value must be set)
			rotateZ: 0 // z-axis rotation; by default there's no rotation on the z-axis (perspective value must be set)
		},
		// element for relative custom position offset
		element: {
			// element what will be bind to mousemove
			mouseMoveWatcher: null,
			// element for set bounds of mousemove
			viewWatcher: null
		}
	};

	/**
  * Initialize: build the necessary structure for the image elements and replace it with the HTML img element.
  */
	TiltFx.prototype._init = function () {
		this.tiltWrapper = document.createElement('div');
		this.tiltWrapper.className = 'tilt';

		// main image element.
		this.tiltImgBack = document.createElement('div');
		this.tiltImgBack.className = 'tilt__back';
		this.tiltImgBack.tiltFxType = 'back';
		this.tiltImgBack.style.backgroundImage = 'url(' + this.el.src + ')';
		this.tiltWrapper.appendChild(this.tiltImgBack);

		// image elements limit.
		if (this.options.extraImgs < 1) {
			this.imgCount = 0;
		} else if (this.options.extraImgs > 64) {
			this.imgCount = 64;
		} else {
			this.imgCount = this.options.extraImgs;
		}

		if (!this.options.movement.perspective) {
			this.options.movement.perspective = 0;
		}

		// add the extra image elements.
		this.imgElems = [];
		var frontExtraImagesCount = this.imgCount;
		var customImgsOpacity = this.options.customImgsOpacity;

		if (!this.options.bgfixed) {
			this.imgElems.push(this.tiltImgBack);
			++this.imgCount;
		}

		for (var i = 0; i < frontExtraImagesCount; ++i) {
			var el = document.createElement('div');
			el.className = 'tilt__front';
			el.style.backgroundImage = 'url(' + this.el.src + ')';
			this.tiltWrapper.appendChild(el);
			this.imgElems.push(el);
		}

		// set opacity for images
		this._initSetImagesOpacity();

		// add it to the DOM and remove original img element.
		this.el.parentNode.insertBefore(this.tiltWrapper, this.el);
		this.el.parentNode.removeChild(this.el);

		// set mosemove element area and view restrictions
		this._setViewWatcher(this);
		this._setMouseMoveWatcher(this);

		// viewWatcher properties: width/height/left/top
		this._calcView(this);
	};

	/**
  * Set images opacity.
  * @private
  */
	TiltFx.prototype._initSetImagesOpacity = function () {
		if (this.options.customImgsOpacity) {
			for (var i = 0, len = this.imgElems.length; i < len; ++i) {
				var opacity = this.options.customImgsOpacity[i] ? this.options.customImgsOpacity[i] : this.options.opacity;

				this.imgElems[i].style.opacity = opacity;
			}
		} else {
			for (var i = 0, len = this.imgElems.length; i < len; ++i) {
				if (this.imgElems[i].tiltFxType === 'back') {
					continue;
				}

				this.imgElems[i].style.opacity = this.options.opacity;
			}
		}
	};

	TiltFx.prototype._calcView = function (self) {
		self.view = {
			width: self.viewWatcher.offsetWidth,
			height: self.viewWatcher.offsetHeight
		};
	};

	TiltFx.prototype._setMouseMoveWatcher = function (self) {
		var isSet = false;

		if (self.options.element && self.options.element.mouseMoveWatcher) {
			var mouseMoveWatcherElement = document.querySelector(self.options.element.mouseMoveWatcher);

			self.mouseMoveWatcher = mouseMoveWatcherElement;
			isSet = true;
		}

		if (!isSet) {
			self.mouseMoveWatcher = self.viewWatcher;
		}
	};

	TiltFx.prototype._setViewWatcher = function (self) {
		var isSet = false;

		if (self.options.element && self.options.element.viewWatcher) {
			var customElementRelative = document.querySelector(self.options.element.viewWatcher);

			if (customElementRelative) {
				self.viewWatcher = customElementRelative;
				isSet = true;
			}
		}

		if (!isSet) {
			self.viewWatcher = self.tiltWrapper;
		}
	};

	/**
  * Initialize the events on the main wrapper.
  */
	TiltFx.prototype._initEvents = function () {
		var self = this,
		    moveOpts = self.options.movement;

		// mousemove event..
		self.mouseMoveWatcher.addEventListener('mousemove', function (ev) {
			requestAnimationFrame(function () {
				// mouse position relative to the document.
				var mousepos = getMousePos(ev),

				// document scrolls.
				docScrolls = {
					left: document.body.scrollLeft + document.documentElement.scrollLeft,
					top: document.body.scrollTop + document.documentElement.scrollTop
				},
				    bounds = self.tiltWrapper.getBoundingClientRect(),

				// mouse position relative to the main element (tiltWrapper).
				relmousepos = {
					x: mousepos.x - bounds.left - docScrolls.left,
					y: mousepos.y - bounds.top - docScrolls.top
				};

				// configure the movement for each image element.
				for (var i = 0, len = self.imgElems.length; i < len; ++i) {
					var el = self.imgElems[i],
					    rotX = moveOpts.rotateX ? 2 * ((i + 1) * moveOpts.rotateX / self.imgCount) / self.view.height * relmousepos.y - (i + 1) * moveOpts.rotateX / self.imgCount : 0,
					    rotY = moveOpts.rotateY ? 2 * ((i + 1) * moveOpts.rotateY / self.imgCount) / self.view.width * relmousepos.x - (i + 1) * moveOpts.rotateY / self.imgCount : 0,
					    rotZ = moveOpts.rotateZ ? 2 * ((i + 1) * moveOpts.rotateZ / self.imgCount) / self.view.width * relmousepos.x - (i + 1) * moveOpts.rotateZ / self.imgCount : 0,
					    transX = moveOpts.translateX ? 2 * ((i + 1) * moveOpts.translateX / self.imgCount) / self.view.width * relmousepos.x - (i + 1) * moveOpts.translateX / self.imgCount : 0,
					    transY = moveOpts.translateY ? 2 * ((i + 1) * moveOpts.translateY / self.imgCount) / self.view.height * relmousepos.y - (i + 1) * moveOpts.translateY / self.imgCount : 0,
					    transZ = moveOpts.translateZ ? 2 * ((i + 1) * moveOpts.translateZ / self.imgCount) / self.view.height * relmousepos.y - (i + 1) * moveOpts.translateZ / self.imgCount : 0,
					    scale = 1 + self.options.extraImgsScaleGrade * (len - (i + 1)),
					    scaleCss = scale !== 1 ? ' scale(' + scale + ', ' + scale + ')' : '';

					el.style.WebkitTransform = 'perspective(' + moveOpts.perspective + 'px)' + ' translate3d(' + transX + 'px,' + transY + 'px,' + transZ + 'px)' + ' rotate3d(1,0,0,' + rotX + 'deg)' + ' rotate3d(0,1,0,' + rotY + 'deg)' + ' rotate3d(0,0,1,' + rotZ + 'deg)' + scaleCss;

					el.style.transform = 'perspective(' + moveOpts.perspective + 'px)' + ' translate3d(' + transX + 'px,' + transY + 'px,' + transZ + 'px)' + ' rotate3d(1,0,0,' + rotX + 'deg)' + ' rotate3d(0,1,0,' + rotY + 'deg)' + ' rotate3d(0,0,1,' + rotZ + 'deg)' + scaleCss;
				}
			});
		});

		// reset all when mouse leaves the main wrapper.
		if (self.options.resetOnLeave) {
			self.mouseMoveWatcher.addEventListener('mouseleave', function () {
				setTimeout(function () {
					for (var i = 0, len = self.imgElems.length; i < len; ++i) {
						var el = self.imgElems[i];
						el.style.WebkitTransform = 'perspective(' + moveOpts.perspective + 'px) translate3d(0,0,0) rotate3d(1,1,1,0deg)';
						el.style.transform = 'perspective(' + moveOpts.perspective + 'px) translate3d(0,0,0) rotate3d(1,1,1,0deg)';
					}
				}, 60);
			});
		}

		// window resize
		window.addEventListener('resize', throttle(function () {
			// recalculate viewWatcher properties: width/height/left/top
			self._calcView(self);
		}, 50));
	};

	/**
  * Init tiltFx on each imgs with the class "tilt-effect"
  */
	TiltFx.prototype.init = function () {
		// search for imgs with the class "tilt-effect"
		[].slice.call(document.querySelectorAll('img.tilt-effect')).forEach(function (img) {
			new TiltFx(img, JSON.parse(img.getAttribute('data-tilt-options')));
		});
	};

	new TiltFx().init();

	window.TiltFx = TiltFx;
});

/***/ }),

/***/ 38:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(7)();
exports.i(__webpack_require__(40), "");
exports.push([module.i, "\n\n", ""]);

/***/ }),

/***/ 40:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(7)();
exports.push([module.i, ".tilt {\n\toverflow: hidden;\n\tposition: relative;\n\twidth: 100%;\n\theight: 100%;\n\tmargin: 0 auto;\n}\n\n.tilt__back,\n.tilt__front {\n\twidth: 100%;\n\theight: 100%;\n\tbackground-position: 50% 50%;\n\tbackground-repeat: no-repeat;\n\tbackground-size: cover;\n\t-webkit-backface-visibility: hidden;\n\tbackface-visibility: hidden;\n}\n\n.tilt__back {\n\tposition: relative;\n}\n\n.tilt__front {\n\tposition: absolute;\n\ttop: 0;\n\tleft: 0;\n}", ""]);

/***/ }),

/***/ 42:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('img', {
    staticClass: "tilt-effect responsive",
    attrs: {
      "src": _vm.image,
      "alt": _vm.alt,
      "data-tilt-options": _vm.effects[_vm.effect]
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-6be5e7db", module.exports)
  }
}

/***/ }),

/***/ 46:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(38);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(29)("bd43af64", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-6be5e7db\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Imagetilt.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"id\":\"data-v-6be5e7db\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Imagetilt.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});