$(function() {
  var lang = $('html').attr('lang');
  getAllEvents();
  var date = new Date();
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var day = date.getDate();
  if (day < 10) {
    day = '0' + day;
  }
  if (month < 10) {
    month = '0' + month;
  }
  var today = year + '-' + month + '-' + day;

  var LocalCalendar = {
    az: {
      weeks: [
        'Bazar',
        'Bazar ertəsi',
        'Çərşənbə axşamı',
        'Çərşənbə',
        'Cümə axşamı',
        'Cümə',
        'Şənbə'
      ],
      weekabbrs: ['Baz.', 'B.e', 'Ç.a', 'Çər.', 'C.a', 'Cüm.', 'Şən.'],
      months: [
        'Yanvar',
        'Fevral',
        'Mart',
        'Aprel',
        'May',
        'İyun',
        'İyul',
        'Avqust',
        'Sentyabr',
        'Oktyabr',
        'Noyabr',
        'Dekabr'
      ],
      monthabbrs: [
        'Yan',
        'Fev',
        'Mar',
        'Apr',
        'May',
        'İyn',
        'İyl',
        'Avq',
        'Sen',
        'Okt',
        'Noy',
        'Dek'
      ]
    },
    en: {
      weeks: [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
      ],
      weekabbrs: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
      months: [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
      ],
      monthabbrs: [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'
      ]
    }
  };

  var messages = {
    az: {
      noEvents: 'Bu tarixdə heç bir tədbir yoxdur.'
    },
    en: {
      noEvents: 'There are no events on this date.'
    }
  };

  var cal = $('#fullcalendar').calendario({
    weeks: LocalCalendar[lang]['weeks'],
    months: LocalCalendar[lang]['months'],
    weekabbrs: LocalCalendar[lang]['weekabbrs'],
    monthabbrs: LocalCalendar[lang]['monthabbrs'],
    displayWeekAbbr: true,
    onDayClick: function($el, $contentEl, dateProperties) {
      onSelectedDay($contentEl.content, dateProperties);
    }
  });

  function onSelectedDay($contentEl, dateProperties) {
    spinHandler('show');
    if ($contentEl.length == 0) {
      spinHandler('hide');
      setCurrentDay(dateProperties);
      $('.custom-calendar-day-events').html(
        `<span class="fullcalendar-noevents">${messages[lang].noEvents}</span>`
      );
      return false;
    }
    var selectedDayEvents = $($contentEl[0])
      .filter('.dateEvents')
      .html();
    setTimeout(() => {
      createTimeTable(JSON.parse(selectedDayEvents));
      setCurrentDay(dateProperties);
    }, 700);
  }

  function setCurrentDay(date) {
    $('.custom-calendar-day-current_date').html(`
    <span class="fullcalendar-current-weekday">${date.weekdayname}</span>,
    <span class="fullcalendar-current-day">${date.day}<span>
    <span class="fullcalendar-current-month">${date.monthname}</span>
  `);
  }
  var month = $('#custom-month').html(cal.getMonthName());
  var year = $('#custom-year').html(cal.getYear());

  $('#custom-next').on('click', function() {
    cal.gotoNextMonth(updateMonthYear);
  });
  $('#custom-prev').on('click', function() {
    cal.gotoPreviousMonth(updateMonthYear);
  });
  $('#custom-current').on('click', function() {
    cal.gotoNow(updateMonthYear);
  });

  function updateMonthYear() {
    month.html(cal.getMonthName());
    year.html(cal.getYear());
  }

  function getAllEvents() {
    $.ajax({
      type: 'GET',
      url: '/' + lang + '/allEvents',
      dataType: 'JSON',
      beforeSend: function() {
        spinHandler('show');
      },
      success: function(response) {
        setCalendarEvents(response);
      },
      error: function(error) {
        console.log('Error in "getAllEvents"');
      }
    });
  }

  function getDayEvents(date) {
    $.ajax({
      type: 'GET',
      url: '/async-one-day-events/' + date,
      dataType: 'JSON',
      beforeSend: function() {
        spinHandler('show');
      },
      success: function(response) {
        createTimeTable(response);
      },
      error: function(error) {
        console.log('Error in "getDayEvents"');
      }
    });
  }

  function createTimeTable(jsonData) {
    var htmlData = '';
    for (var venueName in jsonData) {
      var venue = jsonData[venueName];
      htmlData =
        htmlData + `<div class="fullcalendar-venue">${venueName}</div>`;
      for (var event in venue) {
        htmlData = `${htmlData}
					<div class="fullcalendar-event">
						<span>
							${venue[event]['start'] !== null ? venue[event]['start'] + ' / ' : '-- '}
							${venue[event]['end'] !== null ? venue[event]['end'] : '--'}
						</span>
						<a href="/${lang}/article/${venue[event]['url']}">${
          venue[event]['event_name'] !== null && venue[event]['event_name'] != '<p><br></p>'
            ? venue[event]['event_name']
            : venue[event]['name']
        }</a>
					</div>`;
      }
    }
    spinHandler('hide');
    $('.custom-calendar-day-events').html(htmlData);
  }

  function spinHandler(action) {
    var spinner = $('#fullcalendar-spinner');
    if (action == 'show') {
      spinner.removeClass('d-none');
    } else if (action == 'hide') {
      spinner.addClass('d-none');
    } else {
      console.log('You are note setup action');
    }
  }
  function setCalendarEvents(allevents) {
    var elems = {};
    for (var date in allevents) {
      var venues = allevents[date];
      var count = 0;
      for (var venue in venues) {
        var events = venues[venue];
        for (var event in events) {
          var event = events[event];
          ++count;
          elems[date] = {
            content:
              '<span class="fullcaelndar-event_count">' +
              count +
              '</span><span class="dateEvents">' +
              JSON.stringify(venues) +
              '</span>',
            startDate: event['start'],
            endDate: event['end']
          };
        }
      }
    }
    cal.setData(elems);
    spinHandler('hide');
  }
});
